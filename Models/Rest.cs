﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Ultimate_Schedule_Builder.Models
{
    public static class Rest
    {
        private const string baseUrl = "http://stag.honzad.cz/";

        /// <summary>
        /// Stores local cookie for keeping the user logged in
        /// </summary>
        private static CookieContainer cookieContainer = new CookieContainer();

        /// <summary>
        /// Creates Login request to the website
        /// </summary>
        /// <param name="u">Stag username</param>
        /// <param name="p">password</param>
        /// <returns>User object</returns>
        public async static Task<Tuple<StagUserRole, dynamic>> CreateAuthLoginRequest(string u, string p)
        {
            Dictionary<string, string> data = new Dictionary<string, string>() { { "Username", u }, { "Password", p } };
            Tuple<int, string> response = await CreateRequestAsync(RequestType.AuthLogin, data);

            try
            {
                CheckResponseCode(RequestType.AuthLogin, response.Item1);

                JObject lr = JObject.Parse(response.Item2);
                string ut = lr.Value<string>("stagUserRole");
                JObject ud = lr.Value<JObject>("stagUserInfo");

                if (ut == "ST")
                {
                    StagFacultyType tt = StagFacultyType.Default;
                    Enum.TryParse<StagFacultyType>(ud.Value<string>("stagUserFaculty"), out tt);

                    StagStudent stObj = new StagStudent(
                        ud.Value<string>("stagUserOsCislo"),
                        ud.Value<string>("stagUserFirstName"),
                        ud.Value<string>("stagUserLastName"),
                        ud.Value<string>("stagUserName"),
                        tt,
                        ud.Value<string>("stagUserField"),
                        ud.Value<int>("stagUserCurrentGrade")
                    );
                    return new Tuple<StagUserRole, dynamic>(StagUserRole.Student, stObj);
                }
                else
                {
                    StagTeacher stObj = new StagTeacher(
                        ud.Value<string>("stagUserTeacherId"),
                        ud.Value<string>("stagUserFirstName"),
                        ud.Value<string>("stagUserLastName")
                    );

                    return new Tuple<StagUserRole, dynamic>(StagUserRole.Teacher, stObj);
                }
            }
            catch (RequestFailedException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Logout request
        /// </summary>
        /// <returns>Boolean value about the result</returns>
        public async static Task<bool> CreateAuthLogoutRequest()
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            Tuple<int, string> response = await CreateRequestAsync(RequestType.AuthLogout, data);

            try
            {
                CheckResponseCode(RequestType.AuthLogout, response.Item1);

                return true;
            }
            catch (RequestFailedException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Gets all possible finds in the School student db
        /// </summary>
        /// <param name="n">First name</param>
        /// <param name="s">Last name</param>
        /// <returns>StagStudent Array</returns>
        public async static Task<StagStudent[]> CreateStudentGetByNameRequest(string n, string s)
        {
            Dictionary<string, string> data = new Dictionary<string, string>() { { "Name", n }, { "Surname", s } };
            Tuple<int, string> response = await CreateRequestAsync(RequestType.StudentGetByName, data);

            try
            {
                CheckResponseCode(RequestType.StudentGetByName, response.Item1);

                if (response.Item1 == 204)
                {
                    return new StagStudent[] { };
                }

                JArray sarr = (JArray)JToken.Parse(response.Item2);

                List<StagStudent> stList = new List<StagStudent>();
                foreach (JObject st in sarr)
                {
                    StagFacultyType tt = StagFacultyType.Default;
                    Enum.TryParse<StagFacultyType>(st.Value<string>("stagUserFaculty"), out tt);

                    StagStudent stObj = new StagStudent(
                        st.Value<string>("stagUserOsCislo"),
                        st.Value<string>("stagUserFirstName"),
                        st.Value<string>("stagUserLastName"),
                        st.Value<string>("stagUserName"),
                        tt,
                        st.Value<string>("stagUserField"),
                        st.Value<int>("stagUserCurrentGrade")
                    );

                    stList.Add(stObj);
                }

                return stList.ToArray();
            }
            catch (RequestFailedException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Get student schedule by oscislo
        /// </summary>
        /// <param name="oc"> Student osobnicislo </param>
        /// <param name="args">Datefrom,Dateto -> from.ToString("d.M.yyyy"),Nocreditterms -> true   </param>
        /// <returns> Schedule </returns>
        public async static Task<Schedule> CreateStudentGetScheduleRequest(StagUser su, params KeyValuePair<string, string>[] args)
        {
            Dictionary<string, string> data = new Dictionary<string, string>() { { "Oscislo", su.stagId } };
            foreach (KeyValuePair<string, string> v in args)
            {
                data.Add(v.Key, v.Value);
            }

            Tuple<int, string> response = await CreateRequestAsync(RequestType.StudentGetSchedule, data);

            try
            {
                CheckResponseCode(RequestType.StudentGetSchedule, response.Item1);

                if (response.Item1 == 204)
                {
                    return new Schedule();
                }

                JArray sarr = (JArray)JToken.Parse(response.Item2);
                List<Course> stList = new List<Course>();
                foreach (JObject st in sarr)
                {
                    JObject stObj = st.Value<JObject>("ScheduleTeacher");
                    StagTeacher steach = new StagTeacher(
                       stObj.Value<string>("stagUserTeacherId"),
                       stObj.Value<string>("stagUserFirstName"),
                       stObj.Value<string>("stagUserLastName")
                    );

                    if (st.Value<string>("ScheduleFrom") == null || st.Value<string>("ScheduleFrom") == "null")
                    {
                        continue;
                    }

                    string dateStringFrom = $"{st.Value<string>("ScheduleDate")} {st.Value<string>("ScheduleFrom")}";
                    string dateStringTo = $"{st.Value<string>("ScheduleDate")} {st.Value<string>("ScheduleTo")}";

                    if (DateTime.TryParseExact(dateStringFrom, "d.M.yyyy HH:mm", null, DateTimeStyles.None, out DateTime dateFrom))
                    {
                        if (dateFrom.Minute != 0)
                        {
                            dateFrom.AddMinutes(-dateFrom.Minute);
                        }

                        if (DateTime.TryParseExact(dateStringTo, "d.M.yyyy HH:mm", null, DateTimeStyles.None, out DateTime dateTo))
                        {
                            if (dateTo.Minute != 0)
                            {
                                dateTo.AddMinutes(60 - dateTo.Minute);
                            }

                            int SubjectLenght = (int)Math.Ceiling(((TimeSpan)(dateTo - dateFrom)).TotalHours);
                            CourseDate cd = new CourseDate(dateFrom);
                            CourseType ct = Utility.GetCourseType(st.Value<string>("ScheduleType"));

                            Course c = new Course(
                                cd,
                                SubjectLenght,
                                st.Value<string>("CourseScheduleId"),
                                st.Value<string>("ScheduleId"),
                                st.Value<string>("ScheduleName"),
                                ct,
                                steach,
                                st.Value<string>("ScheduleDepartment"),
                                st.Value<string>("ScheduleBuilding"),
                                st.Value<string>("ScheduleRoom"),
                                su
                            );

                            if (st.TryGetValue("ScheduleStatic", out JToken val))
                            {
                                c.IsStatic = true;
                            }

                            if (st.TryGetValue("ScheduleCourseEmptyTimeData", out JToken istm))
                            {
                                c.IsTimeDataMissing = true;
                            }

                            stList.Add(c);
                        }
                    }
                }

                return new Schedule(stList.ToArray());
            }
            catch (RequestFailedException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Gets all possible finds in the School teacher db
        /// </summary>
        /// <param name="n">First name</param>
        /// <param name="s">Last name</param>
        /// <returns>StagTeacher Array</returns>
        public async static Task<StagTeacher[]> CreateTeacherGetByNameRequest(string n, string s)
        {
            Dictionary<string, string> data = new Dictionary<string, string>() { { "Name", n }, { "Surname", s } };
            Tuple<int, string> response = await CreateRequestAsync(RequestType.TeacherGetByName, data);

            try
            {
                CheckResponseCode(RequestType.TeacherGetByName, response.Item1);

                if (response.Item1 == 204)
                {
                    return new StagTeacher[] { };
                }

                JArray sarr = (JArray)JToken.Parse(response.Item2);

                List<StagTeacher> stList = new List<StagTeacher>();
                foreach (JObject st in sarr)
                {
                    StagTeacher stObj = new StagTeacher(
                        st.Value<string>("stagUserTeacherId"),
                        st.Value<string>("stagUserFirstName"),
                        st.Value<string>("stagUserLastName")
                    );

                    stList.Add(stObj);
                }

                return stList.ToArray();
            }
            catch (RequestFailedException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Get teacher schedule by teacherid
        /// </summary>
        /// <param name="ui"> Teacher id </param>
        /// <param name="args"> Datefrom,Dateto -> from.ToString("d.M.yyyy"), Nocreditterms -> true  </param>
        /// <returns>Schedule</returns>
        public async static Task<Schedule> CreateTeacherGetScheduleRequest(StagUser su, params KeyValuePair<string, string>[] args)
        {
            Dictionary<string, string> data = new Dictionary<string, string>() { { "Teacherid", su.stagId } };
            foreach (KeyValuePair<string, string> v in args)
            {
                data.Add(v.Key, v.Value);
            }

            Tuple<int, string> response = await CreateRequestAsync(RequestType.TeacherGetSchedule, data);

            try
            {
                CheckResponseCode(RequestType.StudentGetSchedule, response.Item1);

                if (response.Item1 == 204)
                {
                    return new Schedule();
                }

                JArray sarr = (JArray)JToken.Parse(response.Item2);
                List<Course> stList = new List<Course>();
                foreach (JObject st in sarr)
                {
                    JObject stObj = st.Value<JObject>("ScheduleTeacher");
                    StagTeacher steach = new StagTeacher(
                       stObj.Value<string>("stagUserTeacherId"),
                       stObj.Value<string>("stagUserFirstName"),
                       stObj.Value<string>("stagUserLastName")
                    );

                    if (st.Value<string>("ScheduleFrom") == null || st.Value<string>("ScheduleFrom") == "null")
                    {
                        continue;
                    }

                    string dateStringFrom = $"{st.Value<string>("ScheduleDate")} {st.Value<string>("ScheduleFrom")}";
                    string dateStringTo = $"{st.Value<string>("ScheduleDate")} {st.Value<string>("ScheduleTo")}";

                    if (DateTime.TryParseExact(dateStringFrom, "d.M.yyyy HH:mm", null, DateTimeStyles.None, out DateTime dateFrom))
                    {
                        if (dateFrom.Minute != 0)
                        {
                            dateFrom.AddMinutes(-dateFrom.Minute);
                        }

                        if (DateTime.TryParseExact(dateStringTo, "d.M.yyyy HH:mm", null, DateTimeStyles.None, out DateTime dateTo))
                        {
                            if (dateTo.Minute != 0)
                            {
                                dateTo.AddMinutes(60 - dateTo.Minute);
                            }

                            int SubjectLenght = (int)Math.Ceiling(((TimeSpan)(dateTo - dateFrom)).TotalHours);
                            CourseDate cd = new CourseDate(dateFrom);
                            CourseType ct = Utility.GetCourseType(st.Value<string>("ScheduleType"));

                            Course c = new Course(
                                cd,
                                SubjectLenght,
                                st.Value<string>("CourseScheduleId"),
                                st.Value<string>("ScheduleId"),
                                st.Value<string>("ScheduleName"),
                                ct,
                                steach,
                                st.Value<string>("ScheduleDepartment"),
                                st.Value<string>("ScheduleBuilding"),
                                st.Value<string>("ScheduleRoom"),
                                su
                            );

                            if (st.TryGetValue("ScheduleStatic", out JToken ist))
                            {
                                c.IsStatic = true;
                            }

                            if (st.TryGetValue("ScheduleCourseEmptyTimeData", out JToken istm))
                            {
                                c.IsTimeDataMissing = true;
                            }

                            stList.Add(c);
                        }
                    }
                }

                return new Schedule(stList.ToArray()); ;
            }
            catch (RequestFailedException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Gets free rooms of selected buildings
        /// </summary>
        /// <param name="spos">Course position in schedule 0 - 14</param>
        /// <param name="date"></param>
        /// <param name="bstring"></param>
        /// <returns>Array of StagRooms</returns>
        public async static Task<StagRoom[]> CreateRoomGetFreeRoomsInBuildingRequest(int spos, string date, string[] buildings)
        {
            Dictionary<string, string> data = new Dictionary<string, string>() { { "Schedulepos", spos.ToString() }, { "Datefrom", date }, { "Buildings", WebUtility.UrlEncode(JsonConvert.SerializeObject(buildings)) } };

            Tuple<int, string> response = await CreateRequestAsync(RequestType.RoomGetFreeRoomsInBuilding, data);

            if (response.Item1 == 204)
            {
                return new StagRoom[] { };
            }

            try
            {
                CheckResponseCode(RequestType.RoomGetFreeRoomsInBuilding, response.Item1);

                JArray sarr = (JArray)JToken.Parse(response.Item2);
                List<StagRoom> rList = new List<StagRoom>();

                foreach (JObject rm in sarr)
                {
                    if (Enum.TryParse<StagFacultyType>(rm.Value<string>("RoomFaculty"), out StagFacultyType srf))
                    {
                        if (Enum.TryParse<StagBuildingList>(rm.Value<string>("RoomBuilding"), out StagBuildingList sbl))
                        {
                            StagRoom sr = new StagRoom(
                                srf,
                                sbl,
                                rm.Value<string>("RoomBuildingAdress"),
                                rm.Value<string>("RoomNumber"),
                                rm.Value<int>("RoomCapacity"),
                                rm.Value<string>("RoomFloor"),
                                rm.Value<bool>("RoomFree")
                            );

                            rList.Add(sr);
                        }
                    }
                }

                return rList.ToArray();
            }
            catch (RequestFailedException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Gets logged users grades
        /// </summary>
        /// <returns></returns>
        public async static Task<StagGrade[]> CreateStudentGetGradesRequest()
        {
            Tuple<int, string> response = await CreateRequestAsync(RequestType.StudentGetGrades, new Dictionary<string, string>() { });

            if (response.Item1 == 204)
            {
                return new StagGrade[] { };
            }

            try
            {
                CheckResponseCode(RequestType.StudentGetGrades, response.Item1);

                JArray sarr = (JArray)JToken.Parse(response.Item2);
                List<StagGrade> rList = new List<StagGrade>();

                foreach (JObject grd in sarr)
                {
                    JObject gradeCourse = grd.Value<JObject>("GradeCourse");
                    StagCourse sCourse = new StagCourse(
                        gradeCourse.Value<string>("CourseFaculty"),
                        gradeCourse.Value<string>("CourseId"),
                        gradeCourse.Value<string>("CourseName"),
                        gradeCourse.Value<int>("CourseCredits"),
                        gradeCourse.Value<bool>("CourseHasExam"),
                        gradeCourse.Value<bool>("CourseFinished")
                        );

                    JObject gradeAcredation = grd.Value<JObject>("GradeAcredation");
                    StagGradePart gAcredit;
                    if (gradeAcredation != null)
                    {
                        JObject gradeTeacher = gradeAcredation.Value<JObject>("GradeTeacher");

                        gAcredit = new StagGradePart(
                            gradeAcredation.Value<bool>("GradeByNumber"),
                            gradeAcredation.Value<dynamic>("GradeStatus"),
                            gradeAcredation.Value<int>("GradeAttempt"),
                            gradeAcredation.Value<string>("GradeAttemptDate"),
                            new StagTeacher(
                                gradeTeacher.Value<int>("stagUserTeacherId").ToString(),
                                gradeTeacher.Value<string>("stagUserFirstName"),
                                gradeTeacher.Value<string>("stagUserLastName")
                                )
                            );
                    }
                    else
                    {
                        gAcredit = null;
                    }

                    JObject gradeExam = grd.Value<JObject>("GradeExam");
                    StagTeacher examTeacher;
                    if(gradeExam.Value<JObject>("GradeTeacher") != null)
                    {
                        JObject examTeach = gradeExam.Value<JObject>("GradeTeacher");

                        examTeacher = new StagTeacher(
                            examTeach.Value<int>("stagUserTeacherId").ToString(),
                            examTeach.Value<string>("stagUserFirstName"),
                            examTeach.Value<string>("stagUserLastName")
                            );
                    }
                    else
                    {
                        examTeacher = null;
                    }

                    StagGradePart gExam = new StagGradePart(
                        gradeExam.Value<bool>("GradeByNumber"),
                        gradeExam.Value<dynamic>("GradeStatus"),
                        gradeExam.Value<int>("GradeAttempt"),
                        gradeExam.Value<string>("GradeAttemptDate"),
                        examTeacher
                        );

                    StagGrade sgrade = new StagGrade(sCourse, gAcredit, gExam);
                    rList.Add(sgrade);
                }

                return rList.ToArray();
            }
            catch (RequestFailedException e)
            {
                throw e;
            }

        }

        /// <summary>
        /// Gets logged users info
        /// </summary>
        /// <returns></returns>
        public async static Task<StagStudent> CreateStudentGetInfoRequest()
        {
            Tuple<int, string> response = await CreateRequestAsync(RequestType.StudentGetInfo, new Dictionary<string, string>() { });

            try
            {
                CheckResponseCode(RequestType.StudentGetInfo, response.Item1);

                if (response.Item1 == 204)
                {
                    throw new RequestFailedException("Not found", 204);
                }

                JObject st = (JObject)JToken.Parse(response.Item2);
                if (Enum.TryParse<StagFacultyType>(st.Value<string>("StudentFaculty"), out StagFacultyType tt))
                {
                    StagStudent stObj = new StagStudent(
                       st.Value<string>("StudentOsCislo"),
                       st.Value<string>("StudentFirstName"),
                       st.Value<string>("StudentLastName"),
                       st.Value<string>("StudentUserName"),
                       tt,
                       st.Value<string>("StudentField"),
                       st.Value<int>("StudentCurrentYear"),
                       st.Value<string>("StudentTitleBefore"),
                       st.Value<string>("StudentTitleAfter"),
                       st.Value<bool>("StudentStudyState"),
                       st.Value<int>("StudentCredits")
                   );

                    return stObj;
                }
                else
                {
                    throw new RequestFailedException("Not found", 204);
                }
            }
            catch (RequestFailedException e)
            {
                throw e;
            }

        }

        /// <summary>
        /// Gets logged users all absolved courses with partial groades
        /// </summary>
        /// <returns></returns>
        public async static Task<StagAbsolvedCourse[]> CreateStudentGetAbsolvedSubjectsRequest()
        {
            Tuple<int, string> response = await CreateRequestAsync(RequestType.StudentGetAbsolvedSubjects, new Dictionary<string, string>() { });

            if (response.Item1 == 204)
            {
                return new StagAbsolvedCourse[] { };
            }

            try
            {
                CheckResponseCode(RequestType.StudentGetAbsolvedSubjects, response.Item1);
                JArray sarr = (JArray)JToken.Parse(response.Item2);
                List<StagAbsolvedCourse> rList = new List<StagAbsolvedCourse>();

                foreach (JObject grd in sarr)
                {
                    StagAbsolvedCourse s = new StagAbsolvedCourse(
                         grd.Value<string>("CourseFaculty"),
                         grd.Value<string>("CourseId"),
                         grd.Value<string>("CourseName"),
                         grd.Value<string>("CourseSemester"),
                         grd.Value<string>("CourseYear"),
                         grd.Value<string>("CourseDateAbsolved"),
                         grd.Value<bool>("CourseAbsolved"),
                         grd.Value<string>("CourseGrade"),
                         grd.Value<int>("CourseCreditCount")
                        );

                    rList.Add(s);
                }



                return rList.ToArray();
            }
            catch (RequestFailedException e)
            {
                throw e;
            }

        }

        /// <summary>
        /// Gets logged users terms
        /// </summary>
        /// <returns>Schedule object</returns>
        public async static Task<Schedule> CreateStudentGetTermsRequest()
        {
            Tuple<int, string> response = await CreateRequestAsync(RequestType.StudentGetTerms, new Dictionary<string, string>() { });

            if (response.Item1 == 204)
            {
                return new Schedule();
            }

            try
            {
                CheckResponseCode(RequestType.StudentGetTerms, response.Item1);

                JArray sarr = (JArray)JToken.Parse(response.Item2);
                List<Course> stList = new List<Course>();
                foreach (JObject st in sarr)
                {
                    JObject stObjTch = st.Value<JObject>("ScheduleTeacher");
                    StagTeacher steach = new StagTeacher(
                       stObjTch.Value<string>("stagUserTeacherId"),
                       stObjTch.Value<string>("stagUserFirstName"),
                       stObjTch.Value<string>("stagUserLastName")
                    );

                    JObject stObjTerm = st.Value<JObject>("ScheduleTerm");
                    StagTerm sterm = null;

                    if (DateTime.TryParseExact(stObjTerm.Value<string>("TermRegistrationDeadline"), "d.M.yyyy H:mm", null, DateTimeStyles.None, out DateTime termRegDL))
                    {
                        if (DateTime.TryParseExact(stObjTerm.Value<string>("TermUnregistrationDeadline"), "d.M.yyyy H:mm", null, DateTimeStyles.None, out DateTime termUnRegDL))
                        {
                            sterm = new StagTerm(
                                stObjTerm.Value<string>("TermId"),
                                stObjTerm.Value<int>("TermLimit"),
                                stObjTerm.Value<int>("TermStudentCount"),
                                stObjTerm.Value<bool>("TermRegistered"),
                                stObjTerm.Value<bool>("TermCanUnregister"),
                                new CourseDate(termRegDL),
                                new CourseDate(termUnRegDL)
                                );
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }


                    if (st.Value<string>("ScheduleFrom") == null || st.Value<string>("ScheduleFrom") == "null")
                    {
                        continue;
                    }

                    string dateStringFrom = $"{st.Value<string>("ScheduleDate")} {st.Value<string>("ScheduleFrom")}";
                    string dateStringTo = $"{st.Value<string>("ScheduleDate")} {st.Value<string>("ScheduleTo")}";

                    if (DateTime.TryParseExact(dateStringFrom, "d.M.yyyy HH:mm", null, DateTimeStyles.None, out DateTime dateFrom))
                    {
                        if (dateFrom.Minute != 0)
                        {
                            dateFrom.AddMinutes(-dateFrom.Minute);
                        }

                        if (DateTime.TryParseExact(dateStringTo, "d.M.yyyy HH:mm", null, DateTimeStyles.None, out DateTime dateTo))
                        {
                            if (dateTo.Minute != 0)
                            {
                                dateTo.AddMinutes(60 - dateTo.Minute);
                            }

                            int SubjectLenght = (int)Math.Ceiling(((TimeSpan)(dateTo - dateFrom)).TotalHours);
                            CourseDate cd = new CourseDate(dateFrom);
                            CourseType ct = Utility.GetCourseType(st.Value<string>("ScheduleType"));

                            Course c = new Course(
                                cd,
                                SubjectLenght,
                                st.Value<string>("ScheduleId"),
                                st.Value<string>("ScheduleName"),
                                ct,
                                steach,
                                st.Value<string>("ScheduleDepartment"),
                                st.Value<string>("ScheduleBuilding"),
                                st.Value<string>("ScheduleRoom"),
                                Settings.LocalUser,
                                sterm
                            );

                            stList.Add(c);
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }

                return new Schedule(stList.ToArray());
            }
            catch (RequestFailedException e)
            {
                throw e;
            }


        }

        /// <summary>
        /// Sets target Term register status
        /// </summary>
        /// <param name="tid"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public async static Task<bool> CreateStudentSetTermRequest(string tid, bool t)
        {
            Dictionary<string, string> data = new Dictionary<string, string>() { { "Termid", tid }, { "Termisreg", t.ToString() } };

            Tuple<int, string> response = await CreateRequestAsync(RequestType.StudentSetTerm, data);

            if (response.Item1 == 204)
            {
                return false;
            }

            try
            {
                CheckResponseCode(RequestType.StudentSetTerm, response.Item1);

                return true;
            }
            catch (RequestFailedException e)
            {
                throw e;
            }

        }

        /// <summary>
        /// Returns all Students on this Course term
        /// </summary>
        /// <param name="csid">Course schedule id</param>
        /// <returns></returns>
        public async static Task<StagStudent[]> CreateStudentGetByCourseRequest(string csid)
        {
            Dictionary<string, string> data = new Dictionary<string, string>() { { "Coursescheduleid", csid } };

            Tuple<int, string> response = await CreateRequestAsync(RequestType.StudentGetByCourse, data);

            if (response.Item1 == 204)
            {
                return new StagStudent[] { };
            }

            try
            {
                CheckResponseCode(RequestType.StudentGetByCourse, response.Item1);

                JArray sarr = (JArray)JToken.Parse(response.Item2);

                List<StagStudent> stList = new List<StagStudent>();
                foreach (JObject st in sarr)
                {
                    StagFacultyType tt = StagFacultyType.Default;
                    Enum.TryParse<StagFacultyType>(st.Value<string>("stagUserFaculty"), out tt);

                    StagStudent stObj = new StagStudent(
                        st.Value<string>("stagUserOsCislo"),
                        st.Value<string>("stagUserFirstName"),
                        st.Value<string>("stagUserLastName"),
                        st.Value<string>("stagUserName"),
                        tt,
                        st.Value<string>("stagUserField"),
                        st.Value<int>("stagUserCurrentGrade")
                    );

                    stList.Add(stObj);
                }

                return stList.ToArray();
            }
            catch (RequestFailedException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Returns all Students on this Term
        /// </summary>
        /// <param name="tid">Term id</param>
        /// <returns></returns>
        public async static Task<StagStudent[]> CreateStudentGetByTermRequest(string tid)
        {
            Dictionary<string, string> data = new Dictionary<string, string>() { { "Termid", tid }};

            Tuple<int, string> response = await CreateRequestAsync(RequestType.StudentGetByTerm, data);

            if (response.Item1 == 204)
            {
                return new StagStudent[] { };
            }

            try
            {
                CheckResponseCode(RequestType.StudentGetByTerm, response.Item1);

                JArray sarr = (JArray)JToken.Parse(response.Item2);

                List<StagStudent> stList = new List<StagStudent>();
                foreach (JObject st in sarr)
                {
                    StagFacultyType tt = StagFacultyType.Default;
                    Enum.TryParse<StagFacultyType>(st.Value<string>("stagUserFaculty"), out tt);

                    StagStudent stObj = new StagStudent(
                        st.Value<string>("stagUserOsCislo"),
                        st.Value<string>("stagUserFirstName"),
                        st.Value<string>("stagUserLastName"),
                        st.Value<string>("stagUserName"),
                        tt,
                        st.Value<string>("stagUserField"),
                        st.Value<int>("stagUserCurrentGrade")
                    );

                    stList.Add(stObj);
                }

                return stList.ToArray();
            }
            catch (RequestFailedException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Creates http request
        /// </summary>
        /// <param name="rt">Request type</param>
        /// <param name="data">Arguments</param>
        /// <returns>Tuple with HTTP Status code and Body string</returns>
        private async static Task<Tuple<int, string>> CreateRequestAsync(RequestType rt, Dictionary<string, string> data)
        {
            List<KeyValuePair<string, string>> dList = new List<KeyValuePair<string, string>>();
            foreach (KeyValuePair<string, string> v in data)
            {
                dList.Add(new KeyValuePair<string, string>(v.Key, v.Value));
            }
            string paramString = "";
            foreach (KeyValuePair<string, string> v in dList)
            {
                int vIndex = dList.IndexOf(v);
                bool isFirst = vIndex == 0;

                paramString += (isFirst ? "?" : "&");
                paramString += v.Key + "=" + v.Value;
            }

            string requestUrlString = baseUrl + GetRequestTypeUrl(rt) + paramString;

            Console.WriteLine("Test");

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(requestUrlString);

            req.CookieContainer = cookieContainer;
            req.Accept = "application/json";
            req.ContentType = "application/json";
            req.Method = HttpMethod.Get.ToString();
            req.KeepAlive = false;

            try
            {
                HttpWebResponse res = (HttpWebResponse)await req.GetResponseAsync();

                if (res.StatusCode == HttpStatusCode.OK)
                {
                    using (Stream stream = res.GetResponseStream())
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string ret = await reader.ReadToEndAsync();

                        return new Tuple<int, string>((int)res.StatusCode, ret);
                    }
                }
                else
                {
                    return new Tuple<int, string>((int)res.StatusCode, "");
                }
            }
            catch (WebException e)
            {
                throw new RequestFailedException(e.Message, (int)e.Status);
            }
        }

        /// <summary>
        /// Returns url continuation by Request type
        /// </summary>
        /// <param name="r">Request type</param>
        /// <returns>string</returns>
        private static string GetRequestTypeUrl(RequestType r)
        {
            string url = "";

            switch (r)
            {
                case RequestType.AuthLogin:
                    url = "Auth/Login";
                    break;

                case RequestType.AuthLogout:
                    url = "Auth/Logout";
                    break;

                case RequestType.StudentGetByName:
                    url = "Student/GetByName";
                    break;

                case RequestType.StudentGetSchedule:
                    url = "Student/GetSchedule";
                    break;

                case RequestType.StudentGetOsCisloByStagId:
                    url = "Student/GetOsCisloByStagId";
                    break;

                case RequestType.StudentGetGrades:
                    url = "Student/GetGrades";
                    break;

                case RequestType.StudentGetInfo:
                    url = "Student/GetInfo";
                    break;

                case RequestType.StudentSetTerm:
                    url = "Student/SetTerm";
                    break;

                case RequestType.StudentGetAbsolvedSubjects:
                    url = "Student/GetAbsolvedSubjects";
                    break;

                case RequestType.StudentGetTerms:
                    url = "Student/GetTerms";
                    break;

                case RequestType.StudentGetByCourse:
                    url = "Student/GetByCourse";
                    break;

                case RequestType.StudentGetByTerm:
                    url = "Student/GetByTerm";
                    break;

                case RequestType.TeacherGetByName:
                    url = "Teacher/GetByName";
                    break;

                case RequestType.TeacherGetSchedule:
                    url = "Teacher/GetSchedule";
                    break;

                case RequestType.RoomGetFreeRoomsInBuilding:
                    url = "Room/GetFreeRoomsInBuilding";
                    break;
            }

            return url;
        }

        /// <summary>
        /// Checks the code and throws exceptions when bad ones show
        /// </summary>
        /// <param name="r">Request type</param>
        /// <param name="c">Code</param>
        private static void CheckResponseCode(RequestType r, int c)
        {
            switch (r)
            {
                case RequestType.AuthLogin:
                    switch (c)
                    {
                        case 498:
                            throw new RequestFailedException("User already logged in", c);
                        case 497:
                            throw new RequestFailedException("Forced logout", c);
                        case 495:
                            throw new RequestFailedException("Logout failed", c);
                        case 406:
                            throw new RequestFailedException("Header paramater missing", c);
                        case 401:
                            throw new RequestFailedException("Unathorized", c);
                    }
                    break;

                case RequestType.AuthLogout:
                    switch (c)
                    {
                        case 499:
                            throw new RequestFailedException("Session expired", c);
                        case 401:
                            throw new RequestFailedException("Unathorized", c);
                    }
                    break;

                case RequestType.StudentGetByName:

                case RequestType.StudentGetSchedule:

                case RequestType.StudentGetOsCisloByStagId:

                case RequestType.StudentGetGrades:

                case RequestType.StudentGetInfo:

                case RequestType.StudentGetAbsolvedSubjects:

                case RequestType.StudentGetTerms:

                case RequestType.StudentGetByCourse:

                case RequestType.StudentGetByTerm:

                case RequestType.TeacherGetByName:

                case RequestType.TeacherGetSchedule:

                case RequestType.RoomGetFreeRoomsInBuilding:
                    switch (c)
                    {
                        case 406:
                            throw new RequestFailedException("Header paramater missing", c);
                        case 401:
                            throw new RequestFailedException("Unathorized", c);
                    }
                    break;
                case RequestType.StudentSetTerm:
                    switch (c)
                    {
                        case 493:
                            throw new RequestFailedException("Invalid term id", c);
                        case 410:
                            throw new RequestFailedException("Term status already set to this value", c);
                        case 406:
                            throw new RequestFailedException("Header paramater missing", c);
                        case 401:
                            throw new RequestFailedException("Unathorized", c);
                    }
                    break;
            }
        }
    }

    public enum RequestType
    {
        AuthLogin,
        AuthLogout,
        StudentGetByName,
        StudentGetSchedule,
        StudentGetOsCisloByStagId,
        StudentGetGrades,
        StudentGetInfo,
        StudentGetAbsolvedSubjects,
        StudentGetTerms,
        StudentSetTerm,
        StudentGetByCourse,
        StudentGetByTerm,
        TeacherGetByName,
        TeacherGetSchedule,
        RoomGetFreeRoomsInBuilding
    }

    public enum StagUserRole
    {
        Student,
        Teacher,
        Unset
    }

    public enum StagFacultyType
    {
        FSI, FZP, FUD, FF, FZS, PRF, PF, FSE, Default
    }

    public enum StagBuildingList
    {
        BR, BU, CA, CH, CN, CS, CT, CV, CZ, DD, DT, DU, EX, FF, FU, HO, K1, K2, K3, K4, K5, K6, KA, KD, KH, KJ, KL, KT, KV, L1, LT, M1, MF, MN, MO, MP, Ma, NO, PS, RE, S1, S2, SK, SP, ST, TV, UL, V1, V2, VA, VL, ZD, ZV
    }

    public class StagUser
    {
        public StagUserRole stagUserRole { get; private set; } = StagUserRole.Unset;

        public string stagId { get; private set; }

        public string stagUserFirstName { get; private set; }
        public string stagUserLastName { get; private set; }

        public Schedule stagSchedule { get; set; } = null;

        /// <summary>
        /// Stag user
        /// </summary>
        /// <param name="sur"> User role </param>
        /// <param name="sid"> User id</param>
        /// <param name="fn"> User first name</param>
        /// <param name="ln"> User last name</param>
        public StagUser(StagUserRole sur, string sid, string fn, string ln)
        {
            this.stagUserRole = sur;
            this.stagId = sid;
            this.stagUserFirstName = fn;
            this.stagUserLastName = ln.Substring(0, 1) + ln.Substring(1).ToLower();
        }

        public Color ToColor()
        {
            var value = (string)stagId + stagUserFirstName + stagUserLastName.ToUpper();

            var md5 = MD5.Create();
            var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(value));
            var color = Color.FromRgb(hash[0], hash[1], hash[2]);

            return color;
        }

        public async Task<bool> LoadSchedule(DateTime SelectedDay, bool DayMode, bool NoCreditTerms, bool OnlyUpcoming)
        {
            DateTime df = SelectedDay;
            DateTime dt = ((DayMode == true) ? df : df.AddDays(7));
            this.stagSchedule = null;
            if (this.stagUserRole == StagUserRole.Student)
            {
                Schedule lo = await Rest.CreateStudentGetScheduleRequest(this,
                    new KeyValuePair<string, string>("Datefrom", df.ToString("d.M.yyyy")),
                    new KeyValuePair<string, string>("Dateto", dt.ToString("d.M.yyyy")),
                    new KeyValuePair<string, string>("Nocreditterms", NoCreditTerms.ToString().ToLower()),
                    new KeyValuePair<string, string>("Onlyupcoming", OnlyUpcoming.ToString().ToLower())
                );
                this.stagSchedule = lo;

                return true;
            }
            else if (this.stagUserRole == StagUserRole.Teacher)
            {
                Schedule lo = await Rest.CreateTeacherGetScheduleRequest(this,
                    new KeyValuePair<string, string>("Datefrom", df.ToString("d.M.yyyy")),
                    new KeyValuePair<string, string>("Dateto", dt.ToString("d.M.yyyy")),
                    new KeyValuePair<string, string>("Nocreditterms", NoCreditTerms.ToString().ToLower()),
                    new KeyValuePair<string, string>("Onlyupcoming", OnlyUpcoming.ToString().ToLower())
                );
                this.stagSchedule = lo;

                return true;
            }

            return false;
        }
    }

    public class StagStudent : StagUser
    {
        public string stagUserName { get; private set; }
        public StagFacultyType stagUserFaculty { get; private set; }
        public string stagUserField { get; private set; }
        public int stagUserCurrentGrade { get; private set; }

        public string StudentTitleBefore { get; private set; }
        public string StudentTitleAfter { get; private set; }
        public bool StudentStudyState { get; private set; }
        public int StudentCredits { get; private set; }

        /// <summary>
        /// Student class
        /// </summary>
        /// <param name="osc"> Osobni cislo </param>
        /// <param name="fn"> First name </param>
        /// <param name="ln"> Last name </param>
        /// <param name="un"> Username  </param>
        /// <param name="ft"> Student Faculty  </param>
        /// <param name="ufi"> Student field </param>
        /// <param name="uug"> Student current grade </param>
        public StagStudent(string osc, string fn, string ln, string un, StagFacultyType ft, string ufi, int uug) : base(StagUserRole.Student, osc, fn, ln)
        {
            this.stagUserName = un;
            this.stagUserFaculty = ft;
            this.stagUserField = ufi;
            this.stagUserCurrentGrade = uug;
        }

        /// <summary>
        /// Full student class 
        /// </summary>
        /// <param name="osc"> Osobni cislo </param>
        /// <param name="fn"> First name </param>
        /// <param name="ln"> Last name </param>
        /// <param name="un"> Username  </param>
        /// <param name="ft"> Student Faculty  </param>
        /// <param name="ufi"> Student field </param>
        /// <param name="uug"> Student current grade </param>
        /// <param name="stb"> Student title before</param>
        /// <param name="sta"> Student title after</param>
        /// <param name="sss"> Is student studing</param>
        /// <param name="sc"> Students current credit count</param>
        public StagStudent(string osc, string fn, string ln, string un, StagFacultyType ft, string ufi, int uug,string stb, string sta, bool sss, int sc) : base(StagUserRole.Student, osc, fn, ln)
        {
            this.stagUserName = un;
            this.stagUserFaculty = ft;
            this.stagUserField = ufi;
            this.stagUserCurrentGrade = uug;

            this.StudentTitleBefore = stb;
            this.StudentTitleAfter = sta;
            this.StudentStudyState = sss;
            this.StudentCredits = sc;
        }
    }

    public class StagTeacher : StagUser
    {
        /// <summary>
        /// Teacher class
        /// </summary>
        /// <param name="tid"> Teacher id </param>
        /// <param name="fn"> First name </param>
        /// <param name="ln"> Last name </param>
        public StagTeacher(string tid, string fn, string ln) : base(StagUserRole.Teacher, tid, fn, ln)
        {
        }
    }

    public class StagRoom
    {
        public StagFacultyType RoomFaculty { get; private set; } = StagFacultyType.Default;
        public StagBuildingList RoomBuilding { get; private set; }
        public string RoomBuildingAdress { get; private set; }
        public string RoomNumber { get; private set; }
        public int RoomCapacity { get; private set; }
        public string RoomFloor { get; private set; }
        public bool RoomFree { get; private set; }

        /// <summary>
        /// Represents room in stag system
        /// </summary>
        /// <param name="sft">Ujep faculty type</param>
        /// <param name="sbl">Ujep building </param>
        /// <param name="rba">Ujep building location </param>
        /// <param name="rn">Room name</param>
        /// <param name="rc">Room capacity</param>
        /// <param name="rf">Room floor</param>
        /// <param name="ro">Room occupied</param>
        public StagRoom(StagFacultyType sft, StagBuildingList sbl, string rba, string rn, int rc, string rf, bool ro)
        {
            this.RoomFaculty = sft;
            this.RoomBuilding = sbl;
            this.RoomBuildingAdress = rba;
            this.RoomNumber = rn;
            this.RoomCapacity = rc;
            this.RoomFloor = rf;
            this.RoomFree = ro;
        }
    }

    public class StagCourse
    {
        public string CourseFaculty { get; private set; }
        public string CourseId { get; private set; }
        public string CourseName { get; private set; }
        public int CourseCredits { get; private set; }
        public bool CourseHasExam { get; private set; }
        public bool CourseFinished { get; private set; }

        /// <summary>
        /// Represents stag course
        /// </summary>
        /// <param name="cf">Course faculty</param>
        /// <param name="ci">Course id</param>
        /// <param name="cn">Course name</param>
        /// <param name="cc">Course credit count</param>
        /// <param name="che">If course has Exam on end</param>
        /// <param name="cfn">if the course is done</param>
        public StagCourse(string cf, string ci, string cn, int cc, bool che, bool cfn)
        {
            this.CourseFaculty = cf;
            this.CourseId = ci;
            this.CourseName = cn;
            this.CourseCredits = cc;
            this.CourseHasExam = che;
            this.CourseFinished = cfn;
        }
    }

    public class StagAbsolvedCourse
    {
        public string CourseFaculty { get; private set; }
        public string CourseId { get; private set; }
        public string CourseName { get; private set; }
        public string CourseSemester { get; private set; }
        public string CourseYear { get; private set; }
        public string CourseDateAbsolved { get; private set; }
        public bool CourseAbsolved { get; private set; }
        public string CourseGrade { get; private set; }
        public int CourseCreditCount { get; private set; }

        /// <summary>
        /// Got lazy there, its basically the same as the above but maybe better
        /// </summary>
        /// <param name="cf">CourseFaculty</param>
        /// <param name="ci">CourseId</param>
        /// <param name="cn">CourseName</param>
        /// <param name="cs">CourseSemester</param>
        /// <param name="cy">CourseYear</param>
        /// <param name="cda">CourseDateAbsolved</param>
        /// <param name="ca">CourseAbsolved</param>
        /// <param name="cg">CourseGrade</param>
        /// <param name="ccc">CourseCreditCount</param>
        public StagAbsolvedCourse(string cf, string ci, string cn, string cs, string cy, string cda, bool ca, string cg, int ccc)
        {
            this.CourseFaculty = cf;
            this.CourseId = ci;
            this.CourseName = cn;
            this.CourseSemester = cs;
            this.CourseYear = cy;
            this.CourseDateAbsolved = cda;
            this.CourseAbsolved = ca;
            this.CourseGrade = cg;
            this.CourseCreditCount = ccc;
        }
    }

    public class StagGradePart
    {
        public bool GradeByNumber { get; private set; }
        public dynamic GradeStatus { get; private set; }
        public int GradeAttempt { get; private set; }
        public string GradeAttemptDate { get; private set; }
        public StagTeacher GradeTeacher { get; private set; }

        /// <summary>
        /// Course grade part
        /// </summary>
        /// <param name="gbn">Is grade given by number or bool</param>
        /// <param name="gs">Provided grade</param>
        /// <param name="ga">Attempt at the grade</param>
        /// <param name="gad">Last attempt date</param>
        /// <param name="gt">Attempt teacher</param>
        public StagGradePart(bool gbn, dynamic gs, int ga, string gad, StagTeacher gt)
        {
            this.GradeByNumber = gbn;
            this.GradeStatus = gs;
            this.GradeAttempt = ga;
            this.GradeAttemptDate = gad;
            this.GradeTeacher = gt; 
        }
    }

    public class StagGrade
    {
        public StagCourse GradeCourse { get; private set; }
        public StagGradePart GradeAcredation { get; private set; }
        public StagGradePart GradeExam { get; private set; }

        /// <summary>
        /// Represents a grade with acredation and exam results
        /// </summary>
        /// <param name="gc">Stag course data</param>
        /// <param name="ga">Acredation grade</param>
        /// <param name="ge">Exam grade</param>
        public StagGrade(StagCourse gc, StagGradePart ga, StagGradePart ge)
        {
            this.GradeCourse = gc;
            this.GradeAcredation = ga;
            this.GradeExam = ge;
        }
    }

    public class StagTerm
    {
        public string TermId { get; private set; }
        public int TermLimit { get; private set; }
        public int TermStudentCount { get; private set; }
        public bool TermRegistered { get; private set; }
        public bool TermCanUnregister { get; private set; }
        public CourseDate TermRegistrationDeadline { get; private set; }
        public CourseDate TermUnregistrationDeadline { get; private set; }

        /// <summary>
        /// Name stands for itself doesn't it
        /// </summary>
        /// <param name="ti">TermId</param>
        /// <param name="tl">TermLimit</param>
        /// <param name="tsc">TermStudentCount</param>
        /// <param name="tr">TermRegistered</param>
        /// <param name="tcu">TermCanUnregister</param>
        /// <param name="trd">TermRegistrationDeadline</param>
        /// <param name="turd">TermUnregistrationDeadline</param>
        public StagTerm(string ti, int tl, int tsc, bool tr, bool tcu, CourseDate trd, CourseDate turd)
        {
            this.TermId = ti;
            this.TermLimit = tl;
            this.TermStudentCount = tsc;
            this.TermRegistered = tr;
            this.TermCanUnregister = tcu;
            this.TermRegistrationDeadline = trd;
            this.TermUnregistrationDeadline = turd;
        }
    }

    public class RequestFailedException : Exception
    {
        public int Code { get; set; }

        public RequestFailedException(string message, int code) : base(message)
        {
            this.Code = code;
        }
    }
}