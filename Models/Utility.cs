﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Ultimate_Schedule_Builder.Views.Components;

namespace Ultimate_Schedule_Builder.Models
{
    public static class Utility
    {
        public static DateTime FirstDayOfWeek(DateTime date)
        {
            DayOfWeek fd = CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;
            int offset = fd - date.DayOfWeek;
            DateTime fdDate = date.AddDays(offset);
            return fdDate;
        }

        public static DateTime LastDayOfWeek(DateTime date)
        {
            DateTime ldDate = FirstDayOfWeek(date).AddDays(6);
            return ldDate;
        }

        public static CourseType GetCourseType(string ct)
        {
            CourseType ret = CourseType.Pr; 

            if(ct == "Přednáška" || ct == "Př")
            {
                ret = CourseType.Pr;
            }
            else if (ct == "Cvičení" || ct == "Cv")
            {
                ret = CourseType.Cv;
            }
            else if (ct == "Seminář" || ct == "Se")
            {
                ret = CourseType.Se;
            }
            else if (ct == "Zápočet" || ct == "Zá")
            {
                ret = CourseType.Za;
            }
            else if (ct == "Zkouška" || ct == "Zk")
            {
                ret = CourseType.Zk;
            }

            return ret;
        }

        public static RenderTargetBitmap GetImage(ScheduleComponent view)
        {
            Size size = new Size(view.ActualWidth, view.ActualHeight);
            if (size.IsEmpty)
            {
                return null;
            }
                

            RenderTargetBitmap result = new RenderTargetBitmap((int)size.Width, (int)size.Height, 96, 96, PixelFormats.Pbgra32);

            DrawingVisual drawingvisual = new DrawingVisual();
            using (DrawingContext context = drawingvisual.RenderOpen())
            {
                context.DrawRectangle(new VisualBrush(view), null, new Rect(new Point(), size));
                context.Close();
            }

            result.Render(drawingvisual);
            return result;
        }

        public static void SaveAsPng(RenderTargetBitmap src, Stream outputStream)
        {
            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(src));

            encoder.Save(outputStream);
        }

    }
}
