﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ultimate_Schedule_Builder.Models
{
    public static class Settings
    {
        public static bool ScheduleStartWithStaticWeek { get; set; } = true;
        public static bool SearchRoomsShowOccupied { get; set; } = true;
        public static bool ScheduleConnectFreeHours { get; set; } = false;

        public static ApplicationColorSchema ApplicationColorSchema { get; set; } = new ApplicationColorSchema();
        public static StagUser LocalUser { get; set; } = null;

        //Language
    }
}
