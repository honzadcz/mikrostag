﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ultimate_Schedule_Builder.Views;


namespace Ultimate_Schedule_Builder.Models
{
    public class Schedule : ICloneable
    {
        public List<ScheduleDay> Courses { get; private set; }

        public int UserCount { get; private set; } = 0;

        public Schedule()
        {
            this.Courses = new List<ScheduleDay>();
        }

        public Schedule(Course[] cList)
        {
            this.Courses = new List<ScheduleDay>();

            foreach(Course c in cList.ToList())
            {
                if(this.Courses.ToList().Any(x => x.ScheduleDate.ToString("d.M.yyyy") == c.Start.ScheduleDateString))
                {
                    this.Courses.First(x => x.ScheduleDate.ToString("d.M.yyyy") == c.Start.ScheduleDateString).Add(c);
                }
                else
                {
                    ScheduleDay nd = new ScheduleDay(c.Start.ScheduleDate.Date);
                    nd.Add(c);
                    this.Courses.Add(nd);
                }
            }
            
            this.Courses.Sort((x, y) => DateTime.Compare(x.ScheduleDate, y.ScheduleDate));
            UserCount++;

        }

        public Schedule(Schedule[] sList)
        {
            this.Courses = new List<ScheduleDay>();

            foreach (Schedule s in sList)
            {
                this.Merge(s);
            }
        }

        public void Merge(Schedule s)
        {
            foreach(ScheduleDay sd in s.Courses.ToList())
            {
                if(this.Courses.ToList().Any(x => x.ScheduleDate.ToString("d.M.yyyy") == sd.ScheduleDate.ToString("d.M.yyyy")))
                {
                    ScheduleDay fd = this.Courses.First(x => x.ScheduleDate.ToString("d.M.yyyy") == sd.ScheduleDate.ToString("d.M.yyyy"));



                    foreach(List<Course> cList in sd.Courses.ToList())
                    {
                        foreach(Course c in cList.ToList())
                        {
                            if (c == null) { continue; }
                            if (c.IsEmpty) { continue; }
                            fd.Add(c);
                        }
                    }
                }
                else
                {
                    this.Courses.Add(sd);
                }
            }

            UserCount++;
            this.Courses.Sort((x, y) => DateTime.Compare(x.ScheduleDate, y.ScheduleDate));

        }

        public void Remove(StagUser o)
        {
            foreach(ScheduleDay sd in this.Courses)
            {
                sd.Remove(o);
            }
            foreach(ScheduleDay sd in this.Courses.ToList())
            {
                if(sd.CourseCount <= 0)
                {
                    this.Courses.Remove(sd);
                }
            }
            UserCount--;
        }

        public void ToHtml()
        {

        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    public class ScheduleDay
    {
        public DateTime ScheduleDate { get; private set; }

        public int CourseCount { get; private set; }
        public int MaxCoursesInHour { get; private set; }
        public int LastDayCourseHour { get; private set; }

        public List<List<Course>> Courses { get; private set; }

        public ScheduleDay(DateTime date)
        {
            this.ScheduleDate = date;
            this.CourseCount = 0;

            this.Courses = new List<List<Course>>();

            for(int i = 0; i < 15; i++)
            {
                this.Courses.Insert(i,new List<Course>());
            }
          
        }

        public ScheduleDay(DateTime date, List<Course> cList)
        {
            this.ScheduleDate = date;
            this.CourseCount = 0;
            this.Courses = new List<List<Course>>();
            for (int i = 0; i < 15; i++)
            {
                this.Courses.Insert(i, new List<Course>());
            }

            foreach(Course c in cList)
            {
                this.Add(c);
            }

        }

        public void Add(Course c)
        {
            if (!Enumerable.Range(0, 15).Contains(c.Start.SchedulePosition))
            {
                return;
            }

            this.removeEmpty();

            List<Course> cl = this.Courses.ElementAt(c.Start.SchedulePosition);
            int npi = ((cl.Count > 0) ? cl.Count : 0);
            bool rta = false;

            while (!rta)
            {
                bool ilo = false;
                for (int i = c.Start.SchedulePosition; i > -1; i--) // Go left
                {
                    List<Course> cList = this.Courses[i];

                    if (npi < cList.Count)
                    {
                        if (cList[npi] != null)
                        {
                            if (i == c.Start.SchedulePosition)
                            {
                                ilo = true;
                                break;
                            }
                            if (cList[npi].Start.SchedulePosition + cList[npi].Duration > c.Start.SchedulePosition)
                            {
                                ilo = true;
                                break;
                            }
                        }
                    }

                }
                bool iro = false;
                for (int i = c.Start.SchedulePosition; i < 15; i++) // Go right
                {
                    
                    List<Course> cList = this.Courses[i];

                    if (npi < cList.Count)
                    {
                        if (cList[npi] != null)
                        {
                            if (i == c.Start.SchedulePosition)
                            {
                                iro = true;
                                break;
                            }
                            if (c.Start.SchedulePosition + c.Duration > cList[npi].Start.SchedulePosition)
                            {
                                iro = true;
                                break;
                            }
                        }
                    }
                }

                if(iro || ilo)
                {
                    npi++;
                }
                else
                {
                    rta = true;
                }
            }

            if(!(npi < cl.Count))
            {
                int tn = ((cl.Count > 0) ? cl.Count : 0);
                for (int i = tn; i <= npi; i++)
                {
                    if(i == npi)
                    {
                        cl.Add(c);
                        this.CourseCount++;
                    }
                    else
                    {
                        cl.Add(null);
                    }
                }
            }


            if (c.Start.SchedulePosition + c.Duration > this.LastDayCourseHour)
            {
                this.LastDayCourseHour = c.Start.SchedulePosition + c.Duration;
            }

            if (npi > this.MaxCoursesInHour)
            {
                this.MaxCoursesInHour = npi;
            }

            CheckMaxCoursesInHour();
            //this.checkOverlaps();

            this.createEmpty();


            /*
            try
            {
                int pIndex = 0;
                int lastH = 0;

                this.removeEmpty();

                List<Course> cList = this.Courses.ElementAt(c.Start.SchedulePosition);
                pIndex += cList.Count;

                while (this.isOverlapped(c.Start.SchedulePosition, pIndex))
                {
                    pIndex++;
                }

                while(pIndex > cList.Count) 
                {
                    cList.Add(null);
                }

                this.Courses[c.Start.SchedulePosition].Insert(pIndex, c);
                this.CourseCount++;
                

                foreach (List<Course> cc in this.Courses.ToList())
                {
                    if(cc.Count > 0)
                    {
                        foreach (Course cr in cc.ToList())
                        {
                            if(cr == null) { continue; }

                            if (lastH < cr.Start.SchedulePosition + cr.Duration)
                            {
                                lastH = cr.Start.SchedulePosition + cr.Duration;
                            }
                        }
                    }
                    
                }

                if (lastH > this.LastDayCourseHour)
                {
                    this.LastDayCourseHour = lastH;
                }

                if (pIndex + 1 > this.MaxCoursesInHour)
                {
                    this.MaxCoursesInHour = pIndex + 1;
                }

                CheckMaxCoursesInHour();
                //this.checkOverlaps();

                this.createEmpty();
            }
            catch (ArgumentOutOfRangeException e)
            {
                throw new IndexOutOfRangeException();
            }
            */
        }

        public void Remove(StagUser s)
        {

            try
            {
                this.removeEmpty();
                foreach (List<Course> cList in this.Courses.ToList())
                {
                    int i = Array.IndexOf(this.Courses.ToArray(), cList);
                    foreach (Course c in cList.ToList())
                    {
                        if (c == null) { continue; }
                        if (c.IsEmpty) { continue; }
                        if (c.Owner.stagId == s.stagId)
                        {
                            this.Courses[i].Remove(c);
                            this.CourseCount--;
                        }
                    }
                }
                CheckMaxCoursesInHour();
                this.createEmpty();
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine("Invalid course");
            }
        }

        private void createEmpty()
        {
            foreach(List<Course> cList in this.Courses)
            {
                if(cList.Count != 0) { continue; }
                int i = Array.IndexOf(this.Courses.ToArray(), cList);

                if (!this.isOverlapped(i))
                {
                    cList.Add(new Course(new CourseDate(this.ScheduleDate.AddHours(7 + i)), 1));
                }
                else
                {
                    continue;
                }
            }
        }

        private void removeEmpty()
        {
            foreach(List<Course> cList in this.Courses.ToList())
            {
                if (cList.Count == 0) { continue; }
                int i = Array.IndexOf(this.Courses.ToArray(), cList);
                foreach(Course c in cList.ToList())
                {
                    if(c == null) { continue; }
                    if (c.IsEmpty)
                    {
                        this.Courses[i].Remove(c);
                    }
                }
            }
            CheckMaxCoursesInHour();
        }

        private void CheckMaxCoursesInHour()
        {
            int mcih = 0;
            foreach(List<Course> cList in this.Courses.ToList())
            {
                if(mcih < cList.Count)
                {
                    mcih = cList.Count;
                }
            }
            this.MaxCoursesInHour = mcih;
        }

        private void checkOverlaps()
        {
            for(int i = this.Courses.Count-1; i >= 0; i--)
            {
                List<Course> cList = this.Courses[i];

                for (int j = 0; j < cList.Count; j++)
                {
                    if(cList[j] == null) { continue; }

                    if (this.isOverlapped(i, j))
                    {
                        Course cc = cList[j];
                        cList.RemoveAt(j);

                        int nIndex = j;

                        while (this.isOverlapped(i, j))
                        {
                            nIndex++;
                        }

                        while (nIndex > cList.Count)
                        {
                            cList.Add(null);
                        }

                        cList[nIndex] = cc;

                        if (nIndex + 1 > this.MaxCoursesInHour)
                        {
                            this.MaxCoursesInHour = nIndex + 1;
                        }

                    }
                }
            }
        }

        private bool isOverlapped(int hour, int courseIndex)
        {
            if (Enumerable.Range(0, 15).Contains(hour))
            {
                if (hour == 0) { return false; }

                for (int i = hour - 1; i > -1; i--)
                {
                    if(!(courseIndex >= this.Courses[i].Count))
                    {
                        Course c = this.Courses[i].ElementAt(courseIndex);
                        if (c == null) { continue; }
                        if (c.Start.SchedulePosition + c.Duration > hour)
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
            else
            {
                throw new IndexOutOfRangeException();
            }
        }

        private bool isOverlapped(int hour)
        {
            if (Enumerable.Range(0, 15).Contains(hour))
            {
                if (hour == 0) { return false; }
                for (int i = hour - 1; i > -1; i--)
                {
                    List<Course> cList = this.Courses[i];
                    foreach (Course c in cList.ToList())
                    {
                        if (c == null) { continue; }
                        if (c.Start.SchedulePosition + c.Duration > hour)
                        {
                            return true;
                        }
                    }
                }

                return false;

            }
            else
            {
                throw new IndexOutOfRangeException();
            }
        }

    }

    public class Course
    {
        public StagUser Owner { get; private set; } = null;

        public bool IsTerm { get; private set; } = false;
        public StagTerm CourseTerm { get; private set; } = null;

        public CourseDate Start { get; private set; }
        public int Duration { get; private set; }
        public string CourseScheduleId { get; private set; }
        public string CourseId { get; private set; }
        public string CourseName { get; private set; }
        public CourseType CourseType { get; private set; }
        public StagTeacher CourseTeacher { get; private set; }
        public string CourseDepartment { get; private set; }
        public string CourseBuilding { get; private set; }
        public string CourseRoom { get; private set; }

        public bool IsEmpty { get; private set; } // In render if you render date or not
        public bool IsStatic { get; set; }
        public bool IsTimeDataMissing { get; set; }

        /// <summary>
        /// Creates empty Course (Free)
        /// </summary>
        /// <param name="s">CourseDate</param>
        /// <param name="d">duration</param>
        public Course(CourseDate s, int d)
        {
            this.Start = s;
            this.Duration = d;
            this.CourseId = "EMPTY";
            this.IsEmpty = true;
            this.CourseType = CourseType.Empty;
        }

        /// <summary>
        /// Schedule events
        /// </summary>
        /// <param name="s"> Event start date with time </param>
        /// <param name="d"> Event duration </param>
        /// <param name="csid"> Event ID </param>
        /// <param name="cid"> Event short name / ID </param>
        /// <param name="cn"> Event full name </param>
        /// <param name="ct"> Event type </param>
        /// <param name="ctea"> Event teacher </param>
        /// <param name="cd"> Event department </param>
        /// <param name="cb"> Event building </param>
        /// <param name="cr"> Event room </param>
        /// <param name="owner">Event owner</param>
        public Course(CourseDate s, int d,string csid, string cid, string cn, CourseType ct, StagTeacher ctea,string cd, string cb, string cr,StagUser owner)
        {
            this.Start = s;
            this.Duration = d;
            this.CourseScheduleId = csid;
            this.CourseId = cid;
            this.CourseName = cn;
            this.CourseType = ct;
            this.CourseTeacher = ctea;

            this.CourseDepartment = cd;
            this.CourseBuilding = cb;
            this.CourseRoom = cr;
            this.IsEmpty = false;
            this.Owner = owner;
        }

        /// <summary>
        /// Schedule Term
        /// </summary>
        /// <param name="s"> Event start date with time </param>
        /// <param name="d"> Event duration </param>
        /// <param name="cid"> Event short name / ID </param>
        /// <param name="cn"> Event full name </param>
        /// <param name="ct"> Event type </param>
        /// <param name="ctea"> Event teacher </param>
        /// <param name="cd"> Event department </param>
        /// <param name="cb"> Event building </param>
        /// <param name="cr"> Event room </param>
        /// <param name="owner">Event owner</param>
        /// <param name="sterm">Event term info</param>
        public Course(CourseDate s, int d, string cid, string cn, CourseType ct, StagTeacher ctea, string cd, string cb, string cr, StagUser owner,StagTerm sterm)
        {
            this.Start = s;
            this.Duration = d;
            this.CourseId = cid;
            this.CourseName = cn;
            this.CourseType = ct;
            this.CourseTeacher = ctea;

            this.CourseDepartment = cd;
            this.CourseBuilding = cb;
            this.CourseRoom = cr;
            this.IsEmpty = false;
            this.Owner = owner;

            this.IsTerm = true;
            this.CourseTerm = sterm;
        }
    }

    public class CourseDate
    {
        public int Year { get; private set; }
        public DayOfWeek Day { get; private set; }
        public int Hour { get; private set; }
        public int Minute { get; private set; }
        public DateTime ScheduleDate { get; private set; }

        public int SchedulePosition { get; private set; }

        public string ScheduleDateString
        {
            get
            {
                return ScheduleDate.ToString("d.M.yyyy");
            }
        }

        public CourseDate(DateTime t)
        {
            this.Year = t.Year;
            this.Day = t.DayOfWeek;
            this.Hour = t.Hour;
            this.Minute = t.Minute;

            this.SchedulePosition = getSubjectPosition(t.Hour);
            this.ScheduleDate = t;
        }

        private static int getSubjectPosition(int startHour)
        {
            DateTime d = new DateTime(1, 1, 1, 7, 0, 0);          
            DateTime s = new DateTime(1, 1, 1, startHour, 0, 0);

            return ((TimeSpan)(s - d)).Hours;
        }
    }

    public enum CourseType
    {
        Pr,
        Cv,
        Se,
        Za,
        Zk,
        Empty
    }

    public enum RoomType
    {
        Unfilled,
        ItsNot,
        Schoolroom,
        DrawingRoom,
        Laboratory,
        Other,
        Cabinet,
        Gym,
        Atelier,
        Office
    }
}
