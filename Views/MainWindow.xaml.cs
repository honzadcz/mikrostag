﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using System.Threading;
using Ultimate_Schedule_Builder.WindowFunc;
using Ultimate_Schedule_Builder.Views;

using Newtonsoft.Json;

using Ultimate_Schedule_Builder.Models;
using System.ComponentModel;
using Ultimate_Schedule_Builder.Views.Main;

namespace Ultimate_Schedule_Builder
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        //? Main controlls
        public MainView MV { get; private set; } = null;

        private int transitionSlideCount = 0;
        public int TransitionSlideCount
        {
            get
            {
                return transitionSlideCount;
            }
            set
            {
                transitionSlideCount = value;
                RaisePropertyChanged("TransitionSlideCount");
            }
        }

        private LoginView loginView = null;

        public bool IsUserLoggedIn
        {
            get => ((Settings.LocalUser != null) ? true : false);
        }

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            var resizer = new WindowResizer(this);

            App.Current.Exit += Current_Exit;


            LoginView v = new LoginView();
            loginView = v;

            this.LoginViewModule.Content = loginView;

            TransitionSlideCount = 0;

        }

        private void setUpNewUser(StagUserRole ut, dynamic uo)
        {
            if(ut == StagUserRole.Student)
            {
                StagStudent st = (StagStudent)uo;
                Settings.LocalUser = st;
                Settings.ApplicationColorSchema = getColorSchemeFromFacultyType(st.stagUserFaculty);
            }
            else if(ut == StagUserRole.Teacher)
            {
                StagTeacher st = (StagTeacher)uo;
                Settings.LocalUser = st;
                Settings.ApplicationColorSchema = getColorSchemeFromFacultyType(StagFacultyType.Default);
            }

            loadLastSession();

            loginView.ChangeColorSchema();
        }

        public void changeColorScheme() //! Do something
        {

        }
        

        private ApplicationColorSchema getColorSchemeFromFacultyType(StagFacultyType f)
        {
            ApplicationColorSchema ret = new ApplicationColorSchema();

            switch (f)
            {
                case StagFacultyType.FSI:
                    ret.MainColor = new SolidColorBrush(Color.FromRgb(95,87,84));

                    break;
                case StagFacultyType.FZP:
                    ret.MainColor = new SolidColorBrush(Color.FromRgb(89,191,56));

                    break;
                case StagFacultyType.FUD:
                    ret.MainColor = new SolidColorBrush(Color.FromRgb(255,192,1));

                    break;
                case StagFacultyType.FF:
                    ret.MainColor = new SolidColorBrush(Color.FromRgb(245,111,0));

                    break;
                case StagFacultyType.FZS:
                    ret.MainColor = new SolidColorBrush(Color.FromRgb(217,0,9));

                    break;
                case StagFacultyType.PRF:
                    ret.MainColor = new SolidColorBrush(Color.FromRgb(0,127,136));

                    break;
                case StagFacultyType.PF:
                    ret.MainColor = new SolidColorBrush(Color.FromRgb(241,45,153));

                    break;
                case StagFacultyType.FSE:
                    ret.MainColor = new SolidColorBrush(Color.FromRgb(9,152,218));

                    break;
                case StagFacultyType.Default:
                    ret.MainColor = new SolidColorBrush(Color.FromRgb(95,151,251));

                    break;
            }

            return ret;
        }

        private void loadLastSession() //! Do something
        {
            if(Settings.LocalUser.stagUserRole == StagUserRole.Student)
            {

            }
            else if (Settings.LocalUser.stagUserRole == StagUserRole.Teacher)
            {

            }
        }

        public void OnLoginSuccess(StagUserRole ut, dynamic uo)
        {
            setUpNewUser(ut, uo);

            MainView mv = new MainView();
            this.ScheduleViewModule.Content = mv;

            MV = mv;
        }

        public void OnLoginSuccess()
        {
            TransitionSlideCount = 1;
        }

        public void OnLogoutSuccess()
        {
            Settings.ApplicationColorSchema = new ApplicationColorSchema();

            TransitionSlideCount = 0;
            loginView.ChangeColorSchema();

            Settings.LocalUser = null;
        }

        private void CloseApplication(object sender, RoutedEventArgs e)
        {
            App.Current.MainWindow.Close();
        }

        private void MaximizeApplication(object sender, RoutedEventArgs e)
        {
            App.Current.MainWindow.WindowState = App.Current.MainWindow.WindowState == System.Windows.WindowState.Maximized ? System.Windows.WindowState.Normal : System.Windows.WindowState.Maximized;

        }

        private void MinimalizeApplication(object sender, RoutedEventArgs e)
        {
            App.Current.MainWindow.WindowState = System.Windows.WindowState.Minimized;
        }

        private async void Current_Exit(object sender, ExitEventArgs e)
        {
            if (IsUserLoggedIn)
            {
                try
                {
                    bool lo = await Rest.CreateAuthLogoutRequest();
                    Settings.LocalUser = null;
                }
                catch (RequestFailedException ex)
                {

                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        private void RaisePropertyChanged(string propertyName)
        {
            var handlers = PropertyChanged;

            handlers(this, new PropertyChangedEventArgs(propertyName));
        }

    }

    public class ApplicationColorSchema
    {
        public SolidColorBrush MainColor { get; set; } = new SolidColorBrush(Color.FromRgb(95, 151, 251));
        public SolidColorBrush MainDisababledColor { get; set; } = new SolidColorBrush(Color.FromRgb(101,108,126));


    }
}
