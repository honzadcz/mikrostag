﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using MaterialDesignThemes.Wpf;

namespace Ultimate_Schedule_Builder.Views
{
    /// <summary>
    /// Interaction logic for MenuSubItemView.xaml
    /// </summary>
    public partial class MenuSubItemView : UserControl, INotifyPropertyChanged
    {
        private SubItemType subMenuType;
        public SubItemType SubMenuType
        {
            get { return subMenuType; }
            set
            {
                subMenuType = value;
                RaisePropertyChanged("SubMenuType");
            }
        }

        private string subMenuText;
        public string SubMenuText
        {
            get { return subMenuText; }
            set
            {
                subMenuText = value;
                RaisePropertyChanged("SubMenuText");
            }
        }

        //? ScheduleUser

        private string userId;
        public string UserId
        {
            get { return userId; }
            set
            {
                userId = value;
                RaisePropertyChanged("UserId");
            }
        }

        private bool isScheduleActive;
        public bool IsScheduleActive
        {
            get { return isScheduleActive; }
            set
            {
                isScheduleActive = value;
                RaisePropertyChanged("IsScheduleActive");
            }
        }

        private string scheduleUserRole;
        public string ScheduleUserRole
        {
            get { return scheduleUserRole; }
            set
            {
                scheduleUserRole = value;
                RaisePropertyChanged("ScheduleUserRole");
            }
        }

        //? ToolItem

        private string toolId;
        public string ToolId
        {
            get { return toolId; }
            set
            {
                toolId = value;
                RaisePropertyChanged("ToolId");
            }
        }

        private PackIconKind toolIconType;
        public PackIconKind ToolIconType
        {
            get { return toolIconType; }
            set
            {
                toolIconType = value;
                RaisePropertyChanged("ToolIconType");
            }
        }

        //? SettingItem

        private string settingId;
        public string SettingId
        {
            get { return settingId; }
            set
            {
                settingId = value;
                RaisePropertyChanged("SettingId");
            }
        }

        private bool isSettingActive;
        public bool IsSettingActive
        {
            get { return isSettingActive; }
            set
            {
                isSettingActive = value;
                RaisePropertyChanged("IsSettingActive");
            }
        }

        public dynamic DataStorage;

        public MenuSubItemView()
        {
            InitializeComponent();
            DataContext = this;

        }

        public void SetUp(SubItemType t, params Tuple<string,object>[] values)
        {
            SubMenuType = t;
            switch (t)
            {
                case SubItemType.ScheduleUser:
                    IsScheduleActive = false;
                    GridScheduleUser.Visibility = Visibility.Visible;
                    UserScheduleUserIcon.Foreground = new SolidColorBrush(Color.FromRgb(117, 117, 117));
                    UserScheduleUserName.Foreground = new SolidColorBrush(Color.FromRgb(117, 117, 117));

                    foreach (Tuple<string,object> v in values)
                    {
                        switch (v.Item1) 
                        {
                            case "UserId":
                                UserId = v.Item2 as string;
                                break;
                            case "UserText":
                                SubMenuText = v.Item2 as string;
                                break;
                            case "UserRole":
                                ScheduleUserRole = v.Item2 as string;
                                break;
                        }
                    }

                    break;
                case SubItemType.Tool:
                    GridTool.Visibility = Visibility.Visible;
                    foreach (Tuple<string, object> v in values)
                    {
                        switch (v.Item1)
                        {
                            case "ToolId":
                                ToolId = v.Item2 as string;
                                break;
                            case "ToolText":
                                SubMenuText = v.Item2 as string;
                                break;
                            case "ToolIcon":
                                ToolIconType = (PackIconKind)v.Item2;
                                break;
                        }
                    }

                    break;
                case SubItemType.Setting:
                    GridSetting.Visibility = Visibility.Visible;
                    foreach (Tuple<string, object> v in values)
                    {
                        switch (v.Item1)
                        {
                            case "SettingId":
                                SettingId = v.Item2 as string;
                                break;
                            case "SettingText":
                                SubMenuText = v.Item2 as string;
                                break;
                            case "SettingDefaultActive":
                                SettingToggle.IsChecked = (bool)v.Item2;
                                break;

                        }
                    }

                    break;
            }
        }

        private void SettingToggle_Click(object sender, RoutedEventArgs e)
        {
            ToggleButton tb = (ToggleButton)sender;
            IsSettingActive = tb.IsChecked.Value;
        }

        private void ToolActivate_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void UserScheduleActivate_Click(object sender, RoutedEventArgs e)
        {
            IsScheduleActive = !isScheduleActive;
            if (IsScheduleActive)
            {
                UserScheduleUserIcon.Foreground = Brushes.White;
                UserScheduleUserName.Foreground = Brushes.White;
            }
            else
            {
                UserScheduleUserIcon.Foreground = new SolidColorBrush(Color.FromRgb(117, 117, 117));
                UserScheduleUserName.Foreground = new SolidColorBrush(Color.FromRgb(117, 117, 117));
            }
        }

        private void UserScheduleRemove_Click(object sender, RoutedEventArgs e)
        {

        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        private void RaisePropertyChanged(string propertyName)
        {
            var handlers = PropertyChanged;

            handlers(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class SubItemTypeToVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            SubItemType authenticationEnum = (SubItemType)parameter;
            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public enum SubItemType
    {
        ScheduleUser,
        Tool,
        Setting
    }
}
