﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


using MaterialDesignThemes.Wpf;
using Ultimate_Schedule_Builder.Models;

namespace Ultimate_Schedule_Builder.Views.Menu
{
    public partial class MainMenuComponent : UserControl
    {
        public delegate void ButtonPressedHandler (MenuCategories b);
        public event ButtonPressedHandler OnButtonPressed;


        private ApplicationColorSchema currentSchema;
        private Dictionary<string, List<MainMenuItem>> menuItems = new Dictionary<string, List<MainMenuItem>>()
        {
            {
                "MAIN", new List<MainMenuItem>()
                {
                    new MainMenuItem(){ItemContent="My Semester",ItemShort=MenuCategories.MySemester ,ItemIconKind=PackIconKind.School,ItemActive=true}, 
                    new MainMenuItem(){ItemContent="Registration",ItemShort=MenuCategories.Registration ,ItemIconKind=PackIconKind.ClipboardCheck},
                    new MainMenuItem(){ItemContent="Personal info",ItemShort=MenuCategories.PersonalInfo ,ItemIconKind=PackIconKind.ContactMail}
                }
            },
            {
                "TOOLS", new List<MainMenuItem>()
                {
                    new MainMenuItem(){ItemContent="Tools",ItemShort=MenuCategories.Tools ,ItemIconKind=PackIconKind.Archive},
                    new MainMenuItem(){ItemContent="Settings",ItemShort=MenuCategories.Settings ,ItemIconKind=PackIconKind.Settings}
                }
            },
            {
                "OTHER", new List<MainMenuItem>()
                {
                    new MainMenuItem(){ItemContent="Logout",ItemShort=MenuCategories.Logout ,ItemIconKind=PackIconKind.ExitToApp}
                }
            }
        };


        public MainMenuComponent()
        {
            InitializeComponent();
            DataContext = this;

            currentSchema = Settings.ApplicationColorSchema;

            createButtons();
            RecolorItems();
        }

        private void createButtons()
        {
            MenuItemsHolder.Children.Clear();

            ApplicationColorSchema e = currentSchema;

            MenuHeaderName.Background = e.MainColor;

            foreach (KeyValuePair<string, List<MainMenuItem>> Cat in menuItems.AsEnumerable())
            {
                Grid catName = new Grid();
                catName.Height = 35;
                catName.HorizontalAlignment = HorizontalAlignment.Stretch;

                TextBlock catNameText = new TextBlock();
                catNameText.Text = Cat.Key;
                catNameText.FontSize = 8;
                catNameText.HorizontalAlignment = HorizontalAlignment.Left;
                catNameText.VerticalAlignment = VerticalAlignment.Bottom;
                catNameText.Margin = new Thickness(25, 0, 0, 8);
                catNameText.Foreground = e.MainDisababledColor;
                catName.Children.Add(catNameText);

                Rectangle catNameDivider = new Rectangle();
                catNameDivider.VerticalAlignment = VerticalAlignment.Bottom;
                catNameDivider.Height = 1;
                catNameDivider.Fill = e.MainDisababledColor;
                catName.Children.Add(catNameDivider);

                MenuItemsHolder.Children.Add(catName);

                foreach (MainMenuItem Itm in Cat.Value)
                {
                    Grid itmName = new Grid();
                    itmName.Height = 35;
                    itmName.HorizontalAlignment = HorizontalAlignment.Stretch;

                    PackIcon itmIcon = new PackIcon();
                    itmIcon.Kind = Itm.ItemIconKind;
                    itmIcon.Width = 18;
                    itmIcon.Height = 18;
                    itmIcon.HorizontalAlignment = HorizontalAlignment.Left;
                    itmIcon.VerticalAlignment = VerticalAlignment.Center;
                    itmIcon.Margin = new Thickness(25, 0, 0, 0);
                    itmIcon.Foreground = e.MainDisababledColor;

                    Itm.pi = itmIcon;
                    
                    itmName.Children.Add(itmIcon);

                    TextBlock itmNameText = new TextBlock();
                    itmNameText.Text = Itm.ItemContent;
                    itmNameText.FontSize = 10;
                    itmNameText.HorizontalAlignment = HorizontalAlignment.Left;
                    itmNameText.VerticalAlignment = VerticalAlignment.Center;
                    itmNameText.Margin = new Thickness(50, 0, 0, 0);
                    itmNameText.Foreground = e.MainDisababledColor;

                    Itm.tb = itmNameText;

                    itmName.Children.Add(itmNameText);

                    Button itmButton = new Button();
                    itmButton.BorderBrush = Brushes.Transparent;
                    itmButton.Background = Brushes.Transparent;
                    itmButton.VerticalAlignment = VerticalAlignment.Stretch;
                    itmButton.HorizontalAlignment = HorizontalAlignment.Stretch;
                    itmButton.Style = (Style)FindResource("MaterialDesignRaisedButton");
                    itmButton.Click += (obj, arg) =>
                    {
                        menuButtonPressed(Itm.ItemShort);
                    };

                    itmName.Children.Add(itmButton);
                    MenuItemsHolder.Children.Add(itmName);
                }

            }

        }

        private void RecolorItems()
        {
            MenuHeaderName.Background = currentSchema.MainColor;

            foreach (KeyValuePair<string, List<MainMenuItem>> Cat in menuItems.AsEnumerable())
            {
                foreach (MainMenuItem Itm in Cat.Value)
                {
                    if (Itm.ItemActive)
                    {
                        Itm.pi.Foreground = currentSchema.MainColor;
                        Itm.tb.Foreground = currentSchema.MainColor;
                    }
                    else
                    {
                        Itm.pi.Foreground = currentSchema.MainDisababledColor;
                        Itm.tb.Foreground = currentSchema.MainDisababledColor;
                    }
                }
            }

           
        }

        public void changeCurrentColorSchema(ApplicationColorSchema ns) //! Animated translation?
        {
            currentSchema = ns;
            RecolorItems();
        }

        private void menuButtonPressed(MenuCategories b)
        {
            foreach (KeyValuePair<string, List<MainMenuItem>> Cat in menuItems.AsEnumerable())
            {
                foreach (MainMenuItem Itm in Cat.Value)
                {
                    if(Itm.ItemShort == b)
                    {
                        if (!Itm.ItemActive)
                        {
                            Itm.ItemActive = true;
                            RaiseButtonPressed(b);
                        }
                    }
                    else
                    {
                        Itm.ItemActive = false;
                    }
                }
            }

            RecolorItems();
        }

        private void RaiseButtonPressed(MenuCategories b)
        {
            if(OnButtonPressed == null) { return; }
            OnButtonPressed(b);
        }

    }

    public class MainMenuItem
    {
        public MenuCategories ItemShort { get; set; }
        public string ItemContent { get;  set; }
        public PackIconKind ItemIconKind { get;  set; }
        public bool ItemActive { get; set; } = false;

        public PackIcon pi;
        public TextBlock tb;

    }

    public class MenuButtonEventArgs : EventArgs
    {
        public string Status { get; private set; }

        MenuCategories ButtonType { get; set; }

        public MenuButtonEventArgs(MenuCategories b)
        {
            ButtonType = b;
        }
    }

    public enum MenuCategories 
    {
        MySemester,

        Registration,
        PersonalInfo,

        Tools,
        Settings,
        Logout
    }
}
