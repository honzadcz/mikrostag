﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ultimate_Schedule_Builder.Views
{
    public partial class MenuItemView : UserControl, INotifyPropertyChanged
    {
        private string buttonName;
        public string ButtonName
        {
            get { return buttonName; }
            set
            {
                buttonName = value;
                RaisePropertyChanged("ButtonName");
            }
        }

        private string badgeContent;
        public string BadgeContent
        {
            get { return badgeContent; }
            set
            {
                badgeContent = value;
                RaisePropertyChanged("BadgeContent");
            }
        }

        private int itemCount = 0;

        private PackIconKind buttonIcon;
        public PackIconKind ButtonIcon
        {
            get { return buttonIcon; }
            set
            {
                buttonIcon = value;
                RaisePropertyChanged("ButtonIcon");
            }
        }

        private bool isActive;
        public bool IsActive
        {
            get { return isActive; }
            set
            {
                isActive = value;
                RaisePropertyChanged("IsActive");
            }
        }

        public MenuItemView()
        {
            InitializeComponent();
            DataContext = this;

            IsActive = false;
            ButtonName = "Test";
            BadgeContent = "";
            ButtonIcon = PackIconKind.TestTube;
        }

        public void SetUp(string bn, PackIconKind bi)
        {
            ButtonName = bn;
            ButtonIcon = bi;
        }

        private void CurrentButton_Click(object sender, RoutedEventArgs e)
        {
            IsActive = !isActive;

            if (isActive)
            {
                Activate();
            }
            else
            {
                Deactivate();
            }
        }

        public void ButtonIncrement()
        {
            this.itemCount++;
            if (itemCount == 0)
            {
                BadgeContent = "";
            }
            else
            {
                BadgeContent = itemCount.ToString();
            }
        }

        public void ButtonDecrement()
        {
            this.itemCount--;
            if (itemCount == 0)
            {
                BadgeContent = "";
            }
            else
            {
                BadgeContent = itemCount.ToString();
            }
        }

        public void Activate()
        {
            IsActive = true;
            ButtonPackIcon.Foreground = new SolidColorBrush(Color.FromRgb(96, 151, 251));
            ButtonNameText.Foreground = new SolidColorBrush(Color.FromRgb(96, 151, 251));
        }

        public void Deactivate()
        {
            IsActive = false;
            ButtonPackIcon.Foreground = new SolidColorBrush(Color.FromRgb(117,117,117));
            ButtonNameText.Foreground = new SolidColorBrush(Color.FromRgb(117, 117, 117));
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        private void RaisePropertyChanged(string propertyName)
        {
            var handlers = PropertyChanged;

            handlers(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
