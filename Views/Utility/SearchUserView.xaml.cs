﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Ultimate_Schedule_Builder.Models;

namespace Ultimate_Schedule_Builder.Views
{
    /// <summary>
    /// Interaction logic for SearchUserView.xaml
    /// </summary>
    public partial class SearchUserView : UserControl, INotifyPropertyChanged
    {
        private string userNameText;
        public string UserNameText
        {
            get { return userNameText; }
            set
            {
                userNameText = value;
                RaisePropertyChanged("UserNameText");
            }
        }

        private List<StagStudent> lastStudentFoundList;
        public List<StagStudent> LastStudentFoundList
        {
            get { return lastStudentFoundList ?? (lastStudentFoundList = new List<StagStudent>());  }
            set
            {
                lastStudentFoundList = value;
                RaisePropertyChanged("LastStudentFoundList");
            }
        }

        private List<StagTeacher> lastTeacherFoundList;
        public List<StagTeacher> LastTeacherFoundList
        {
            get { return lastTeacherFoundList ?? (lastTeacherFoundList = new List<StagTeacher>()); }
            set
            {
                lastTeacherFoundList = value;
                RaisePropertyChanged("LastTeacherFoundList");
            }
        }

        private ObservableCollection<StagUser> userList = new ObservableCollection<StagUser>();
        public ObservableCollection<StagUser> UserList
        {
            get { return userList; }
            set
            {
                userList = value;
                RaisePropertyChanged("UserList");
            }
        }

        private QueryType selectedQuery = QueryType.Student;
        public QueryType SelectedQuery
        {
            get { return selectedQuery; }
            set
            {
                selectedQuery = value;
                RaisePropertyChanged("SelectedQuery");
            }
        }

        private List<KeyValuePair<StagUserRole,dynamic>> usersToAdd;
        public List<KeyValuePair<StagUserRole, dynamic>> UsersToAdd
        {
            get { return usersToAdd ?? (usersToAdd = new List<KeyValuePair<StagUserRole, dynamic>>()); }
            set
            {
                usersToAdd = value;
                RaisePropertyChanged("UsersToAdd");
            }
        }

        private List<dynamic> usersToAddReference = new List<dynamic>();

        public int RemainingSlots;

        public List<dynamic> ReturnList;

        public SearchUserView()
        {
            InitializeComponent();
            DataContext = this;

            ButtonSearch.IsEnabled = false;

            ButtonAccept.IsEnabled = false;

            UserNameInput.TextChanged += (e, a) =>
            {
                ButtonSearch.IsEnabled = CanSearch();
            };
        }

        public void SetUp(int remaining)
        {
            RemainingSlots = remaining;
        }

        public bool CanSearch()
        {
            if (!String.IsNullOrWhiteSpace(UserNameInput.Text))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CanAdd()
        {
            if(UsersToAdd.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CanAddUser()
        {
            if(RemainingSlots - 1 >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private async void ButtonSearch_Click(object sender, RoutedEventArgs e)
        {
            if (CanSearch())
            {
                ClearUserList();

                LoadingView lv = new LoadingView();
                string[] splitName = UserNameText.Split(' ');
                string fn;
                string ln;
                if (splitName.Length < 2)
                {
                    fn = splitName[0];
                    ln = "";
                }
                else
                {
                    fn = splitName[0];
                    ln = splitName[1];
                }
                

                switch (SelectedQuery)
                {
                    case QueryType.Student:
                        var sr = await LoadingDialog.ShowDialog(lv, delegate (object s, DialogOpenedEventArgs a)
                        {
                            GetStudentsRequest(lv, fn, ln);
                        });
                        break;
                    case QueryType.Teacher:
                        var tr = await LoadingDialog.ShowDialog(lv, delegate (object s, DialogOpenedEventArgs a)
                        {
                            GetTeacherRequest(lv, fn, ln);
                        });
                        break;
                    case QueryType.Both:
                        var br = await LoadingDialog.ShowDialog(lv, delegate (object s, DialogOpenedEventArgs a)
                        {
                            GetBothRequest(lv, fn, ln);
                        });
                        break;
                }
            }
        }

        private void ClearUserList()
        {
            if (UsersToAdd.Count > 0)
            {
                List<KeyValuePair<StagUserRole, dynamic>> uta = UsersToAdd.ToList();
                foreach(StagUser su in UserList.ToList())
                {
                    bool isInUTA = false;
                    foreach(KeyValuePair<StagUserRole, dynamic> u in uta)
                    {
                        StagUser ut = u.Value;
                        if(ut.stagId == su.stagId)
                        {
                            isInUTA = true;
                            break;
                        }
                    }
                    if (!isInUTA)
                    {
                        UserList.Remove(su);
                    }
                }
            }
            else
            {
                UserList = new ObservableCollection<StagUser>();
            }
        }

        private async void GetStudentsRequest(LoadingView lv, string n, string s)
        {
            try
            {
                StagStudent[] res = await Rest.CreateStudentGetByNameRequest(n, s);
                LastStudentFoundList = new List<StagStudent>(res);
                
                foreach(StagStudent st in res)
                {
                    bool duplicate = false;
                    foreach(StagUser u in UserList.ToList())
                    {
                        if(u.stagId == st.stagId)
                        {
                            duplicate = true;
                            break;
                        }
                    }
                    if (!duplicate)
                    {
                        UserList.Add(st);
                    }
                }

                lv.OnRequestSuccess();
            }
            catch (RequestFailedException e)
            {
                LastStudentFoundList = new List<StagStudent>();
                lv.OnRequestFailed(new Tuple<int, string>(e.Code, e.Message));
            }
        }

        private async void GetTeacherRequest(LoadingView lv, string n, string s)
        {
            try
            {
                StagTeacher[] res = await Rest.CreateTeacherGetByNameRequest(n, s);
                LastTeacherFoundList = new List<StagTeacher>(res);

                foreach (StagTeacher st in res)
                {
                    bool duplicate = false;
                    foreach (StagUser u in UserList.ToList())
                    {
                        if (u.stagId == st.stagId)
                        {
                            duplicate = true;
                            break;
                        }
                    }
                    if (!duplicate)
                    {
                        UserList.Add(st);
                    }
                }

                lv.OnRequestSuccess();
            }
            catch (RequestFailedException e)
            {
                LastTeacherFoundList = new List<StagTeacher>();
                lv.OnRequestFailed(new Tuple<int, string>(e.Code, e.Message));
            }
        }

        private async void GetBothRequest(LoadingView lv, string n, string s)
        {

            bool oneFailed = false;
            try
            {
                StagStudent[] rss = await Rest.CreateStudentGetByNameRequest(n, s);
                LastStudentFoundList = new List<StagStudent>(rss);
                foreach (StagStudent st in rss)
                {
                    bool duplicate = false;
                    foreach (StagUser u in UserList.ToList())
                    {
                        if (u.stagId == st.stagId)
                        {
                            duplicate = true;
                            break;
                        }
                    }
                    if (!duplicate)
                    {
                        UserList.Add(st);
                    }
                }
            }
            catch (RequestFailedException e)
            {
                LastStudentFoundList = new List<StagStudent>();
                oneFailed = true;
            }



            try
            {
                StagTeacher[] rst = await Rest.CreateTeacherGetByNameRequest(n, s);
                LastTeacherFoundList = new List<StagTeacher>(rst);
                foreach (StagTeacher st in rst)
                {
                    bool duplicate = false;
                    foreach (StagUser u in UserList.ToList())
                    {
                        if (u.stagId == st.stagId)
                        {
                            duplicate = true;
                            break;
                        }
                    }
                    if (!duplicate)
                    {
                        UserList.Add(st);
                    }
                }

                lv.OnRequestSuccess();
            }
            catch (RequestFailedException e)
            {
                LastTeacherFoundList = new List<StagTeacher>();

                if (oneFailed)
                {
                    lv.OnRequestFailed(new Tuple<int, string>(e.Code, e.Message));
                }
                else
                {
                    lv.OnRequestSuccess();
                }
                
            }

            
        }

        private void UserAddToggle_Click(object sender, RoutedEventArgs e)
        {
            ToggleButton t = (ToggleButton)sender;
            PackIcon p = (PackIcon)VisualTreeHelper.GetChild(t.Parent, 0);

            if (!CanAddUser())
            {
                t.IsChecked = false;
                return;
            }
            else
            {
                RemainingSlots--;
            }

            //Console.WriteLine($"{UsersToAdd.Count}");

            Enum.TryParse<StagUserRole>((string)p.DataContext,out StagUserRole sr);
            if(sr == StagUserRole.Student)
            {
                string uid = (string)t.DataContext;
                

                foreach (StagUser st in LastStudentFoundList)
                {
                    if (st.stagId == uid)
                    {
                        KeyValuePair<StagUserRole, dynamic> val = new KeyValuePair<StagUserRole, dynamic>(sr, st);

                        if (t.IsChecked.Value)
                        {
                            //! st.setToAdd = true;
                            UsersToAdd.Add(val);
                        }
                        else
                        {
                            //! st.setToAdd = false;
                            UsersToAdd.Remove(val);
                        }
                        break;
                    }
                }
                foreach(KeyValuePair<StagUserRole, dynamic> st in UsersToAdd.ToList())
                {
                    if(((StagUser)st.Value).stagId == uid)
                    {
                        if (!t.IsChecked.Value)
                        {
                            UsersToAdd.Remove(st);
                        }
                    }
                }


            }
            else
            {
                int.TryParse((string)t.DataContext, out int uid);

                foreach (StagUser st in LastTeacherFoundList)
                {
                    if(st.stagId == uid.ToString())
                    {
                        KeyValuePair<StagUserRole, dynamic> val = new KeyValuePair<StagUserRole, dynamic>(sr, st);

                        if (t.IsChecked.Value)
                        {
                            //! st.setToAdd = true;
                            UsersToAdd.Add(val);
                        }
                        else
                        {
                            //! st.setToAdd = false;
                            UsersToAdd.Remove(val);
                        }
                    }
                }
                foreach (KeyValuePair<StagUserRole, dynamic> st in UsersToAdd.ToList())
                {
                    if (((StagUser)st.Value).stagId == uid.ToString())
                    {
                        if (!t.IsChecked.Value)
                        {
                            UsersToAdd.Remove(st);
                        }
                    }
                }

            }
            //Console.WriteLine($"{UsersToAdd.Count}");


            ButtonAccept.IsEnabled = CanAdd();



            //Console.WriteLine($"{(string)p.DataContext} - {(string)t.DataContext} toggled {(t.IsChecked == true ? "On" : "Off")}");
        }

        private void ButtonAccept_Click(object sender, RoutedEventArgs e)
        {
            List<dynamic> ret = new List<dynamic>();

            foreach (KeyValuePair<StagUserRole, dynamic> k in UsersToAdd)
            {
                ret.Add(k.Value);
            }

            ReturnList = ret;

            if (this.CloseDialog.Command.CanExecute(ret))
            {
                this.CloseDialog.Command.Execute(ret);
            }
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            List<dynamic> ret = new List<dynamic>();

            if (this.CloseDialog.Command.CanExecute(ret))
            {
                this.CloseDialog.Command.Execute(ret);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        private void RaisePropertyChanged(string propertyName)
        {
            var handlers = PropertyChanged;

            handlers(this, new PropertyChangedEventArgs(propertyName));
        }

    }

    public enum QueryType
    {
        Student,
        Teacher,
        Both
    }

    public class EnumMatchToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
                              object parameter, CultureInfo culture)
        {
            if (value == null || parameter == null)
                return false;

            string checkValue = value.ToString();
            string targetValue = parameter.ToString();
            return checkValue.Equals(targetValue,
                     StringComparison.InvariantCultureIgnoreCase);
        }

        public object ConvertBack(object value, Type targetType,
                                  object parameter, CultureInfo culture)
        {
            if (value == null || parameter == null)
                return null;

            bool useValue = (bool)value;
            string targetValue = parameter.ToString();
            if (useValue)
                return Enum.Parse(targetType, targetValue);

            return null;
        }
    }

}
