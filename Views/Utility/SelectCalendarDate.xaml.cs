﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ultimate_Schedule_Builder.Views
{
    /// <summary>
    /// Interaction logic for SelectCalendarDate.xaml
    /// </summary>
    public partial class SelectCalendarDate : UserControl, INotifyPropertyChanged
    {
        private DateTime currentDate;
        public DateTime CurrentDate
        {
            get => currentDate;
            set
            {
                currentDate = value;
                RaisePropertyChanged("CurrentDate");
            }
        }


        public SelectCalendarDate()
        {
            InitializeComponent();
            DataContext = this;
            CurrentDate = DateTime.Today; 

        }

        private void AcceptButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.CloseDialog.Command.CanExecute(CurrentDate))
            {
                this.CloseDialog.Command.Execute(CurrentDate);
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.CloseDialog.Command.CanExecute(null))
            {
                this.CloseDialog.Command.Execute(null);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        private void RaisePropertyChanged(string propertyName)
        {
            var handlers = PropertyChanged;

            handlers(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Calendar_DisplayDateChanged(object sender, CalendarDateChangedEventArgs e)
        {
            Console.WriteLine($"{Calendar.DisplayDate.ToString()}   {Calendar.SelectedDate.ToString()}");
            if(Calendar.SelectedDate != null)
            CurrentDate = Calendar.SelectedDate.Value;
        }

        private void Calendar_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Console.WriteLine($"{Calendar.DisplayDate.ToString()}   {Calendar.SelectedDate.ToString()}");
        }
    }
}
