﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Ultimate_Schedule_Builder.Views
{
    /// <summary>
    /// Interaction logic for LoadingView.xaml
    /// </summary>
    public partial class LoadingView : UserControl, INotifyPropertyChanged
    {
        private bool isButtonLoading;
        public bool IsButtonLoading
        {
            get { return isButtonLoading; }
            set
            {
                isButtonLoading = value;
                RaisePropertyChanged("IsButtonLoading");
            }
        }
        private int responseStatus;
        public int ResponseStatus
        {
            get { return responseStatus; }
            set
            {
                responseStatus = value;
                RaisePropertyChanged("ResponseStatus");
            }
        }

        public LoadingView()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            LoadingButton.Background = new SolidColorBrush(Color.FromRgb(79, 79, 79));
            ShowButton(0.5);
            IsButtonLoading = true;
        }

        public void OnRequestFailed(Tuple<int,string> res)
        {
            
            ResponseStatus = 2;

            ColorButton(0.5, Color.FromRgb(165,16,0));

            ButtonProgressAssist.SetIndicatorForeground(LoadingButton, new SolidColorBrush(Color.FromRgb(165, 16, 0)));

            var tmr = new DispatcherTimer { Interval = TimeSpan.FromSeconds(1) };
            tmr.Start();
            tmr.Tick += (s, a) =>
            {
                tmr.Stop();
                HideButton(0.5);

                if (this.CloseDialog.Command.CanExecute(null))
                {
                    this.CloseDialog.Command.Execute(null);
                }

            };
        }

        public void OnRequestSuccess()
        {
            
            ResponseStatus = 1;


            ColorButton(0.5, Color.FromRgb(16, 165, 0));

            ButtonProgressAssist.SetIndicatorForeground(LoadingButton, new SolidColorBrush(Color.FromRgb(16, 165, 0)));

            var tmr = new DispatcherTimer { Interval = TimeSpan.FromSeconds(1) };
            tmr.Start();
            tmr.Tick += (s, a) =>
            {
                tmr.Stop();
                HideButton(0.5);

                if (this.CloseDialog.Command.CanExecute(null))
                {
                    this.CloseDialog.Command.Execute(null);
                }

            };

        }

        private void ShowButton(double delay)
        {
            DoubleAnimation da = new DoubleAnimation(0, 1, new Duration(TimeSpan.FromSeconds(delay)));
            LoadingButton.BeginAnimation(OpacityProperty, da);
        }

        private void HideButton(double delay)
        {
            DoubleAnimation da = new DoubleAnimation(1, 0, new Duration(TimeSpan.FromSeconds(delay)));
            LoadingButton.BeginAnimation(OpacityProperty, da);
        }

        private void ColorButton(double d, Color c)
        {
            ColorAnimation ca = new ColorAnimation(c, new Duration(TimeSpan.FromSeconds(d)));
            LoadingButton.Background.BeginAnimation(SolidColorBrush.ColorProperty, ca);
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        private void RaisePropertyChanged(string propertyName)
        {
            var handlers = PropertyChanged;

            handlers(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
