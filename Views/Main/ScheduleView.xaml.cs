﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using MaterialDesignThemes.Wpf;
using Ultimate_Schedule_Builder.Models;

namespace Ultimate_Schedule_Builder.Views
{
    /// <summary>
    /// Interaction logic for ScheduleView.xaml
    /// </summary>
    public partial class ScheduleView : UserControl, INotifyPropertyChanged
    {

        //? MENU 
        private List<MenuItemView> menuItemList;
        public List<MenuItemView> MenuItemList
        {
            get { return menuItemList ?? (menuItemList = new List<MenuItemView>()); }
            set
            {
                menuItemList = value;
                RaisePropertyChanged("MenuItemList");
            }
        }

        private List<MenuItemView> menuItemListBottom;
        public List<MenuItemView> MenuItemListBottom
        {
            get { return menuItemListBottom ?? (menuItemListBottom = new List<MenuItemView>()); }
            set
            {
                menuItemListBottom = value;
                RaisePropertyChanged("MenuItemListBottom");
            }
        }

        //? MENU SUB

        private bool subMenuOpenned = false;
        public bool SubMenuOpenned
        {
            get { return subMenuOpenned; }
            set
            {
                subMenuOpenned = value;
                RaisePropertyChanged("SubMenuOpenned");
            }
        }

        private string subMenuType;
        public string SubMenuType
        {
            get { return subMenuType; }
            set
            {
                subMenuType = value;
                RaisePropertyChanged("SubMenuType");
            }
        }

        private string subMenuHeaderName;
        public string SubMenuHeaderName
        {
            get { return subMenuHeaderName; }
            set
            {
                subMenuHeaderName = value;
                RaisePropertyChanged("SubMenuHeaderName");
            }
        }

        private bool subMenuHeaderAddable;
        public bool SubMenuHeaderAddable
        {
            get { return subMenuHeaderAddable; }
            set
            {
                subMenuHeaderAddable = value;
                RaisePropertyChanged("SubMenuHeaderAddable");
            }
        }

        //? MENU SUB -> SCHEDULES

        private int maxScheduleUsers = 10;

        private List<MenuSubItemView> menuSubScheduleUsersList;
        public List<MenuSubItemView> MenuSubScheduleUsersList
        {
            get { return menuSubScheduleUsersList ?? (menuSubScheduleUsersList = new List<MenuSubItemView>()); }
            set
            {
                menuSubScheduleUsersList = value;
                RaisePropertyChanged("MenuSubScheduleUsersList");
            }
        }

        //? MENU SUB -> TOOLS

        private List<ToolObject> CurrentTools = new List<ToolObject>()
        {
            new ToolObject() {ToolId="ETH",ToolText="Export to HTML",ToolIcon=PackIconKind.LanguageHtml5},
            new ToolObject() {ToolId="ETJ",ToolText="Export to JSON",ToolIcon=PackIconKind.Json}
        };

        private List<MenuSubItemView> menuSubScheduleToolsList;
        public List<MenuSubItemView> MenuSubScheduleToolsList
        {
            get { return menuSubScheduleToolsList ?? (menuSubScheduleToolsList = new List<MenuSubItemView>()); }
            set
            {
                menuSubScheduleToolsList = value;
                RaisePropertyChanged("MenuSubScheduleToolsList");
            }
        }

        //? MENU SUB -> SETTINGS

        private List<SettingObject> CurrentSettings = new List<SettingObject>() {  
            new SettingObject() {SettingId="EDWD",SettingText= "Empty days are work days",SettingDefaultActive=false },
            new SettingObject() {SettingId="ASD",SettingText= "Auto save data",SettingDefaultActive=false },
            new SettingObject() {SettingId="SWE",SettingText= "Show weekend",SettingDefaultActive=false },
            new SettingObject() {SettingId="LSH",SettingText= "Last schedule hour is final",SettingDefaultActive=false }
        };

        private List<MenuSubItemView> menuSubScheduleSettingsList;
        public List<MenuSubItemView> MenuSubScheduleSettingsList
        {
            get { return menuSubScheduleSettingsList ?? (menuSubScheduleSettingsList = new List<MenuSubItemView>()); }
            set
            {
                menuSubScheduleSettingsList = value;
                RaisePropertyChanged("MenuSubScheduleSettingsList");
            }
        }

        //? CONTENT

        private string contentCurrentWeekText;
        public string ContentCurrentWeekText
        {
            get => this.contentCurrentWeekText;
            set
            {
                this.contentCurrentWeekText = value;
                RaisePropertyChanged("ContentCurrentWeekText");
            }
        }

        private int contentCurrentScheduleQuery = 0;
        public int ContentCurrentScheduleQuery
        {
            get => contentCurrentScheduleQuery;
            set
            {
                if(value == -1)
                {
                    contentCurrentScheduleQuery = 0;
                }
                else
                {
                    contentCurrentScheduleQuery = value;
                }

                
                ContentCanChangeDate = CanChangeDate();
                OnDateQueryChange();
                contentWeekOffset = 0;
                RaisePropertyChanged("ContentCurrentScheduleQuery");
            }
        }

        private bool contentCanChangeDate = false;
        public bool ContentCanChangeDate
        {
            get => contentCanChangeDate;
            set
            {
                contentCanChangeDate = value;
                RaisePropertyChanged("ContentCanChangeDate");
            }
        }

        private int contentWeekOffset = 0;

        private DateTime contentSelectedDateTime;
        public DateTime ContentSelectedDateTime
        {
            get => this.contentSelectedDateTime;
            set
            {
                this.contentSelectedDateTime = value;
                RefreshDate(true);
            }
        }


        public ScheduleView()
        {
            InitializeComponent();
            DataContext = this;
            
        }

        public void SetUp(StagStudent st, ApplicationColorSchema acs)
        {


            SetUp();
        }

        private void SetUp()
        {
            SetUpMenuButtons();
            SetUpDate();
            GetLastSession();
            //!CreateScheduleGrid();
        }

        private void SetUpMenuButtons()
        {
            Dictionary<string, PackIconKind> mi = new Dictionary<string, PackIconKind>() { { "Schedules", PackIconKind.AccountMultiple }, { "Tools", PackIconKind.Apps } , { "Settings", PackIconKind.Settings } };

            foreach(KeyValuePair<string, PackIconKind> i in mi)
            {
                MenuItemView s = new MenuItemView();
                s.SetUp(i.Key, i.Value);
                s.CurrentButton.Click += OnMenuButtonClick;
                MenuItemList.Add(s);
            }

            MenuItemView l = new MenuItemView();
            l.SetUp("Logout", PackIconKind.LogoutVariant);
            l.CurrentButton.Click += OnMenuButtonClick;
            MenuItemListBottom.Add(l);


            foreach(SettingObject sob in CurrentSettings)
            {
                sob.SettingActive = sob.SettingDefaultActive;

                MenuSubItemView msi = new MenuSubItemView();
                msi.SetUp(
                    SubItemType.Setting,
                    new Tuple<string, object>("SettingId", sob.SettingId),
                    new Tuple<string, object>("SettingText", sob.SettingText), 
                    new Tuple<string, object>("SettingDefaultActive", sob.SettingDefaultActive)
                );
                msi.SettingToggle.Click += OnSubMenuSettingChanged;
                MenuSubScheduleSettingsList.Add(msi);
            }

            foreach(ToolObject to in CurrentTools)
            {
                MenuSubItemView msi = new MenuSubItemView();
                msi.SetUp(
                    SubItemType.Tool,
                    new Tuple<string, object>("ToolId", to.ToolId),
                    new Tuple<string, object>("ToolText", to.ToolText),
                    new Tuple<string, object>("ToolIcon", to.ToolIcon)
                );
                msi.ToolActivate.Click += OnSubMenuToolClick;
                MenuSubScheduleToolsList.Add(msi);
            }
        }

        private void SetUpDate()
        {
            ContentSelectedDateTime = DateTime.Today;
            RefreshDate(true);
        }

        private void RefreshDate(bool picked = false)
        {
            DateTime d = ContentSelectedDateTime;

            if (picked)
            {
                if(ContentCurrentScheduleQuery == 0) // Day mode
                {
                    ContentCurrentWeekText = $"{d.ToString("dddd")}, {d.ToString("MMM")} {d.ToString("M")}, {d.ToString("yyyy")}"; 
                }
                else if(ContentCurrentScheduleQuery == 1) // Week mode
                {
                    DateTime ws = Utility.FirstDayOfWeek(d);
                    DateTime we = Utility.LastDayOfWeek(d);

                    ContentCurrentWeekText = $"{ws.ToString("dddd")}, {ws.ToString("MMM")} {ws.ToString("M")}, {ws.ToString("yyyy")} - {we.ToString("dddd")}, {we.ToString("MMM")} {we.ToString("M")}, {we.ToString("yyyy")}";
                }
                else
                {
                    ContentCurrentWeekText = $"{d.ToString("dddd")}, {d.ToString("MMM")} {d.ToString("M")}, {d.ToString("yyyy")}";
                }

                contentWeekOffset = 0;
            }
            else
            {
                if(contentWeekOffset != 0)
                {
                    if (ContentCurrentScheduleQuery == 0) // Day mode
                    {
                        DateTime dm = d.AddDays(contentWeekOffset);
                        ContentCurrentWeekText = $"{dm.ToString("dddd")}, {dm.ToString("MMM")} {dm.ToString("M")}, {dm.ToString("yyyy")}";
                    }
                    else if (ContentCurrentScheduleQuery == 1) // Week mode
                    {
                        DateTime dm = d.AddDays(contentWeekOffset * 7);

                        DateTime ws = Utility.FirstDayOfWeek(dm);
                        DateTime we = Utility.LastDayOfWeek(dm);

                        ContentCurrentWeekText = $"{ws.ToString("dddd")}, {ws.ToString("MMM")} {ws.ToString("M")}, {ws.ToString("yyyy")} - {we.ToString("dddd")}, {we.ToString("MMM")} {we.ToString("M")}, {we.ToString("yyyy")}";
                    }
                    else
                    {
                        ContentCurrentWeekText = $"{d.ToString("dddd")}, {d.ToString("MMM")} {d.ToString("M")}, {d.ToString("yyyy")}";
                    }
                }
                else
                {
                    RefreshDate(true);
                }
            }

        }

        private void AddNewUsers(List<dynamic> utd)
        {
            if(MenuSubScheduleUsersList.Count + utd.Count <= maxScheduleUsers)
            {
                bool added = false;

                foreach (dynamic u in utd)
                {
                    bool duplicate = false;
                    if(u.StagUserRoleString == "Student")
                    {
                        StagStudent st = (StagStudent)u;
                        
                        foreach(MenuSubItemView dm in MenuSubScheduleUsersList)
                        {
                            if(dm.DataStorage.StagUserRoleString == "Student")
                            {
                                if(dm.UserId == st.stagUserName)
                                {
                                    duplicate = true;
                                }
                            }
                        }

                        if (!duplicate)
                        {
                            MenuSubItemView ms = new MenuSubItemView();
                            ms.SetUp(
                                SubItemType.ScheduleUser,
                                new Tuple<string, object>("UserId", st.stagUserName),
                                new Tuple<string, object>("UserText", $"{st.stagUserFirstName} {st.stagUserLastName}"),
                                new Tuple<string, object>("UserRole", st.stagUserRole.ToString())
                            );
                            ms.UserScheduleActivate.Click += OnSubMenuScheduleActiveChanged;
                            ms.UserScheduleRemove.Click += OnSubMenuScheduleRemoved;
                            ms.DataStorage = st;

                            MenuSubScheduleUsersList.Add(ms);

                            added = true;
                        }
                    }
                    else
                    {
                        StagTeacher st = (StagTeacher)u;

                        foreach (MenuSubItemView dm in MenuSubScheduleUsersList)
                        {
                            if (dm.DataStorage.StagUserRoleString == "Teacher")
                            {
                                if (dm.UserId == st.stagId.ToString())
                                {
                                    duplicate = true;
                                }
                            }
                        }

                        if (!duplicate)
                        {
                            MenuSubItemView ms = new MenuSubItemView();
                            ms.SetUp(
                                SubItemType.ScheduleUser,
                                new Tuple<string, object>("UserId", st.stagId.ToString()),
                                new Tuple<string, object>("UserText", $"{st.stagUserFirstName} {st.stagUserLastName}"),
                                new Tuple<string, object>("UserRole", st.stagUserRole.ToString())
                            );
                            ms.UserScheduleActivate.Click += OnSubMenuScheduleActiveChanged;
                            ms.UserScheduleRemove.Click += OnSubMenuScheduleRemoved;
                            ms.DataStorage = st;

                            MenuSubScheduleUsersList.Add(ms);

                            added = true;
                        }
                    }
                }

                if (added)
                {
                    HeaderContentData.ItemsSource = null;
                    HeaderContentData.ItemsSource = MenuSubScheduleUsersList;

                    //!RefreshSchedules();
                }
            }
        }
        /*! //
        private async void RefreshSchedules()
        {
            ScheduleScrollViewer.Content = null;

            LoadingView lv = new LoadingView();
            var result = await LoadingDialog.ShowDialog(lv, delegate (object s, DialogOpenedEventArgs a)
            {
                foreach (MenuSubItemView dm in MenuSubScheduleUsersList)
                {
                    if (dm.DataStorage.StagUserRoleString == "Student")
                    {
                        StagStudent st = (StagStudent)dm.DataStorage;

                        try
                        {
                            GetSchedule(StagUserRole.Student, st.stagUserOsCislo, (StagScheduleCourse[] lo) =>
                            {
                                if(lo == null)
                                {
                                    lo = new StagScheduleCourse[] { };
                                }
                                Schedule sch = new Schedule(lo, ContentSelectedDateTime);
                                st.StagUserSchedule = sch;
                            });
                        }
                        catch (RequestFailedException e)
                        {
                            //lv.OnRequestFailed(new Tuple<int, string>(e.Code, e.Message));
                        }

                    }
                    else
                    {
                        StagTeacher st = (StagTeacher)dm.DataStorage;

                        try
                        {
                            GetSchedule(StagUserRole.Teacher, st.stagUserTeacherId, (StagScheduleCourse[] lo) =>
                            {
                                if (lo == null)
                                {
                                    lo = new StagScheduleCourse[] { };
                                }
                                Schedule sch = new Schedule(lo, ContentSelectedDateTime);
                                st.StagUserSchedule = sch;
                            });
                        }
                        catch (RequestFailedException e)
                        {
                            //lv.OnRequestFailed(new Tuple<int, string>(e.Code, e.Message));
                        }
                    }
                }

                lv.OnRequestSuccess();
            }, 
            (s,a) => 
            {
                CreateScheduleGrid();
            });
        }

        
        private async void GetSchedule(StagUserRole r, dynamic id, Action<StagScheduleCourse[]> ret)
        {

            if (contentWeekOffset != 0)
            {
                if (ContentCurrentScheduleQuery == 0) // Day
                {
                    DateTime dm = ContentSelectedDateTime.AddDays(contentWeekOffset);

                    if (r == StagUserRole.Student)
                    {
                        StagScheduleCourse[] lo = await Rest.CreateStudentGetScheduleRequest((string)id, dm, dm);
                        ret(lo);
                    }
                    else
                    {
                        StagScheduleCourse[] lo = await Rest.CreateTeacherGetScheduleRequest((string)id, dm, dm);
                        ret(lo);
                    }
                }
                else if (ContentCurrentScheduleQuery == 1) // Week
                {
                    DateTime dm = ContentSelectedDateTime.AddDays(contentWeekOffset * 7);

                    DateTime ws = Utility.FirstDayOfWeek(dm);
                    DateTime we = Utility.LastDayOfWeek(dm);

                    if (r == StagUserRole.Student)
                    {
                        StagScheduleCourse[] lo = await Rest.CreateStudentGetScheduleRequest((string)id, ws, we);
                        ret(lo);
                    }
                    else
                    {
                        StagScheduleCourse[] lo = await Rest.CreateTeacherGetScheduleRequest((string)id, ws, we);
                        ret(lo);
                    }
                }
            }
            else // Is zero
            {
                if (ContentCurrentScheduleQuery == 0) // Day
                {
                    if (r == StagUserRole.Student)
                    {
                        StagScheduleCourse[] lo = await Rest.CreateStudentGetScheduleRequest((string)id, ContentSelectedDateTime, ContentSelectedDateTime);
                        ret(lo);
                    }
                    else
                    {
                        StagScheduleCourse[] lo = await Rest.CreateTeacherGetScheduleRequest((string)id, ContentSelectedDateTime, ContentSelectedDateTime);
                        ret(lo);
                    }
                }
                else if (ContentCurrentScheduleQuery == 1) // Week
                {
                    DateTime ws = Utility.FirstDayOfWeek(ContentSelectedDateTime);
                    DateTime we = Utility.LastDayOfWeek(ContentSelectedDateTime);

                    if (r == StagUserRole.Student)
                    {
                        StagScheduleCourse[] lo = await Rest.CreateStudentGetScheduleRequest((string)id, ws, we);
                        ret(lo);
                    }
                    else
                    {
                        StagScheduleCourse[] lo = await Rest.CreateTeacherGetScheduleRequest((string)id, ws, we);
                        ret(lo);
                    }

                }
            }

        }
        
        private void CreateScheduleGrid()
        {
            //? Create setting list
            Dictionary<string, bool> cs = new Dictionary<string, bool>();
            foreach(SettingObject so in CurrentSettings)
            {
                cs.Add(so.SettingId, so.SettingActive);
            }

            //? Set settings
            bool EmptyDaysAreWorkDays   = cs["EDWD"],
                 ShowWeekend            = cs["SWE"],
                 IsLastScheduleHourLast = cs["LSH"];

            int LastScheduleHour = 0;

            int MaxCollums = 15; //? Create number of collums (+1 for day name with date)

            int MaxRows = 5; //? Create number of rows (+1 for hour name)

            if (ShowWeekend)
            {
                MaxRows += 2;
            }

            if (IsLastScheduleHourLast)
            {
                foreach (MenuSubItemView dm in MenuSubScheduleUsersList)
                {
                    StagUser st = (StagUser)dm.DataStorage;
                    foreach(List<StagScheduleSubject> sch in st.StagUserSchedule.ScheduleWeek)
                    {
                        foreach(StagScheduleSubject schs in sch)
                        {
                            int endHour = schs.SubjectDate.Hour + schs.SubjectLenght;
                            if(endHour > LastScheduleHour)
                            {
                                LastScheduleHour = endHour;
                            }
                        }
                    }
                }
            }

            //? Create grid
            Grid DynamicGrid = new Grid();
            DynamicGrid.ShowGridLines = true;
            DynamicGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
            DynamicGrid.VerticalAlignment = VerticalAlignment.Stretch;

            //Create Definitions

            RowDefinition BaseRow = new RowDefinition();
            BaseRow.Height = new GridLength(45);
            DynamicGrid.RowDefinitions.Add(BaseRow);

            ColumnDefinition BaseColumn = new ColumnDefinition();
            BaseColumn.Width = new GridLength(160);
            DynamicGrid.ColumnDefinitions.Add(BaseColumn);

            for(var i = 0; i < MaxCollums; i++)
            {
                ColumnDefinition DynamicColumn = new ColumnDefinition();
                DynamicColumn.Width = new GridLength(80);
                DynamicGrid.ColumnDefinitions.Add(DynamicColumn);
            }
            for(var i = 0; i < MaxRows; i++)
            {

                RowDefinition DynamicRow = new RowDefinition();
                DynamicRow.Height = new GridLength(45);
                DynamicGrid.RowDefinitions.Add(DynamicRow);
            }

            //? Populate placeholders



            for(var i = 0; i < MaxRows; i++)
            {
                TextBlock DateDayText = new TextBlock();
                DateDayText.Text = CultureInfo.CurrentCulture.DateTimeFormat.GetDayName((DayOfWeek)i);
                DateDayText.VerticalAlignment = VerticalAlignment.Stretch;
                DateDayText.HorizontalAlignment = HorizontalAlignment.Stretch;
                DateDayText.FontSize = 14;
                DateDayText.FontWeight = FontWeights.Bold;
                DateDayText.Foreground = Brushes.Black;
                Grid.SetColumn(DateDayText, 0);
                Grid.SetRow(DateDayText, i + 1);

                DynamicGrid.Children.Add(DateDayText);
            }


            //? Populate schedule with users time table

            ScheduleScrollViewer.Content = DynamicGrid;

        }
        */
        private void GetLastSession()
        {

        }

        private bool CanChangeDate()
        {
            bool ret = false;
            if(ContentCurrentScheduleQuery == 0 || ContentCurrentScheduleQuery == 1)
            {
                ret = true;
            }
            return ret; 
        }

        private void OnDateQueryChange()
        {
            RefreshDate();
            //!RefreshSchedules();

        }

        private void MoveDateNext()
        {
            contentWeekOffset++;
            OnDateQueryChange();
        }

        private void MoveDatePrevious()
        {
            contentWeekOffset--;
            OnDateQueryChange();
        }

        

        private void ActivateTool(string tid)
        {
            Console.WriteLine($"Tool {tid} clicked");
        }

        private async void LogoutRequest(LoadingView lv)
        {
            try
            {

                bool lo = await Rest.CreateAuthLogoutRequest();

                lv.OnRequestSuccess();
            }
            catch (RequestFailedException e)
            {
                lv.OnRequestFailed(new Tuple<int, string>(e.Code, e.Message));
            }
        }

        private async void SetDateButton_Click(object sender, RoutedEventArgs e)
        {
            SelectCalendarDate su = new SelectCalendarDate();

            //su.SetUp(maxScheduleUsers - MenuSubScheduleUsersList.Count);

            var result = await LoadingDialog.ShowDialog(su);

            if(result != null)
            {
                ContentSelectedDateTime = (DateTime)result;
                contentWeekOffset = 0;
            }
        }

        private async void OnMenuButtonClick(object sender, RoutedEventArgs e)
        {

            MenuItemView miv = (MenuItemView)((Button)sender).DataContext;
            if (miv.ButtonName == "Logout")
            {
                LoadingView lv = new LoadingView();
                var result = await LoadingDialog.ShowDialog(lv, delegate (object s, DialogOpenedEventArgs a)
                {
                    LogoutRequest(lv);

                }, delegate (object s, DialogClosingEventArgs a)
                {
                    Console.WriteLine($" Dialog closed with code {lv.ResponseStatus}");

                    if (lv.ResponseStatus == 1)
                    {
                        var par = App.Current.MainWindow as MainWindow;
                        par.OnLogoutSuccess();
                    }

                });
                return;
            }


            if (miv.IsActive)
            {
                SubMenuOpenned = true;
                SubMenuType = miv.ButtonName;
                switch (miv.ButtonName) //Schedules Tools Settings
                {
                    case "Schedules":
                        SubMenuHeaderName = "Active schedules";
                        SubMenuHeaderAddable = true;

                        HeaderContentData.ItemsSource = MenuSubScheduleUsersList;

                        break;
                    case "Tools":
                        SubMenuHeaderName = "Tools";
                        SubMenuHeaderAddable = false;

                        HeaderContentData.ItemsSource = MenuSubScheduleToolsList;
                        break;
                    case "Settings":
                        SubMenuHeaderName = "Settings";
                        SubMenuHeaderAddable = false;

                        HeaderContentData.ItemsSource = MenuSubScheduleSettingsList;
                        break;
                }

                foreach (MenuItemView m in MenuItemList)
                {
                    if (m.ButtonName != miv.ButtonName)
                    {
                        m.Deactivate();
                    }
                }


            }
            else
            {
                SubMenuOpenned = false;
                SubMenuType = "";
            }
        }

        private async void AddButton_Click(object sender, RoutedEventArgs e)
        {
            if (SubMenuOpenned && SubMenuHeaderAddable)
            {
                if (SubMenuType == "Schedules")
                {
                    if (MenuSubScheduleUsersList.Count <= maxScheduleUsers)
                    {
                        SearchUserView su = new SearchUserView();
                        su.SetUp(maxScheduleUsers - MenuSubScheduleUsersList.Count);
                        var result = await LoadingDialog.ShowDialog(su);
                        List<dynamic> r = (List<dynamic>)result;
                        if (r.Count > 0)
                        {
                            AddNewUsers(r);
                        }
                    }
                }
            }
        }

        private void DateMoveNext_Click(object sender, RoutedEventArgs e)
        {
            MoveDateNext();
        }

        private void DataMovePrevious_Click(object sender, RoutedEventArgs e)
        {
            MoveDatePrevious();
        }

        private void OnSubMenuToolClick(object sender, RoutedEventArgs e)
        {
            MenuSubItemView miv = (MenuSubItemView)((Button)sender).DataContext;
            ActivateTool(miv.ToolId);
        }

        private void OnSubMenuSettingChanged(object sender, RoutedEventArgs e)
        {
            MenuSubItemView miv = (MenuSubItemView)((ToggleButton)sender).DataContext;
            foreach(SettingObject so in CurrentSettings)
            {
                if(so.SettingId == miv.SettingId)
                {
                    Console.WriteLine($"Setting {miv.SettingId} turned {(miv.IsSettingActive == true ? "On" : "Off")}");
                    so.SettingActive = miv.IsSettingActive;

                    ScheduleScrollViewer.Content = null;
                    //!CreateScheduleGrid();

                    break;
                }
            }
            
        }

        private void OnSubMenuScheduleActiveChanged(object sender, RoutedEventArgs e)
        {
            MenuSubItemView miv = (MenuSubItemView)((Button)sender).DataContext;
            Console.WriteLine($"User {miv.UserId} turned {(miv.IsScheduleActive == true ? "On" : "Off")}");
        }

        private void OnSubMenuScheduleRemoved(object sender, RoutedEventArgs e)
        {
            MenuSubItemView miv = (MenuSubItemView)((Button)sender).DataContext;
            foreach(MenuSubItemView v in MenuSubScheduleUsersList.ToList())
            {
                StagUser s = (StagUser)v.DataStorage;
                if(s.stagId == miv.UserId)
                {
                    Console.WriteLine($"User to remove {miv.UserId}");
                    MenuSubScheduleUsersList.Remove(v);
                    RaisePropertyChanged("MenuSubScheduleUsersList");
                    break;
                }
            }
            
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        private void RaisePropertyChanged(string propertyName)
        {
            var handlers = PropertyChanged;

            handlers(this, new PropertyChangedEventArgs(propertyName));
        }

    }


    public class SettingObject
    {
        public string SettingId { get; set; }
        public string SettingText { get; set; }
        public bool   SettingDefaultActive { get; set; }

        public bool SettingActive { get; set; } = false;
    }

    public class ToolObject
    {
        public string ToolId { get; set; }
        public string ToolText { get; set; }
        public PackIconKind ToolIcon { get; set; }
    }


}
