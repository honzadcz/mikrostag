﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Ultimate_Schedule_Builder.Models;
using Ultimate_Schedule_Builder.Views.Components;

namespace Ultimate_Schedule_Builder.Views.Main.Tabs
{
    public partial class MySemesterView : UserControl
    {
        private ContentControl calendarMoverControl = null;
        public MySemesterView()
        {
            InitializeComponent();
            DataContext = this;

            var tmr = new DispatcherTimer { Interval = TimeSpan.FromSeconds(1) };
            tmr.Start();
            tmr.Tick += (s, a) =>
            {
                tmr.Stop();
                this.createSchedule();

            };
        }

        private void createSchedule()
        {
            CalendarComponent cc = new CalendarComponent();
            CalendarMoverComponent cmc = new CalendarMoverComponent(cc);
            ScheduleUsersExpanderComponent suec = new ScheduleUsersExpanderComponent();
            ScheduleSettingsComponent ssc = new ScheduleSettingsComponent();


            ScheduleComponent sc = new ScheduleComponent(
                new KeyValuePair<string, dynamic>("UserExpander", suec),
                new KeyValuePair<string, dynamic>("ScheduleCalendar", cc),
                new KeyValuePair<string, dynamic>("ScheduleSettings", ssc),

                new KeyValuePair<string, dynamic>("NoCreditTerms", Settings.ScheduleStartWithStaticWeek)
            );

            this.MainSchedule.Content = sc;
            this.CalendarHolder.Content = cc;
            this.UsersExpander.Content = suec;
            this.ScheduleSettingsHolder.Content = ssc;
            this.calendarMoverControl.Content = cmc;
            //this.ScheduleExpanderDrop.HeaderTemplate;

            this.GradesContentControll.Content = new GradesComponent();

        }

        private void MainCalendarControlComponent_Initialized(object sender, EventArgs e)
        {
            calendarMoverControl = (ContentControl)sender;
        }
    }
}
