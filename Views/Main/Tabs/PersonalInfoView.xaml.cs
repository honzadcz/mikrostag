﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Ultimate_Schedule_Builder.Models;

namespace Ultimate_Schedule_Builder.Views.Main.Tabs
{
    /// <summary>
    /// Interaction logic for PersonalInfoView.xaml
    /// </summary>
    public partial class PersonalInfoView : UserControl
    {
        public PersonalInfoView()
        {
            InitializeComponent();
            DataContext = this;

            this.getLocalStudentData();
            this.getAbsolvedSubjects();
        }

        private async void getLocalStudentData()
        {
            this.PinfoHolder.IsEnabled = false;
            StagStudent st = await Rest.CreateStudentGetInfoRequest();

            this.CurrentUserCredits.Text = st.StudentCredits.ToString();
            this.CurrentUserFaculty.Text = st.stagUserFaculty.ToString();
            this.CurrentUserField.Text = st.stagUserField;
            this.CurrentUserFirstName.Text = $"{(st.StudentTitleBefore != null ? st.StudentTitleBefore : "")} {st.stagUserFirstName} {st.stagUserLastName} {(st.StudentTitleAfter != null ? st.StudentTitleAfter : "")}";
            this.CurrentUserName.Text = st.stagUserName;
            this.CurrentUserOsCislo.Text = st.stagId.ToString();
            this.CurrentUserYear.Text = st.stagUserCurrentGrade.ToString();

            this.PinfoHolder.IsEnabled = true;
        }

        private async void getAbsolvedSubjects()
        {
            this.TakenCoursesHolder.IsEnabled = false;
            this.TakenSubjectsList.Children.Clear();
            StagAbsolvedCourse[] sac = await Rest.CreateStudentGetAbsolvedSubjectsRequest();

            List<StagAbsolvedCourse> sacList = new List<StagAbsolvedCourse>(sac);
            sacList.Reverse();


            foreach(StagAbsolvedCourse s in sacList.ToList())
            {
                Grid sGrid = new Grid();
                sGrid.Height = 50;
                sGrid.VerticalAlignment = VerticalAlignment.Top;
                sGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                sGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(2, GridUnitType.Star) });
                sGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                sGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(3, GridUnitType.Star) });
                sGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });

                Rectangle divider = new Rectangle();
                sGrid.Children.Add(divider);
                divider.Height = 1;
                divider.HorizontalAlignment = HorizontalAlignment.Stretch;
                divider.VerticalAlignment = VerticalAlignment.Bottom;
                divider.Fill = Brushes.Gray;
                Grid.SetColumn(divider, 0);
                Grid.SetColumnSpan(divider, 5);

                TextBlock courseId = new TextBlock();
                sGrid.Children.Add(courseId);
                courseId.HorizontalAlignment = HorizontalAlignment.Center;
                courseId.VerticalAlignment = VerticalAlignment.Center;
                courseId.Text = s.CourseFaculty + " / " + s.CourseId;
                Grid.SetColumn(courseId, 0);

                TextBlock courseName = new TextBlock();
                sGrid.Children.Add(courseName);
                courseName.HorizontalAlignment = HorizontalAlignment.Center;
                courseName.VerticalAlignment = VerticalAlignment.Center;
                courseName.Text = s.CourseName;
                Grid.SetColumn(courseName, 1);

                TextBlock courseYear = new TextBlock();
                sGrid.Children.Add(courseYear);
                courseYear.HorizontalAlignment = HorizontalAlignment.Center;
                courseYear.VerticalAlignment = VerticalAlignment.Center;
                courseYear.Text = s.CourseYear + " " + s.CourseSemester;
                Grid.SetColumn(courseYear, 2);

                Ellipse bgResult = new Ellipse();
                sGrid.Children.Add(bgResult);
                bgResult.Width = 24;
                bgResult.Height = 24;
                bgResult.HorizontalAlignment = HorizontalAlignment.Center;
                bgResult.VerticalAlignment = VerticalAlignment.Center;
                bgResult.Fill = Brushes.Gray;
                Grid.SetColumn(bgResult, 4);

                Ellipse fiResult = new Ellipse();
                sGrid.Children.Add(fiResult);
                fiResult.Width = 20;
                fiResult.Height = 20;
                fiResult.HorizontalAlignment = HorizontalAlignment.Center;
                fiResult.VerticalAlignment = VerticalAlignment.Center;
                Grid.SetColumn(fiResult, 4);

                fiResult.ToolTip = $" Date absolved: {s.CourseDateAbsolved}. \n Grade: {s.CourseGrade} \n  Credits: {s.CourseCreditCount}";

                if (s.CourseAbsolved)
                {
                    fiResult.Fill = Brushes.LightGreen;
                }
                else
                {
                    fiResult.Fill = Brushes.Red;
                }

                this.TakenSubjectsList.Children.Add(sGrid);
            }

            this.TakenCoursesHolder.IsEnabled = true;
        }
    }
}
