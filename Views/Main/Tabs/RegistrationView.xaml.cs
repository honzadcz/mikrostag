﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Ultimate_Schedule_Builder.Views.Components;

namespace Ultimate_Schedule_Builder.Views.Main.Tabs
{
    /// <summary>
    /// Interaction logic for RegistrationView.xaml
    /// </summary>
    public partial class RegistrationView : UserControl
    {
        public RegistrationView()
        {
            InitializeComponent();
            DataContext = this;

            var tmr = new DispatcherTimer { Interval = TimeSpan.FromSeconds(1) };
            tmr.Start();
            tmr.Tick += (s, a) =>
            {
                tmr.Stop();
                this.createSchedule();

            };
        }

        private void createSchedule()
        {
            ScheduleComponent sc = new ScheduleComponent(
               new KeyValuePair<string, dynamic>("TermListSchedule", true)
           );

            this.MainTermSchedule.Content = sc;
        }
    }
}
