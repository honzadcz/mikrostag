﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Ultimate_Schedule_Builder.Models;
using Ultimate_Schedule_Builder.Views;



namespace Ultimate_Schedule_Builder.Views
{
    public partial class LoginView : UserControl, INotifyPropertyChanged
    {

        public LoginView()
        {
            InitializeComponent();
            DataContext = this;
            LoginButton.IsEnabled = false;

            UsernameInput.TextChanged += (e, s) =>
            {
                LoginButton.IsEnabled = canLogin();
            };

            PasswordINput.PasswordChanged += (e, s) =>
            {
                LoginButton.IsEnabled = canLogin();
            };
            
        }

        private bool canLogin()
        {
            if(!String.IsNullOrWhiteSpace(UsernameInput.Text) && !String.IsNullOrWhiteSpace(PasswordINput.Password))
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }

        public void ChangeColorSchema()
        {
            ApplicationColorSchema df = new ApplicationColorSchema();

            NameHeader.Background = df.MainColor;
            LoginButton.Background = df.MainColor;

            UsernameInput.CaretBrush = df.MainColor;
            PasswordINput.CaretBrush = df.MainColor;

            ColorAnimation ca = new ColorAnimation(Settings.ApplicationColorSchema.MainColor.Color, new Duration(TimeSpan.FromSeconds(0.5)));
            NameHeader.Background.BeginAnimation(SolidColorBrush.ColorProperty, ca);

            LoginButton.Background.BeginAnimation(SolidColorBrush.ColorProperty, ca);

            UsernameInput.CaretBrush.BeginAnimation(SolidColorBrush.ColorProperty, ca);

            PasswordINput.CaretBrush.BeginAnimation(SolidColorBrush.ColorProperty, ca);
        }

        private async void LoginButton_Click(object sender, RoutedEventArgs arg)
        {
            LoadingView lv = new LoadingView();
            var result = await LoadingDialog.ShowDialog(lv, delegate (object s, DialogOpenedEventArgs a)
            {
                LoginRequest(lv);

            },delegate (object s, DialogClosingEventArgs a)
            {
                Console.WriteLine($" Dialog closed with code {lv.ResponseStatus}");

                if (lv.ResponseStatus == 1)
                {
                    var par = App.Current.MainWindow as MainWindow;
                    par.OnLoginSuccess();
                }

                
            });
        }

        private async void LoginRequest(LoadingView lv)
        {
            try
            {
                Regex reg = new Regex(@"^st?");
                if (reg.Match(UsernameInput.Text).Success)
                {
                    //! Student login
                }
                else
                {
                    //! Teacher login
                }

                var par = App.Current.MainWindow as MainWindow;

                Tuple<StagUserRole, dynamic> rll = await Rest.CreateAuthLoginRequest(UsernameInput.Text, PasswordINput.Password);

                par.OnLoginSuccess(rll.Item1, rll.Item2);
                PasswordINput.Password = "";

                lv.OnRequestSuccess();
            }
            catch (RequestFailedException e)
            {
                lv.OnRequestFailed(new Tuple<int, string>(e.Code, e.Message));
            }
        }

        private async void LogoutRequest(LoadingView lv)
        {
            try
            {

                bool lo = await Rest.CreateAuthLogoutRequest();

                lv.OnRequestSuccess();
            }
            catch (RequestFailedException e)
            {
                lv.OnRequestFailed(new Tuple<int, string>(e.Code, e.Message));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        private void RaisePropertyChanged(string propertyName)
        {
            var handlers = PropertyChanged;

            handlers(this, new PropertyChangedEventArgs(propertyName));
        }

        private async void EmergencyLogout_Click(object sender, RoutedEventArgs e)
        {
            LoadingView lv = new LoadingView();
            var result = await LoadingDialog.ShowDialog(lv, delegate (object s, DialogOpenedEventArgs a)
            {
                LogoutRequest(lv);

            }, delegate (object s, DialogClosingEventArgs a)
            {
                Console.WriteLine($" Dialog closed with code {lv.ResponseStatus}");

                if (lv.ResponseStatus == 1)
                {
                    var par = this.Parent as MainWindow;
                    par.OnLogoutSuccess();
                }
            });
            return;
        }
    }
}
