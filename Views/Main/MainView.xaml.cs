﻿using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Controls;

using Ultimate_Schedule_Builder.Models;
using Ultimate_Schedule_Builder.Views.Main.Tabs;
using Ultimate_Schedule_Builder.Views.Menu;

namespace Ultimate_Schedule_Builder.Views.Main
{
    public partial class MainView : UserControl, INotifyPropertyChanged
    {
        //? Navigation
        private MainMenuComponent mainMenuComponent;
        private Transitioner mainViewTransitioner;
        private MenuCategories selectedTab;

        //? Tab storage
        private MySemesterView mySemesterView;

        public MainView()
        {
            InitializeComponent();
            DataContext = this;

            this.createMainMenu();
            this.createMainMenuTabs();
        }

        private void createMainMenu()
        {
            MainMenuComponent mm = new MainMenuComponent();
            mm.OnButtonPressed += (b) =>
            {
                onMenuButtonPressed(b);
            };
            mainMenuComponent = mm;
            MainMenuContentControl.Content = mainMenuComponent;

            selectedTab = MenuCategories.MySemester;
        }

        private void createMainMenuTabs()
        {
            MainViewTransitionerControl.Content = null;

            Transitioner t = new Transitioner();
            t.AutoApplyTransitionOrigins = true;
            t.SelectedIndex = 0;

            List<TransitionerSlide> slideList = new List<TransitionerSlide>();

            foreach (MenuCategories b in Enum.GetValues(typeof(MenuCategories)))
            {
                if(b == MenuCategories.Logout) { continue; }
                TransitionerSlide ts = new TransitionerSlide();
                ts.OpeningEffect = new TransitionEffect(TransitionEffectKind.FadeIn);

                SlideWipe back = new SlideWipe();
                back.Direction = SlideDirection.Left;
                ts.BackwardWipe = back;

                SlideWipe forwards = new SlideWipe();
                forwards.Direction = SlideDirection.Right;
                ts.ForwardWipe = forwards;

                ContentControl categoryContent = new ContentControl();

                switch (b)
                {
                    case MenuCategories.MySemester:
                        MySemesterView msv = new MySemesterView();
                        mySemesterView = msv;
                        categoryContent.Content = mySemesterView;
                        break;
                    case MenuCategories.Registration:
                        categoryContent.Content = new RegistrationView();
                        break;
                    case MenuCategories.PersonalInfo:
                        categoryContent.Content = new PersonalInfoView();
                        break;
                    case MenuCategories.Tools:
                        categoryContent.Content = new ToolsView();
                        break;
                    case MenuCategories.Settings:
                        categoryContent.Content = new SettingsView();
                        break;
                }

                ts.Content = categoryContent;
                slideList.Add(ts);
            }

            t.ItemsSource = slideList;
            MainViewTransitionerControl.Content = t;
            mainViewTransitioner = t;
        }

        private void onMenuButtonPressed(MenuCategories b)
        {
            if(b == MenuCategories.Logout)
            {
                createLogoutRequest();
            }
            else
            {
                selectedTab = b;
                mainViewTransitioner.SelectedIndex = (int)b;
            }
            
        }

        private async void createLogoutRequest()
        {
            LoadingView lv = new LoadingView();
            var result = await LoadingDialog.ShowDialog(lv, delegate (object s, DialogOpenedEventArgs a)
            {
                LogoutRequest(lv);

            }, delegate (object s, DialogClosingEventArgs a)
            {
                if (lv.ResponseStatus == 1)
                {
                    var par = App.Current.MainWindow as MainWindow;
                    par.OnLogoutSuccess();
                }
            });
            return;
        }

        private async void LogoutRequest(LoadingView lv)
        {
            try
            {
                bool lo = await Rest.CreateAuthLogoutRequest();

                lv.OnRequestSuccess();
            }
            catch (RequestFailedException e)
            {
                lv.OnRequestFailed(new Tuple<int, string>(e.Code, e.Message));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        private void RaisePropertyChanged(string propertyName)
        {
            var handlers = PropertyChanged;

            handlers(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
