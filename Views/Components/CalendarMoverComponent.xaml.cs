﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Ultimate_Schedule_Builder.Models;

namespace Ultimate_Schedule_Builder.Views.Components
{
    /// <summary>
    /// Interaction logic for CalendarMoverComponent.xaml
    /// </summary>
    public partial class CalendarMoverComponent : UserControl
    {
        private CalendarComponent cRef;

        private DateTime DisplayTime = DateTime.Today;
        private CalendarQueryType QueryType = CalendarQueryType.Static;



        public CalendarMoverComponent(CalendarComponent cc)
        {
            InitializeComponent();
            DataContext = this;

            this.cRef = cc;

            this.cRef.OnDateChange += OnDateChange;
            this.cRef.OnQueryTypeChange += OnQueryTypeChange;

            this.MoverGrid.IsEnabled = false;

            UpdateDisplayDate();
        }

        private void OnDateChange(DateTime d)
        {
            this.DisplayTime = d;
            UpdateDisplayDate();
        }

        private void OnQueryTypeChange(CalendarQueryType t)
        {
            switch (t)
            {
                case CalendarQueryType.Static:
                    this.MoverGrid.IsEnabled = false;
                    this.QueryType = t;
                    UpdateDisplayDate();
                    break;
                case CalendarQueryType.Week:
                    this.MoverGrid.IsEnabled = true;
                    this.QueryType = t;
                    UpdateDisplayDate();
                    break;
                case CalendarQueryType.Day:
                    this.MoverGrid.IsEnabled = true;
                    this.QueryType = t;
                    UpdateDisplayDate();
                    break;
            }
        }

        private void UpdateDisplayDate()
        {
            switch (this.QueryType)
            {
                case CalendarQueryType.Static:
                    this.CalendarText.Text = "Selected courses";
                    break;
                case CalendarQueryType.Week:
                    DateTime ws = Utility.FirstDayOfWeek(this.DisplayTime);
                    DateTime we = Utility.LastDayOfWeek(this.DisplayTime);

                    this.CalendarText.Text = $"{ws.ToString("dddd")}, {ws.ToString("MMM")} {ws.ToString("M")}, {ws.ToString("yyyy")} - {we.ToString("dddd")}, {we.ToString("MMM")} {we.ToString("M")}, {we.ToString("yyyy")}";
                    break;
                case CalendarQueryType.Day:
                    this.CalendarText.Text = $"{this.DisplayTime.ToString("dddd")}, {this.DisplayTime.ToString("MMM")} {this.DisplayTime.ToString("M")}, {this.DisplayTime.ToString("yyyy")}";
                    break;
            }
            
        }

        private void MoveLeft_Click(object sender, RoutedEventArgs e)
        {
            this.cRef.MoveCalendarPrevious();
        }

        private void MoveRight_Click(object sender, RoutedEventArgs e)
        {
            this.cRef.MoveCalendarNext();
        }
    }
}
