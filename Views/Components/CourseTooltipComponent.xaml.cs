﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using Ultimate_Schedule_Builder.Models;

namespace Ultimate_Schedule_Builder.Views.Components
{
    /// <summary>
    /// Interaction logic for CourseTooltipComponent.xaml
    /// </summary>
    public partial class CourseTooltipComponent : UserControl
    {
        public int Id { get; private set; }

        private StagBuildingList[] selectedBuildings = null;
        private ScheduleComponent scRef = null;
        private StackPanel localStudentHolder = null;
        private bool openned = false;
        private bool isTerm = false;
        private bool isEmpty = false;
        private string eventId;

        public CourseTooltipComponent(Course c, Popup p, int id, ScheduleDay sd)
        {
            InitializeComponent();
            DataContext = this;

            this.Id = id;
            this.isTerm = c.IsTerm;
            this.isEmpty = c.IsEmpty;

            if (c.IsTerm)
            {
                this.eventId = c.CourseTerm.TermId;
            }
            else
            {
                if (!c.IsEmpty)
                {
                    this.eventId = c.CourseScheduleId;
                }
            }

            this.createDayString(sd);
            this.preparePeopleSlide();
            this.createTooltip(c, p);
        }

        public CourseTooltipComponent(Course c, Popup p, int id, ScheduleDay sd, ScheduleComponent scr)
        {
            InitializeComponent();
            DataContext = this;

            this.Id = id;
            this.scRef = scr;
            this.isTerm = c.IsTerm;
            this.isEmpty = c.IsEmpty;

            if (c.IsTerm)
            {
                this.eventId = c.CourseTerm.TermId;
            }
            else
            {
                if (!c.IsEmpty)
                {
                    this.eventId = c.CourseScheduleId;
                }
            }

            this.createDayString(sd);
            this.preparePeopleSlide();
            this.createTooltip(c, p);
        }

        private void createDayString(ScheduleDay sd)
        {
            Dictionary<StagBuildingList, string> roomList = new Dictionary<StagBuildingList, string>();

            foreach (List<Course> cList in sd.Courses)
            {
                foreach (Course c in cList)
                {
                    if (c != null)
                    {
                        if (c.CourseRoom != null && c.CourseBuilding != null)
                        {
                            if (Enum.TryParse<StagBuildingList>(c.CourseBuilding, out StagBuildingList cbr))
                            {
                                if (!roomList.ContainsKey(cbr))
                                {
                                    roomList.Add(cbr, c.CourseRoom);
                                }
                            }
                        }
                    }
                }
            }

            this.selectedBuildings = roomList.Keys.ToArray();
        }

        private async void findRoom(int pos, string dstring)
        {
            StagBuildingList[] buildings = this.selectedBuildings;

            RoomSearchComponent rsc;

            if (buildings.Length > 0)
            {
                rsc = new RoomSearchComponent(pos, dstring, buildings);
            }
            else
            {
                rsc = new RoomSearchComponent(pos, dstring);
            }

            var par = App.Current.MainWindow as MainWindow;
            var result = await par.MV.LoadingDialog.ShowDialog(rsc);

            //? Do something
        }

        private async void registerTerm(string tid, bool t)
        {
            var par = App.Current.MainWindow as MainWindow;
            LoadingView lv = new LoadingView();
            var result = await par.MV.LoadingDialog.ShowDialog(lv, delegate (object s, DialogOpenedEventArgs a)
            {
                try
                {
                    SetTermState(tid, t, (res) =>
                    {
                        scRef.ReloadTermSchedule();
                        lv.OnRequestSuccess();
                    });
                }
                catch (RequestFailedException e)
                {
                    scRef.ReloadTermSchedule();
                    lv.OnRequestFailed(new Tuple<int, string>(e.Code, e.Message));
                }
            });
        }

        private async void SetTermState(string tid, bool t, Action<bool> ret)
        {
            var result = await Rest.CreateStudentSetTermRequest(tid, t);
            ret(result);
        }

        private async void GetStudentsOnTooltip()
        {
            if (this.isTerm)
            {
                var result = await Rest.CreateStudentGetByTermRequest(this.eventId);
                this.renderStudents(new List<StagStudent>(result));
            }
            else
            {
                var result = await Rest.CreateStudentGetByCourseRequest(this.eventId);
                this.renderStudents(new List<StagStudent>(result));
            }
        }

        private void renderStudents(List<StagStudent> slist)
        {
            localStudentHolder.Children.Clear();
            foreach (StagStudent st in slist)
            {
                Expander uExpander = new Expander();
                uExpander.Background = Brushes.White;

                Grid uehtg = new Grid();
                uExpander.Header = uehtg;

                StackPanel uGridContent = new StackPanel();
                uGridContent.HorizontalAlignment = HorizontalAlignment.Left;
                uGridContent.Orientation = Orientation.Horizontal;
                uehtg.Children.Add(uGridContent);

                PackIcon userIcon = new PackIcon();
                userIcon.Kind = PackIconKind.AccountCircle;
                userIcon.VerticalAlignment = VerticalAlignment.Center;
                userIcon.Width = 30;
                userIcon.Height = 30;
                userIcon.Margin = new Thickness(0, 0, 10, 0);
                userIcon.Foreground = Brushes.Black;
                userIcon.Foreground = new SolidColorBrush(Color.FromRgb(95, 151, 251));
                uGridContent.Children.Add(userIcon);

                TextBlock userText = new TextBlock();
                userText.VerticalAlignment = VerticalAlignment.Center;
                userText.FontSize = 18;
                userText.Foreground = Brushes.Black;
                userText.Margin = new Thickness(5, 0, 0, 0);
                userText.Text = $"{st.stagUserFirstName} {st.stagUserLastName}";
                uGridContent.Children.Add(userText);

                Grid eContent = new Grid();
                eContent.Margin = new Thickness(25, 0, 25, 0);
                uExpander.Content = eContent;

                for (int i = 0; i < 6; i++)
                {
                    RowDefinition nrd = new RowDefinition();
                    nrd.Height = new GridLength(25);
                    eContent.RowDefinitions.Add(nrd);

                    StackPanel cData = new StackPanel();
                    cData.Orientation = Orientation.Horizontal;
                    Grid.SetRow(cData, i);
                    eContent.Children.Add(cData);

                    PackIcon cdI = new PackIcon();
                    cdI.Height = 25;
                    cdI.Width = 25;
                    cdI.Foreground = Brushes.Black;
                    cData.Children.Add(cdI);

                    TextBlock cdT = new TextBlock();
                    cdT.Margin = new Thickness(25, 0, 0, 0);
                    cdT.VerticalAlignment = VerticalAlignment.Center;
                    cdT.Foreground = Brushes.Black;
                    cData.Children.Add(cdT);

                    switch (i)
                    {
                        case 0:
                            cData.ToolTip = "StagUserName";
                            cdI.Kind = PackIconKind.AccountKey;
                            cdT.Text = st.stagUserName;
                            break;

                        case 1:
                            cData.ToolTip = "StagUserOsCislo";
                            cdI.Kind = PackIconKind.AccountSearch;
                            cdT.Text = st.stagId;
                            break;

                        case 2:
                            cData.ToolTip = "StagName";
                            cdI.Kind = PackIconKind.AccountCardDetails;
                            cdT.Text = $"{st.stagUserFirstName} {st.stagUserLastName}";
                            break;

                        case 3:
                            cData.ToolTip = "StagUserFaculty";
                            cdI.Kind = PackIconKind.Bank;
                            cdT.Text = st.stagUserFaculty.ToString();
                            break;

                        case 4:
                            cData.ToolTip = "StagUserField";
                            cdI.Kind = PackIconKind.Briefcase;
                            cdT.Text = st.stagUserField;
                            break;

                        case 5:
                            cData.ToolTip = "StagUserCurrentGrade";
                            cdI.Kind = PackIconKind.FormatListNumbers;
                            cdT.Text = st.stagUserCurrentGrade.ToString();
                            break;
                    }
                }

                localStudentHolder.Children.Add(uExpander);
            }
        }

        public void OnOpen()
        {
            if (!openned)
            {
                openned = true;
                if (!this.isEmpty)
                {
                    GetStudentsOnTooltip();
                }
            }
            else
            {
                if (TooltipTransitioner.SelectedIndex == 1)
                {
                    TooltipTransitioner.SelectedIndex = 0;
                }
            }
        }

        private void preparePeopleSlide()
        {
            Grid courseTooltip = new Grid();
            courseTooltip.RowDefinitions.Add(new RowDefinition() { MinHeight = 80 });
            courseTooltip.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(190) });
            GridStudentsContent.Content = courseTooltip;

            Grid ctHeader = new Grid();
            ctHeader.RowDefinitions.Add(new RowDefinition() { MinHeight = 30 });
            ctHeader.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            ctHeader.Background = Settings.ApplicationColorSchema.MainColor;
            Grid.SetRow(ctHeader, 0);
            Grid.SetColumn(ctHeader, 0);
            courseTooltip.Children.Add(ctHeader);

            TextBlock ctHeaderWholeName = new TextBlock();
            ctHeaderWholeName.Text = "Event attendance list:";
            ctHeaderWholeName.VerticalAlignment = VerticalAlignment.Center;
            ctHeaderWholeName.HorizontalAlignment = HorizontalAlignment.Left;
            ctHeaderWholeName.TextAlignment = TextAlignment.Left;
            ctHeaderWholeName.Margin = new Thickness(15, 5, 15, 5);
            ctHeaderWholeName.FontSize = 15;
            ctHeaderWholeName.FontWeight = FontWeights.Bold;
            ctHeaderWholeName.Foreground = Brushes.White;
            ctHeaderWholeName.TextWrapping = TextWrapping.WrapWithOverflow;
            Grid.SetRow(ctHeaderWholeName, 0);
            Grid.SetColumn(ctHeaderWholeName, 0);
            ctHeader.Children.Add(ctHeaderWholeName);

            Button backButton = new Button();
            backButton.VerticalAlignment = VerticalAlignment.Top;
            backButton.HorizontalAlignment = HorizontalAlignment.Right;
            backButton.Margin = new Thickness(5, 5, 15, 5);
            backButton.Height = 30;
            backButton.Width = 30;
            backButton.Background = Brushes.Transparent;
            backButton.BorderThickness = new Thickness(0);
            ctHeader.Children.Add(backButton);

            PackIcon btnIcon = new PackIcon();
            btnIcon.Height = 16;
            btnIcon.Width = 16;
            btnIcon.HorizontalAlignment = HorizontalAlignment.Center;
            btnIcon.VerticalAlignment = VerticalAlignment.Center;
            btnIcon.Kind = PackIconKind.ArrowLeft;
            btnIcon.Foreground = Brushes.White;
            btnIcon.Margin = new Thickness(-9, 0, -9, 0);

            backButton.Content = btnIcon;
            backButton.Click += (obj, arg) =>
            {
                TooltipTransitioner.SelectedIndex = 0;
            };

            Grid studentHolderGrid = new Grid();
            studentHolderGrid.Background = Brushes.White;
            Grid.SetRow(studentHolderGrid, 1);
            courseTooltip.Children.Add(studentHolderGrid);

            ScrollViewer shView = new ScrollViewer();
            shView.Height = 190;
            shView.HorizontalScrollBarVisibility = ScrollBarVisibility.Hidden;
            shView.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            studentHolderGrid.Children.Add(shView);

            StackPanel studentHolder = new StackPanel();
            studentHolder.Orientation = Orientation.Vertical;
            shView.Content = studentHolder;
            localStudentHolder = studentHolder;
        }

        private void createTooltip(Course c, Popup p)
        {
            Grid courseTooltip = new Grid();
            courseTooltip.RowDefinitions.Add(new RowDefinition() { MinHeight = 80 });
            courseTooltip.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            GridContent.Content = courseTooltip;

            Grid ctHeader = new Grid();
            ctHeader.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            ctHeader.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });

            ctHeader.RowDefinitions.Add(new RowDefinition() { MinHeight = 30 });
            ctHeader.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(20) });
            ctHeader.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(20) });

            ctHeader.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(10) });

            ctHeader.Background = Settings.ApplicationColorSchema.MainColor;
            OnePolygonBoi.Fill = Settings.ApplicationColorSchema.MainColor;
            Grid.SetRow(ctHeader, 0);
            Grid.SetColumn(ctHeader, 0);
            courseTooltip.Children.Add(ctHeader);

            TextBlock ctHeaderWholeName = new TextBlock();
            ctHeaderWholeName.Text = (c.IsEmpty ? "Free schedule hour" : c.CourseName);
            ctHeaderWholeName.VerticalAlignment = VerticalAlignment.Center;
            ctHeaderWholeName.HorizontalAlignment = HorizontalAlignment.Left;
            ctHeaderWholeName.TextAlignment = TextAlignment.Left;
            ctHeaderWholeName.Margin = new Thickness(15, 5, 15, 5);
            ctHeaderWholeName.FontSize = 15;
            ctHeaderWholeName.FontWeight = FontWeights.Bold;
            ctHeaderWholeName.Foreground = Brushes.White;
            ctHeaderWholeName.TextWrapping = TextWrapping.WrapWithOverflow;
            Grid.SetRow(ctHeaderWholeName, 0);
            Grid.SetColumn(ctHeaderWholeName, 0);
            Grid.SetColumnSpan(ctHeaderWholeName, 2);
            ctHeader.Children.Add(ctHeaderWholeName);

            StackPanel ctHeaderTime = new StackPanel();
            ctHeaderTime.Height = 20;
            ctHeaderTime.Margin = new Thickness(15, 0, 15, 0);
            ctHeaderTime.Orientation = Orientation.Horizontal;
            Grid.SetColumn(ctHeaderTime, 0);
            Grid.SetRow(ctHeaderTime, 1);
            ctHeader.Children.Add(ctHeaderTime);

            PackIcon ctHeaderTimeIcon = new PackIcon();
            ctHeaderTimeIcon.Height = 15;
            ctHeaderTimeIcon.Width = 15;
            ctHeaderTimeIcon.VerticalAlignment = VerticalAlignment.Center;
            ctHeaderTimeIcon.Foreground = Brushes.WhiteSmoke;
            ctHeaderTimeIcon.Kind = PackIconKind.Clock;
            ctHeaderTime.Children.Add(ctHeaderTimeIcon);

            TextBlock ctHeaderTimeText = new TextBlock();
            ctHeaderTimeText.Text = c.Start.Hour + ":00 - " + (c.Start.Hour + c.Duration) + ":00";
            ctHeaderTimeText.VerticalAlignment = VerticalAlignment.Center;
            ctHeaderTimeText.Margin = new Thickness(5, 0, 0, 0);
            ctHeaderTimeText.FontSize = 10;
            ctHeaderTimeText.Foreground = Brushes.WhiteSmoke;
            ctHeaderTime.Children.Add(ctHeaderTimeText);

            StackPanel ctHeaderDate = new StackPanel();
            ctHeaderDate.Height = 20;
            ctHeaderDate.Margin = new Thickness(15, 0, 15, 0);
            ctHeaderDate.Orientation = Orientation.Horizontal;
            Grid.SetColumn(ctHeaderDate, 1);
            Grid.SetRow(ctHeaderDate, 1);
            ctHeader.Children.Add(ctHeaderDate);

            PackIcon ctHeaderDateIcon = new PackIcon();
            ctHeaderDateIcon.Height = 15;
            ctHeaderDateIcon.Width = 15;
            ctHeaderDateIcon.VerticalAlignment = VerticalAlignment.Center;
            ctHeaderDateIcon.Foreground = Brushes.WhiteSmoke;
            ctHeaderDateIcon.Kind = PackIconKind.Calendar;
            ctHeaderDate.Children.Add(ctHeaderDateIcon);

            TextBlock ctHeaderDateText = new TextBlock();
            ctHeaderDateText.Text = c.Start.ScheduleDate.ToString("ddd, MMM dd");
            ctHeaderDateText.VerticalAlignment = VerticalAlignment.Center;
            ctHeaderDateText.Margin = new Thickness(5, 0, 0, 0);
            ctHeaderDateText.FontSize = 10;
            ctHeaderDateText.Foreground = Brushes.WhiteSmoke;
            ctHeaderDate.Children.Add(ctHeaderDateText);

            StackPanel ctHeaderLocation = new StackPanel();
            ctHeaderLocation.Height = 20;
            ctHeaderLocation.Margin = new Thickness(15, 0, 15, 0);
            ctHeaderLocation.Orientation = Orientation.Horizontal;
            Grid.SetColumn(ctHeaderLocation, 0);
            Grid.SetRow(ctHeaderLocation, 2);
            if (!c.IsEmpty)
            {
                ctHeader.Children.Add(ctHeaderLocation);
            }

            PackIcon ctHeaderLocationIcon = new PackIcon();
            ctHeaderLocationIcon.Height = 15;
            ctHeaderLocationIcon.Width = 15;
            ctHeaderLocationIcon.VerticalAlignment = VerticalAlignment.Center;
            ctHeaderLocationIcon.Foreground = Brushes.WhiteSmoke;
            ctHeaderLocationIcon.Kind = PackIconKind.MapMarker;
            ctHeaderLocation.Children.Add(ctHeaderLocationIcon);

            TextBlock ctHeaderLocationText = new TextBlock();
            ctHeaderLocationText.Text = c.CourseBuilding + " / " + c.CourseRoom;
            ctHeaderLocationText.VerticalAlignment = VerticalAlignment.Center;
            ctHeaderLocationText.Margin = new Thickness(5, 0, 0, 0);
            ctHeaderLocationText.FontSize = 10;
            ctHeaderLocationText.Foreground = Brushes.WhiteSmoke;
            ctHeaderLocation.Children.Add(ctHeaderLocationText);

            StackPanel ctHeaderOwner = new StackPanel();
            ctHeaderOwner.Height = 20;
            ctHeaderOwner.Orientation = Orientation.Horizontal;
            ctHeaderOwner.Margin = new Thickness(15, 0, 15, 0);
            ctHeaderOwner.VerticalAlignment = VerticalAlignment.Stretch;
            ctHeaderOwner.HorizontalAlignment = HorizontalAlignment.Stretch;
            Grid.SetColumn(ctHeaderOwner, (c.IsEmpty ? 0 : 1));
            Grid.SetRow(ctHeaderOwner, 2);
            if (c.Owner != null)
            {
                ctHeader.Children.Add(ctHeaderOwner);
            }

            PackIcon ctHeaderOwnerIcon = new PackIcon();
            ctHeaderOwnerIcon.Height = 15;
            ctHeaderOwnerIcon.Width = 15;
            ctHeaderOwnerIcon.VerticalAlignment = VerticalAlignment.Center;
            ctHeaderOwnerIcon.Foreground = Brushes.WhiteSmoke;
            ctHeaderOwnerIcon.Kind = PackIconKind.Account;
            ctHeaderOwner.Children.Add(ctHeaderOwnerIcon);

            TextBlock ctHeaderOwnerText = new TextBlock();
            ctHeaderOwnerText.VerticalAlignment = VerticalAlignment.Center;
            ctHeaderOwnerText.Margin = new Thickness(5, 0, 0, 0);
            ctHeaderOwnerText.FontSize = 10;
            ctHeaderOwnerText.Foreground = Brushes.WhiteSmoke;
            ctHeaderOwner.Children.Add(ctHeaderOwnerText);

            if (c.Owner != null)
            {
                ctHeaderOwnerText.Text = c.Owner.stagUserFirstName + " " + c.Owner.stagUserLastName;
            }

            Grid ctContent = new Grid();
            ctContent.Background = Brushes.White;
            Grid.SetRow(ctContent, 1);
            Grid.SetColumn(ctContent, 0);
            courseTooltip.Children.Add(ctContent);

            if (c.IsEmpty)
            {
                ctContent.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });

                Grid bgrid = new Grid();
                bgrid.Height = 100;
                ctContent.Children.Add(bgrid);

                Button findRoomButton = new Button();
                bgrid.Children.Add(findRoomButton);
                findRoomButton.VerticalAlignment = VerticalAlignment.Center;
                findRoomButton.HorizontalAlignment = HorizontalAlignment.Center;
                findRoomButton.Style = (Style)FindResource("MaterialDesignRaisedButton");
                findRoomButton.Height = 50;
                findRoomButton.Width = 150;
                findRoomButton.Content = "Find room";

                findRoomButton.Click += (obj, arg) =>
                {
                    p.IsOpen = false;
                    findRoom(c.Start.SchedulePosition, c.Start.ScheduleDateString);
                };
            }
            else
            {
                if (c.IsTerm)
                {
                    ctContent.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
                    ctContent.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });

                    StackPanel cntUpper = new StackPanel();
                    cntUpper.Margin = new Thickness(15, 0, 15, 10);
                    cntUpper.Background = Brushes.White;
                    cntUpper.Orientation = Orientation.Vertical;
                    Grid.SetRow(cntUpper, 0);
                    ctContent.Children.Add(cntUpper);

                    TextBlock cntTextHeader = new TextBlock();
                    cntTextHeader.VerticalAlignment = VerticalAlignment.Center;
                    cntTextHeader.HorizontalAlignment = HorizontalAlignment.Left;
                    cntTextHeader.Margin = new Thickness(0, 10, 0, 10);
                    cntTextHeader.FontSize = 12;
                    cntTextHeader.Foreground = Brushes.DarkGray;
                    cntTextHeader.Text = "Professor: ";
                    cntUpper.Children.Add(cntTextHeader);

                    StackPanel ctContentTeacherPanel = new StackPanel();
                    ctContentTeacherPanel.Orientation = Orientation.Horizontal;
                    ctContentTeacherPanel.VerticalAlignment = VerticalAlignment.Stretch;
                    ctContentTeacherPanel.HorizontalAlignment = HorizontalAlignment.Stretch;
                    cntUpper.Children.Add(ctContentTeacherPanel);

                    PackIcon ctContentTeacherIcon = new PackIcon();
                    ctContentTeacherIcon.Height = 15;
                    ctContentTeacherIcon.Width = 15;
                    ctContentTeacherIcon.VerticalAlignment = VerticalAlignment.Center;
                    ctContentTeacherIcon.Kind = PackIconKind.School;
                    ctContentTeacherIcon.Foreground = new SolidColorBrush(Color.FromRgb(250, 190, 88));
                    ctContentTeacherPanel.Children.Add(ctContentTeacherIcon);

                    TextBlock ctContentTeacherText = new TextBlock();
                    ctContentTeacherText.VerticalAlignment = VerticalAlignment.Center;
                    ctContentTeacherText.Margin = new Thickness(5, 0, 0, 0);
                    ctContentTeacherText.FontSize = 10;
                    ctContentTeacherText.Foreground = Brushes.Black;
                    ctContentTeacherPanel.Children.Add(ctContentTeacherText);

                    if (c.CourseTeacher != null)
                    {
                        ctContentTeacherText.Text = c.CourseTeacher.stagUserFirstName + " " + c.CourseTeacher.stagUserLastName;
                    }

                    Grid otherContentGrid = new Grid();
                    otherContentGrid.Background = new SolidColorBrush(Color.FromRgb(238, 243, 244));
                    Grid.SetRow(otherContentGrid, 1);
                    ctContent.Children.Add(otherContentGrid);

                    otherContentGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(20) });
                    otherContentGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(50) });
                    otherContentGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(20) });

                    otherContentGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                    otherContentGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });

                    Button dropClassButton = new Button();
                    Grid.SetColumn(dropClassButton, 0);
                    Grid.SetRow(dropClassButton, 1);
                    otherContentGrid.Children.Add(dropClassButton);
                    dropClassButton.Style = (Style)FindResource("MaterialDesignRaisedButton");
                    dropClassButton.Background = Brushes.White;
                    dropClassButton.BorderBrush = Brushes.Gray;

                    dropClassButton.IsEnabled = c.CourseTerm.TermCanUnregister;
                    RippleAssist.SetFeedback(dropClassButton, Brushes.DarkGray);

                    StackPanel dcbPanel = new StackPanel();
                    dcbPanel.Orientation = Orientation.Horizontal;
                    dcbPanel.VerticalAlignment = VerticalAlignment.Stretch;
                    dcbPanel.HorizontalAlignment = HorizontalAlignment.Stretch;

                    PackIcon dcbIcon = new PackIcon();
                    dcbIcon.Height = 15;
                    dcbIcon.Width = 15;
                    dcbIcon.VerticalAlignment = VerticalAlignment.Center;
                    dcbPanel.Children.Add(dcbIcon);

                    TextBlock dcbText = new TextBlock();
                    dcbText.VerticalAlignment = VerticalAlignment.Center;
                    dcbText.Margin = new Thickness(5, 0, 0, 0);
                    dcbText.FontSize = 10;
                    dcbPanel.Children.Add(dcbText);

                    dropClassButton.Content = dcbPanel;
                    dropClassButton.Margin = new Thickness(15, 0, 5, 0);

                    if (c.CourseTerm.TermRegistered)
                    {
                        dcbText.Text = "Drop term";
                        dcbText.Foreground = Brushes.Red;

                        dcbIcon.Kind = PackIconKind.CloseCircleOutline;
                        dcbIcon.Foreground = Brushes.Red;
                    }
                    else
                    {
                        dcbText.Text = "Register";
                        dcbText.Foreground = Brushes.LightGreen;

                        dcbIcon.Kind = PackIconKind.CheckCircleOutline;
                        dcbIcon.Foreground = Brushes.LightGreen;
                    }

                    dropClassButton.Click += (obj, arg) =>
                    {
                        p.IsOpen = false;
                        registerTerm(c.CourseTerm.TermId, !c.CourseTerm.TermRegistered);
                        scRef.PopupTooltip.Child = null;
                    };

                    Button attendanceListButton = new Button();
                    Grid.SetColumn(attendanceListButton, 1);
                    Grid.SetRow(attendanceListButton, 1);
                    otherContentGrid.Children.Add(attendanceListButton);
                    attendanceListButton.Style = (Style)FindResource("MaterialDesignRaisedButton");
                    attendanceListButton.Background = Brushes.White;
                    attendanceListButton.BorderBrush = Brushes.Gray;

                    RippleAssist.SetFeedback(attendanceListButton, Brushes.DarkGray);

                    StackPanel albPanel = new StackPanel();
                    albPanel.Orientation = Orientation.Horizontal;
                    albPanel.VerticalAlignment = VerticalAlignment.Stretch;
                    albPanel.HorizontalAlignment = HorizontalAlignment.Stretch;

                    PackIcon albbIcon = new PackIcon();
                    albbIcon.Height = 15;
                    albbIcon.Width = 15;
                    albbIcon.VerticalAlignment = VerticalAlignment.Center;
                    albbIcon.Kind = PackIconKind.FormatListBulleted;
                    albbIcon.Foreground = Brushes.Blue;
                    albPanel.Children.Add(albbIcon);

                    TextBlock albText = new TextBlock();
                    albText.VerticalAlignment = VerticalAlignment.Center;
                    albText.Margin = new Thickness(5, 0, 0, 0);
                    albText.FontSize = 10;
                    albText.Foreground = Brushes.Blue;
                    albText.Text = "Student list";
                    albPanel.Children.Add(albText);

                    attendanceListButton.Content = albPanel;
                    attendanceListButton.Margin = new Thickness(5, 0, 15, 0);
                    attendanceListButton.Click += (obj, arg) =>
                    {
                        TooltipTransitioner.SelectedIndex = 1;
                    };

                    ShadowAssist.SetShadowDepth(dropClassButton, ShadowDepth.Depth0);
                    ShadowAssist.SetShadowDepth(attendanceListButton, ShadowDepth.Depth0);
                }
                else
                {
                    ctContent.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
                    ctContent.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });

                    StackPanel cntUpper = new StackPanel();
                    cntUpper.Margin = new Thickness(15, 0, 15, 10);
                    cntUpper.Background = Brushes.White;
                    cntUpper.Orientation = Orientation.Vertical;
                    Grid.SetRow(cntUpper, 0);
                    ctContent.Children.Add(cntUpper);

                    TextBlock cntTextHeader = new TextBlock();
                    cntTextHeader.VerticalAlignment = VerticalAlignment.Center;
                    cntTextHeader.HorizontalAlignment = HorizontalAlignment.Left;
                    cntTextHeader.Margin = new Thickness(0, 10, 0, 10);
                    cntTextHeader.FontSize = 12;
                    cntTextHeader.Foreground = Brushes.DarkGray;
                    cntTextHeader.Text = "Professor: ";
                    cntUpper.Children.Add(cntTextHeader);

                    StackPanel ctContentTeacherPanel = new StackPanel();
                    ctContentTeacherPanel.Orientation = Orientation.Horizontal;
                    ctContentTeacherPanel.VerticalAlignment = VerticalAlignment.Stretch;
                    ctContentTeacherPanel.HorizontalAlignment = HorizontalAlignment.Stretch;
                    cntUpper.Children.Add(ctContentTeacherPanel);

                    PackIcon ctContentTeacherIcon = new PackIcon();
                    ctContentTeacherIcon.Height = 15;
                    ctContentTeacherIcon.Width = 15;
                    ctContentTeacherIcon.VerticalAlignment = VerticalAlignment.Center;
                    ctContentTeacherIcon.Kind = PackIconKind.School;
                    ctContentTeacherIcon.Foreground = new SolidColorBrush(Color.FromRgb(250, 190, 88));
                    ctContentTeacherPanel.Children.Add(ctContentTeacherIcon);

                    TextBlock ctContentTeacherText = new TextBlock();
                    ctContentTeacherText.VerticalAlignment = VerticalAlignment.Center;
                    ctContentTeacherText.Margin = new Thickness(5, 0, 0, 0);
                    ctContentTeacherText.FontSize = 10;
                    ctContentTeacherText.Foreground = Brushes.Black;
                    ctContentTeacherPanel.Children.Add(ctContentTeacherText);

                    if (c.CourseTeacher != null)
                    {
                        ctContentTeacherText.Text = c.CourseTeacher.stagUserFirstName + " " + c.CourseTeacher.stagUserLastName;
                    }

                    Grid otherContentGrid = new Grid();
                    otherContentGrid.Background = new SolidColorBrush(Color.FromRgb(238, 243, 244));
                    Grid.SetRow(otherContentGrid, 1);
                    ctContent.Children.Add(otherContentGrid);

                    otherContentGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(20) });
                    otherContentGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(50) });
                    otherContentGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(20) });

                    otherContentGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                    otherContentGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });

                    Button dropClassButton = new Button();
                    Grid.SetColumn(dropClassButton, 0);
                    Grid.SetRow(dropClassButton, 1);
                    otherContentGrid.Children.Add(dropClassButton);
                    dropClassButton.Style = (Style)FindResource("MaterialDesignRaisedButton");
                    dropClassButton.Background = Brushes.White;
                    dropClassButton.BorderBrush = Brushes.Gray;

                    RippleAssist.SetFeedback(dropClassButton, Brushes.Black);

                    StackPanel dcbPanel = new StackPanel();
                    dcbPanel.Orientation = Orientation.Horizontal;
                    dcbPanel.VerticalAlignment = VerticalAlignment.Stretch;
                    dcbPanel.HorizontalAlignment = HorizontalAlignment.Stretch;

                    PackIcon dcbIcon = new PackIcon();
                    dcbIcon.Height = 15;
                    dcbIcon.Width = 15;
                    dcbIcon.VerticalAlignment = VerticalAlignment.Center;
                    dcbIcon.Kind = PackIconKind.CloseCircleOutline;
                    dcbIcon.Foreground = Brushes.Red;
                    dcbPanel.Children.Add(dcbIcon);

                    TextBlock dcbText = new TextBlock();
                    dcbText.VerticalAlignment = VerticalAlignment.Center;
                    dcbText.Margin = new Thickness(5, 0, 0, 0);
                    dcbText.FontSize = 10;
                    dcbText.Foreground = Brushes.Red;
                    dcbText.Text = "Drop Class";
                    dcbPanel.Children.Add(dcbText);

                    dropClassButton.Content = dcbPanel;
                    dropClassButton.Margin = new Thickness(15, 0, 5, 0);

                    Button attendanceListButton = new Button();
                    Grid.SetColumn(attendanceListButton, 1);
                    Grid.SetRow(attendanceListButton, 1);
                    otherContentGrid.Children.Add(attendanceListButton);
                    attendanceListButton.Style = (Style)FindResource("MaterialDesignRaisedButton");
                    attendanceListButton.Background = Brushes.White;
                    attendanceListButton.BorderBrush = Brushes.Gray;

                    RippleAssist.SetFeedback(attendanceListButton, Brushes.DarkGray);

                    StackPanel albPanel = new StackPanel();
                    albPanel.Orientation = Orientation.Horizontal;
                    albPanel.VerticalAlignment = VerticalAlignment.Stretch;
                    albPanel.HorizontalAlignment = HorizontalAlignment.Stretch;

                    PackIcon albbIcon = new PackIcon();
                    albbIcon.Height = 15;
                    albbIcon.Width = 15;
                    albbIcon.VerticalAlignment = VerticalAlignment.Center;
                    albbIcon.Kind = PackIconKind.FormatListBulleted;
                    albbIcon.Foreground = Brushes.Blue;
                    albPanel.Children.Add(albbIcon);

                    TextBlock albText = new TextBlock();
                    albText.VerticalAlignment = VerticalAlignment.Center;
                    albText.Margin = new Thickness(5, 0, 0, 0);
                    albText.FontSize = 10;
                    albText.Foreground = Brushes.Blue;
                    albText.Text = "Student list";
                    albPanel.Children.Add(albText);

                    attendanceListButton.Content = albPanel;
                    attendanceListButton.Margin = new Thickness(5, 0, 15, 0);

                    attendanceListButton.Click += (obj, arg) =>
                    {
                        TooltipTransitioner.SelectedIndex = 1;
                    };

                    ShadowAssist.SetShadowDepth(dropClassButton, ShadowDepth.Depth0);
                    ShadowAssist.SetShadowDepth(attendanceListButton, ShadowDepth.Depth0);
                }
            }
        }
    }
}