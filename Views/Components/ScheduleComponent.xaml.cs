﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


using Newtonsoft.Json;

using MaterialDesignThemes.Wpf;

using Ultimate_Schedule_Builder.Models;
using System.Reflection;

namespace Ultimate_Schedule_Builder.Views.Components
{
    /// <summary>
    /// Interaction logic for ScheduleComponent.xaml
    /// </summary>
    public partial class ScheduleComponent : UserControl
    {
        //Schedule addons
        private CalendarComponent ScheduleCalendarComponent = null;
        private ScheduleUsersExpanderComponent UserListComponent = null;
        private ScheduleSettingsComponent ScheduleSettingsPanel = null;

        //What to render
        private Schedule LocalSchedule = null;
        
        public DateTime SelectedDay { get; private set; } = DateTime.Today;

        //Schedule query settings
        private bool DayMode = false;
        private bool NoCreditTerms = false;
        private bool OnlyUpcoming = false;

        private bool IsTermSchedule = false;

        //Schedule render settings
        public ScheduleSettings CurrentSettings { get; private set; } = new ScheduleSettings();

        //Events
        public delegate void ScheduleSettingsArgs(DateTime SelectedDay,bool DayMode, bool NoCreditTerms, bool OnlyUpocoming);
        public event ScheduleSettingsArgs OnScheduleSettingsChange;

        public ScheduleComponent(params KeyValuePair<string,dynamic>[] args) 
        {
            InitializeComponent();
            DataContext = this;

            foreach (KeyValuePair<string,dynamic> p in args)
            {
                switch (p.Key)
                {
                    case "TargetSchedule":
                        this.LocalSchedule = (Schedule)p.Value;
                        break;
                    case "DayMode":
                        this.DayMode = (bool)p.Value;
                        break;
                    case "NoCreditTerms":
                        this.NoCreditTerms = (bool)p.Value;
                        break;
                    case "OnlyUpcoming":
                        this.NoCreditTerms = (bool)p.Value;
                        break;
                    case "DateFrom":
                        this.SelectedDay = (DateTime)p.Value;
                        break;
                    case "ScheduleCalendar":
                        this.ScheduleCalendarComponent = (CalendarComponent)p.Value;
                        this.ScheduleCalendarComponent.OnDateChange += OnCalendarDateChange;
                        this.ScheduleCalendarComponent.OnQueryTypeChange += OnCalendarQueryChange;
                        break;
                    case "UserExpander":
                        this.UserListComponent = (ScheduleUsersExpanderComponent)p.Value;
                        this.UserListComponent.OnSheduleChanged += OnScheduleChange;
                        this.UserListComponent.SetUp(this, this.LoadingDialog);
                        break;
                    case "ScheduleSettings":
                        this.ScheduleSettingsPanel = (ScheduleSettingsComponent)p.Value;
                        this.CurrentSettings = this.ScheduleSettingsPanel.SelectedSettings;
                        this.ScheduleSettingsPanel.OnScheduleSettingsChanged += OnScheduleRenderSettingsChange;
                        this.ScheduleSettingsPanel.setUp(this);
                        break;
                    case "TermListSchedule":
                        this.IsTermSchedule = (bool)p.Value;
                        break;
                }
            }

            if (this.IsTermSchedule)
            {
                //this.CurrentSettings = new ScheduleSettings(false, false, false, false);
                if (LocalSchedule != null)
                {
                    this.renderSchedule(LocalSchedule);
                }
                else
                {
                    loadTermSchedule();
                }
            }
            else
            {
                if (!NoCreditTerms)
                {
                    if (this.ScheduleCalendarComponent != null)
                    {
                        this.ScheduleCalendarComponent.SetCalendarQuery(CalendarQueryType.Week);
                    }
                }

                if (UserListComponent != null)
                {
                    if (this.CurrentSettings.DisplayMultipleSchedules)
                    {
                        this.UserListComponent.Visibility = Visibility.Visible;
                        OnScheduleSettingsChange(SelectedDay, DayMode, NoCreditTerms, OnlyUpcoming);
                    }
                    else
                    {
                        if (LocalSchedule != null)
                        {
                            this.renderSchedule(LocalSchedule);
                        }
                        else
                        {
                            loadSchedule();
                        }
                    }
                }
                else
                {
                    if (LocalSchedule != null)
                    {
                        this.renderSchedule(LocalSchedule);
                    }
                    else
                    {
                        loadSchedule();
                    }
                }
            }

            
        }
        
        private void OnScheduleRenderSettingsChange(string stName,FieldInfo f, bool val)
        {
            if(stName == "DisplayMultipleSchedules")
            {
                if(val == true && this.CurrentSettings.DisplayMultipleSchedules == false)
                {
                    f.SetValue(this.CurrentSettings, val);

                    this.UserListComponent.Visibility = Visibility.Visible;
                    LocalSchedule = null;
                    OnScheduleSettingsChange(SelectedDay, DayMode, NoCreditTerms, OnlyUpcoming);
                }
                else if(val == false && this.CurrentSettings.DisplayMultipleSchedules == true)
                {
                    f.SetValue(this.CurrentSettings, val);

                    this.UserListComponent.Visibility = Visibility.Collapsed;
                    LocalSchedule = null;
                    loadSchedule();
                }
            }
            else
            {
                f.SetValue(this.CurrentSettings, val);
                if (LocalSchedule != null)
                {
                    this.renderSchedule(LocalSchedule);
                }
                else
                {
                    loadSchedule();
                }
            }
        }

        private void OnScheduleChange(Schedule s)
        {
            this.LocalSchedule = s;
            this.renderSchedule(this.LocalSchedule);
        }

        private void OnCalendarQueryChange(CalendarQueryType t)
        {
            switch (t)
            {
                case CalendarQueryType.Static:
                    NoCreditTerms = true;
                    DayMode = false;
                    break;
                case CalendarQueryType.Week:
                    NoCreditTerms = false;
                    DayMode = false;
                    break;
                case CalendarQueryType.Day:
                    NoCreditTerms = false;
                    DayMode = true;
                    break;
            }

            if (this.CurrentSettings.DisplayMultipleSchedules)
            {
                OnScheduleSettingsChange(SelectedDay, DayMode, NoCreditTerms, OnlyUpcoming);
            }
            else
            {
                loadSchedule();
            }
        }

        private void OnCalendarDateChange(DateTime d)
        {
            this.SelectedDay = d;
            if (this.CurrentSettings.DisplayMultipleSchedules)
            {
                OnScheduleSettingsChange(d, DayMode, NoCreditTerms, OnlyUpcoming);
            }
            else
            {
                loadSchedule();
            }
        }

        private async void loadSchedule()
        {
            
            LoadingView lv = new LoadingView();

            var par = App.Current.MainWindow as MainWindow;
            var result = await par.MV.LoadingDialog.ShowDialog(lv, delegate (object s, DialogOpenedEventArgs a)
            {
                try
                {
                    GetSchedule(Settings.LocalUser, (Schedule rs) =>
                    {
                        this.LocalSchedule = rs;
                        renderSchedule(rs);
                    });

                    lv.OnRequestSuccess();
                }
                catch (RequestFailedException e)
                {
                    lv.OnRequestFailed(new Tuple<int, string>(e.Code, e.Message));
                    scheduleFailed();
                }
            });

           
        }

        public void ReloadTermSchedule()
        {
            loadTermSchedule();
        }

        private void loadTermSchedule()
        {
            try
            {
                GetTermSchedule((Schedule rs) =>
                {
                    this.LocalSchedule = rs;
                    renderSchedule(rs);
                });

                //lv.OnRequestSuccess();
            }
            catch (RequestFailedException e)
            {
                //lv.OnRequestFailed(new Tuple<int, string>(e.Code, e.Message));
                scheduleFailed();
            }


            /*LoadingView lv = new LoadingView();


            var result = await this.LoadingDialog.ShowDialog(lv, delegate (object s, DialogOpenedEventArgs a)
            {
                try
                {
                    GetTermSchedule((Schedule rs) =>
                    {
                        this.LocalSchedule = rs;
                        renderSchedule(rs);
                    });

                    lv.OnRequestSuccess();
                }
                catch (RequestFailedException e)
                {
                    lv.OnRequestFailed(new Tuple<int, string>(e.Code, e.Message));
                    scheduleFailed();
                }
            });*/
        }

        private void renderSchedule(Schedule s)
        {
            ContentGrid.Content = null;

            Grid scheduleGrid = new Grid();
            scheduleGrid.VerticalAlignment = VerticalAlignment.Top;
            scheduleGrid.HorizontalAlignment = HorizontalAlignment.Stretch;


            //? Create background with static shit

            Grid bGrid = new Grid();
            bGrid.VerticalAlignment = VerticalAlignment.Top;
            bGrid.HorizontalAlignment = HorizontalAlignment.Stretch;


            int lastRenderHour = 0;
            if (this.CurrentSettings.LastCourseIsLastInSchedule)
            {
                foreach(ScheduleDay sd in s.Courses)
                {
                    if(lastRenderHour < sd.LastDayCourseHour)
                    {
                        lastRenderHour = sd.LastDayCourseHour;
                    }
                }
            }
            else
            {
                lastRenderHour = 15;
            }

            RowDefinition bGridTimeRow = new RowDefinition();
            bGridTimeRow.Height = new GridLength(45);
            bGrid.RowDefinitions.Add(bGridTimeRow);

            for (int i = 0; i < lastRenderHour + 1; i++)
            {
                ColumnDefinition bGridTimeColumn = new ColumnDefinition();
                if (i == 0)
                {
                    bGridTimeColumn.Width = new GridLength(160);
                }
                else
                {
                    bGridTimeColumn.Width = new GridLength(80);
                }
                bGrid.ColumnDefinitions.Add(bGridTimeColumn);



                Grid bGridTimeGrid = new Grid();
                bGridTimeGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
                bGridTimeGrid.VerticalAlignment = VerticalAlignment.Stretch;
                Grid.SetRow(bGridTimeGrid, 0);
                Grid.SetColumn(bGridTimeGrid, i);

                Rectangle rightDivider = new Rectangle();
                rightDivider.VerticalAlignment = VerticalAlignment.Stretch;
                rightDivider.HorizontalAlignment = HorizontalAlignment.Right;
                rightDivider.Width = 2;
                rightDivider.Fill = new SolidColorBrush(Color.FromRgb(242, 242, 242));
                bGridTimeGrid.Children.Add(rightDivider);

                Rectangle bottomDivider = new Rectangle();
                bottomDivider.VerticalAlignment = VerticalAlignment.Bottom;
                bottomDivider.HorizontalAlignment = HorizontalAlignment.Stretch;
                bottomDivider.Height = 1;
                bottomDivider.Fill = new SolidColorBrush(Color.FromRgb(245, 245, 245));
                bGridTimeGrid.Children.Add(bottomDivider);

                TextBlock ScheduleHourText = new TextBlock();
                ScheduleHourText.VerticalAlignment = VerticalAlignment.Center;
                ScheduleHourText.HorizontalAlignment = HorizontalAlignment.Center;
                ScheduleHourText.TextAlignment = TextAlignment.Center;
                ScheduleHourText.FontSize = 15;
                ScheduleHourText.FontWeight = FontWeights.Bold;
                ScheduleHourText.Foreground = Brushes.Black;

                bGridTimeGrid.Children.Add(ScheduleHourText);

                if (i == 0)
                {
                    ScheduleHourText.Text = "Days";
                }
                else
                {
                    ScheduleHourText.Text = (7 + i - 1).ToString();

                    TextBlock ScheduleHourTextAbove = new TextBlock();
                    ScheduleHourTextAbove.Text = ":00";
                    ScheduleHourTextAbove.VerticalAlignment = VerticalAlignment.Center;
                    ScheduleHourTextAbove.HorizontalAlignment = HorizontalAlignment.Center;
                    ScheduleHourTextAbove.TextAlignment = TextAlignment.Center;
                    ScheduleHourTextAbove.FontSize = 9;
                    ScheduleHourTextAbove.Margin = new Thickness(33, 0, 0, 8);

                    bGridTimeGrid.Children.Add(ScheduleHourTextAbove);
                }


                bGrid.Children.Add(bGridTimeGrid);

            }

            int sdc = 1;
            foreach(ScheduleDay sd in s.Courses.ToList())
            {
                RowDefinition bGridCourseDayRow = new RowDefinition();
                bGridCourseDayRow.MinHeight = 45;
                bGridCourseDayRow.Height = new GridLength(sd.MaxCoursesInHour * 45);
                bGrid.RowDefinitions.Add(bGridCourseDayRow);

                for (int i = 0; i < lastRenderHour +1; i++)
                {
                    Grid bGridCourseGrid = new Grid();
                    bGridCourseGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
                    bGridCourseGrid.VerticalAlignment = VerticalAlignment.Stretch;
                    Grid.SetRow(bGridCourseGrid, sdc);
                    Grid.SetColumn(bGridCourseGrid, i);

                    Rectangle rightDivider = new Rectangle();
                    rightDivider.VerticalAlignment = VerticalAlignment.Stretch;
                    rightDivider.HorizontalAlignment = HorizontalAlignment.Right;
                    rightDivider.Width = 2;
                    rightDivider.Fill = new SolidColorBrush(Color.FromRgb(242, 242, 242));
                    bGridCourseGrid.Children.Add(rightDivider);

                    Rectangle bottomDivider = new Rectangle();
                    bottomDivider.VerticalAlignment = VerticalAlignment.Bottom;
                    bottomDivider.HorizontalAlignment = HorizontalAlignment.Stretch;
                    bottomDivider.Height = 1;
                    bottomDivider.Fill = new SolidColorBrush(Color.FromRgb(245, 245, 245));
                    bGridCourseGrid.Children.Add(bottomDivider);

                    bGrid.Children.Add(bGridCourseGrid);

                }
                sdc++;
            }

            scheduleGrid.Children.Add(bGrid);

            //? Render timeTable


            Grid tGrid = new Grid();
            tGrid.VerticalAlignment = VerticalAlignment.Top;
            tGrid.HorizontalAlignment = HorizontalAlignment.Stretch;

            RowDefinition tGridTimeRow = new RowDefinition();
            tGridTimeRow.Height = new GridLength(45);
            tGrid.RowDefinitions.Add(tGridTimeRow);

            for (int i = 0; i < lastRenderHour + 1; i++)
            {
                ColumnDefinition tGridTimeColumn = new ColumnDefinition();
                if (i == 0)
                {
                    tGridTimeColumn.Width = new GridLength(160);
                }
                else
                {
                    tGridTimeColumn.Width = new GridLength(80);
                }
                tGrid.ColumnDefinitions.Add(tGridTimeColumn);
            }

            int cr = 1;

            int cidn = 0;
            foreach(ScheduleDay sd in s.Courses.ToList())
            {
                

                RowDefinition tGridCourseDayRow = new RowDefinition();
                tGridCourseDayRow.MinHeight = 45;
                tGridCourseDayRow.Height = new GridLength(sd.MaxCoursesInHour * 45);
                tGrid.RowDefinitions.Add(tGridCourseDayRow);

                for (int i = 0; i < lastRenderHour +1; i++)
                {
                    Grid tGridCourseGrid = new Grid();
                    tGridCourseGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
                    tGridCourseGrid.VerticalAlignment = VerticalAlignment.Stretch;
                    
                    Grid.SetRow(tGridCourseGrid, cr);
                    Grid.SetColumn(tGridCourseGrid, i);

                    bool isStatic = false;

                    int ii = (i > 0 ? i - 1 : 0);

                    foreach(List<Course> cList in sd.Courses)
                    {
                        if(cList.Count != 0)
                        {
                            foreach(Course c in cList)
                            {
                                if(c == null) { continue; }
                                if (c.IsStatic)
                                {
                                    isStatic = true;
                                    break;
                                }
                            }
                        }
                    }

                    if(i == 0)
                    {
                        TextBlock DayText = new TextBlock();
                        if (isStatic)
                        {
                            DayText.Text = sd.ScheduleDate.DayOfWeek.ToString();
                        }
                        else
                        {
                            DayText.Text = sd.ScheduleDate.DayOfWeek.ToString() + "\n " + sd.ScheduleDate.ToString("d.M.yyyy");
                        }
                        DayText.VerticalAlignment = VerticalAlignment.Center;
                        DayText.HorizontalAlignment = HorizontalAlignment.Center;
                        DayText.TextAlignment = TextAlignment.Center;
                        DayText.FontSize = 14;
                        DayText.Foreground = Brushes.Black;
                        tGridCourseGrid.Children.Add(DayText);
                    }
                    else
                    {
                        List<Course> tcList = sd.Courses[ii];
                        foreach(Course course in tcList)
                        {
                            if(course == null)
                            {
                                RowDefinition cdgr = new RowDefinition();
                                cdgr.Height = new GridLength(45);
                                tGridCourseGrid.RowDefinitions.Add(cdgr);
                                continue;
                            }

                            RowDefinition courseDayGridRow = new RowDefinition();
                            if(course.IsEmpty && tcList.Count == 1)
                            {
                                courseDayGridRow.Height = new GridLength(45 * sd.MaxCoursesInHour);
                            }
                            else
                            {
                                courseDayGridRow.Height = new GridLength(45);
                            }
                            
                            tGridCourseGrid.RowDefinitions.Add(courseDayGridRow);

                            int cx = Array.IndexOf(tcList.ToArray(), course);

                            Border CourseBorder = new Border();
                            CourseBorder.HorizontalAlignment = HorizontalAlignment.Left;
                            CourseBorder.BorderBrush = new SolidColorBrush(Color.FromRgb(215, 215, 215));
                            CourseBorder.BorderThickness = new Thickness(1);
                            CourseBorder.MinHeight = 45;
                            CourseBorder.Width = course.Duration * 80;
                            CourseBorder.ClipToBounds = false;
                            Grid.SetRow(CourseBorder, cx);


                            Grid CourseGrid = new Grid();
                            CourseGrid.VerticalAlignment = VerticalAlignment.Stretch;
                            CourseGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
                            CourseGrid.Width = course.Duration * 80;
                            CourseGrid.ClipToBounds = false;
                            CourseGrid.MinHeight = 45;

                            Rectangle CourseTypeRect = new Rectangle();
                            CourseTypeRect.VerticalAlignment = VerticalAlignment.Stretch;
                            CourseTypeRect.HorizontalAlignment = HorizontalAlignment.Left;
                            CourseTypeRect.Margin = new Thickness(4, 5, 0, 6);
                            CourseTypeRect.Width = 4;

                            switch (course.CourseType)
                            {
                                case CourseType.Pr:
                                    CourseTypeRect.Fill = new SolidColorBrush(Color.FromRgb(227, 227, 227));
                                    break;
                                case CourseType.Cv:
                                    CourseTypeRect.Fill = new SolidColorBrush(Color.FromRgb(158, 229, 193));
                                    break;
                                case CourseType.Se:
                                    CourseTypeRect.Fill = new SolidColorBrush(Color.FromRgb(164, 239, 151));
                                    break;
                                case CourseType.Za:
                                    CourseTypeRect.Fill = new SolidColorBrush(Color.FromRgb(238, 239, 151));
                                    break;
                                case CourseType.Zk:
                                    CourseTypeRect.Fill = new SolidColorBrush(Color.FromRgb(239, 180, 91));
                                    break;
                                case CourseType.Empty:
                                    CourseTypeRect.Fill = new SolidColorBrush(Color.FromRgb(255, 94, 94));
                                    break;
                            }

                            CourseGrid.Children.Add(CourseTypeRect);

                            if (course.IsTerm)
                            {
                                Rectangle CourseTermTaken = new Rectangle();
                                CourseTermTaken.VerticalAlignment = VerticalAlignment.Stretch;
                                CourseTermTaken.HorizontalAlignment = HorizontalAlignment.Left;
                                CourseTermTaken.Margin = new Thickness(10, 5, 0, 6);
                                CourseTermTaken.Width = 4;
                                CourseGrid.Children.Add(CourseTermTaken);

                                if (course.CourseTerm.TermRegistered)
                                {
                                    CourseTermTaken.Fill = Brushes.LightGreen;
                                }
                                else
                                {
                                    CourseTermTaken.Fill = Brushes.Red;
                                }
                            }


                            TextBlock CourseText = new TextBlock();
                            if (course.IsEmpty)
                            {
                                CourseText.Text = "Free";
                            }
                            else
                            {
                                CourseText.Text = course.CourseDepartment + "/" + course.CourseId;
                            }

                            CourseText.VerticalAlignment = VerticalAlignment.Top;
                            CourseText.HorizontalAlignment = HorizontalAlignment.Left;
                            CourseText.Margin = new Thickness(25, 4, 0, 0);
                            CourseText.TextAlignment = TextAlignment.Left;
                            CourseText.FontSize = 12;
                            CourseText.Foreground = Brushes.White;
                            CourseText.TextWrapping = TextWrapping.WrapWithOverflow;
                            CourseGrid.Children.Add(CourseText);

                            if (course.Owner != null)
                            {
                                //CourseGrid.Background = new SolidColorBrush(course.Owner.ToColor());
                                
                                if (s.UserCount == 1)
                                {
                                    CourseGrid.Background = Settings.ApplicationColorSchema.MainColor;
                                }
                                else
                                {
                                    CourseGrid.Background = new SolidColorBrush(course.Owner.ToColor());
                                }
                                
                            }
                            else
                            {
                                CourseGrid.Background = new SolidColorBrush(Color.FromRgb(95, 151, 251));
                            }

                            Button cbtn = new Button();
                            cbtn.BorderBrush = Brushes.Transparent;
                            cbtn.Background = Brushes.Transparent;
                            cbtn.VerticalAlignment = VerticalAlignment.Stretch;
                            cbtn.HorizontalAlignment = HorizontalAlignment.Stretch;
                            cbtn.Style = (Style)FindResource("MaterialDesignRaisedButton");
                            cbtn.MinHeight = 45;
                            cbtn.Height = (45 * sd.MaxCoursesInHour);
                            cbtn.DataContext = cidn;

                            CourseTooltipComponent ctc = null;

                            if (course.IsTerm)
                            {
                                ctc = new CourseTooltipComponent(course, PopupTooltip, cidn, sd, this);
                            }
                            else
                            {
                                ctc = new CourseTooltipComponent(course, PopupTooltip, cidn, sd);
                            }

                            cbtn.Click += (obj, arg) =>
                            {
                                if (PopupTooltip.Child != null)
                                {
                                    if(((CourseTooltipComponent)PopupTooltip.Child).Id == (int)cbtn.DataContext)
                                    {
                                        PopupTooltip.IsOpen = false;
                                        PopupTooltip.Child = null;
                                    }
                                    else
                                    {
                                        PopupTooltip.IsOpen = false;
                                        PopupTooltip.Child = null;

                                        ctc.OnOpen();

                                        PopupTooltip.Child = ctc;
                                        PopupTooltip.PlacementTarget = CourseGrid;
                                        PopupTooltip.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
                                        PopupTooltip.IsOpen = true;
                                    }
                                }
                                else
                                {
                                    ctc.OnOpen();

                                    PopupTooltip.Child = ctc;
                                    PopupTooltip.PlacementTarget = CourseGrid;
                                    PopupTooltip.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
                                    PopupTooltip.IsOpen = true;
                                }
                            };

                            CourseGrid.Children.Add(cbtn);
                            CourseBorder.Child = CourseGrid;

                            Grid.SetColumnSpan(tGridCourseGrid, course.Duration);

                            if (!course.IsEmpty)
                            {
                                tGridCourseGrid.Children.Add(CourseBorder);
                            }
                            else
                            {
                                if (this.CurrentSettings.DisplayFreeHours)
                                {
                                    tGridCourseGrid.Children.Add(CourseBorder);
                                }
                            }

                            cidn++;
                        }

                    }


                    tGrid.Children.Add(tGridCourseGrid);
                }

                cr++;
            }

            scheduleGrid.Children.Add(tGrid);

            //? Set content

            ContentGrid.Content = scheduleGrid;

        }

        private void scheduleFailed()
        {
            this.Background = Brushes.Red;
        }

        private async void GetSchedule(StagUser stU, Action<Schedule> ret)
        {
            DateTime df = this.SelectedDay;
            DateTime dt = ((DayMode == true) ? df : df.AddDays(7));


            if (stU.stagUserRole == StagUserRole.Student)
            {
                Schedule lo = await Rest.CreateStudentGetScheduleRequest(stU, 
                    new KeyValuePair<string, string>("Datefrom",df.ToString("d.M.yyyy")),
                    new KeyValuePair<string, string>("Dateto", dt.ToString("d.M.yyyy")),
                    new KeyValuePair<string, string>("Nocreditterms", NoCreditTerms.ToString().ToLower()),
                    new KeyValuePair<string, string>("Onlyupcoming", OnlyUpcoming.ToString().ToLower())
                );
                ret(lo);
                
            }
            else if (stU.stagUserRole == StagUserRole.Teacher)
            {
                Schedule lo = await Rest.CreateTeacherGetScheduleRequest(stU,
                    new KeyValuePair<string, string>("Datefrom", df.ToString("d.M.yyyy")),
                    new KeyValuePair<string, string>("Dateto", dt.ToString("d.M.yyyy")),
                    new KeyValuePair<string, string>("Nocreditterms", NoCreditTerms.ToString().ToLower()),
                    new KeyValuePair<string, string>("Onlyupcoming", OnlyUpcoming.ToString().ToLower())
                );
                ret(lo);
            }

        }

        private async void GetTermSchedule(Action<Schedule> ret)
        {
            Schedule lo = await Rest.CreateStudentGetTermsRequest();
            ret(lo);
        }

    }

    public class ScheduleSettingsArgs : EventArgs
    {
        public DateTime SelectedDate { get; private set; }
        public bool DayMode { get; private set; }
        public bool NoCreditTerms { get; private set; }
        public bool OnlyUpcoming { get; private set; }

        public ScheduleSettingsArgs(DateTime sd, bool dm, bool nct, bool ou)
        {
            this.SelectedDate = sd;
            this.DayMode = dm;
            this.NoCreditTerms = nct;
            this.OnlyUpcoming = ou;
        }
    }

    public enum ScheduleType
    {
        OnlySchedule,
        OnlyUpcoming
    }
}
