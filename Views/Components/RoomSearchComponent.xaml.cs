﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Ultimate_Schedule_Builder.Models;

namespace Ultimate_Schedule_Builder.Views.Components
{
    /// <summary>
    /// Interaction logic for RoomSearchComponent.xaml
    /// </summary>
    public partial class RoomSearchComponent : UserControl
    {
        private StagRoom[] selectedRooms = new StagRoom[] { };
        private int selectedPosition;
        private string selectedDate;
        //private List<StagBuildingList> selectedBuildings = new List<StagBuildingList>();
        private Dictionary<StagBuildingList, bool> buildingList = new Dictionary<StagBuildingList, bool>();

        private StagBuildingList[] selectedBuildings
        {
            get
            {
                List<StagBuildingList> sList = new List<StagBuildingList>();
                foreach(KeyValuePair<StagBuildingList,bool> sbl in buildingList)
                {
                    if(sbl.Value == true)
                    {
                        sList.Add(sbl.Key);
                    }
                }
                return sList.ToArray();
            }
        }

        public RoomSearchComponent(int position,string dateString)
        {
            InitializeComponent();
            DataContext = this;

            RoomHeaderText.Text = $"Empty rooms for: {dateString} {position + 7}:00";
            this.selectedPosition = position;
            this.selectedDate = dateString;

            this.setUp();
            this.renderButtons();
        }

        public RoomSearchComponent(int position,string dateString, StagBuildingList[] buildings)
        {
            InitializeComponent();
            DataContext = this;

            RoomHeaderText.Text = $"Empty rooms for: {dateString} {position + 7}:00";
            this.selectedPosition = position;
            this.selectedDate = dateString;

            this.setUp();
            this.setBuildings(buildings);
            this.renderButtons();
            //OnSearch(position, dateString, buildings);
        }

        private void setUp()
        {
            foreach(StagBuildingList t in Enum.GetValues(typeof(StagBuildingList)))
            {
                this.buildingList.Add(t, false);
            }
        }

        private void setBuildings(StagBuildingList[] tList)
        {
            foreach(StagBuildingList to in tList)
            {
                foreach(KeyValuePair<StagBuildingList,bool> ta in this.buildingList.ToList())
                {
                    if(to == ta.Key)
                    {
                        this.buildingList[to] = true;
                    }
                }
            }
        }

        private void setExpanderHeader()
        {
            string buildingString = "";
            foreach (StagBuildingList t in this.selectedBuildings)
            {
                buildingString += t.ToString() + ", ";
            }
            buildingString = buildingString.Remove(buildingString.Length - 2);

            BuildingExpander.Header = buildingString;

        }

        private void renderButtons()
        {
            ExpanderContent.Children.Clear();

            setExpanderHeader();

            foreach (KeyValuePair<StagBuildingList, bool> ta in this.buildingList.ToList())
            {
                CheckBox chb = new CheckBox();
                chb.Width = 50;
                chb.Height = 40;
                chb.Content = ta.Key.ToString();
                chb.DataContext = ta.Key;
                chb.IsChecked = ta.Value;

                chb.Checked += (object o, RoutedEventArgs e) =>
                {
                    buildingButtonClicked(ta.Key, true);
                };

                chb.Unchecked += (object o, RoutedEventArgs e) =>
                {
                    buildingButtonClicked(ta.Key, false);
                };

                ExpanderContent.Children.Add(chb);

            }
        }

        private void buildingButtonClicked(StagBuildingList b, bool s)
        {
            foreach (KeyValuePair<StagBuildingList, bool> ta in this.buildingList.ToList())
            {
                if (b == ta.Key)
                {
                    this.buildingList[b] = s;
                    break;
                }
            }
            setExpanderHeader();
        }

        private async void OnSearch(int position, string dateString, StagBuildingList[] buildings)
        {
            List<string> rList = new List<string>();
            foreach (StagBuildingList s in buildings)
            {
                rList.Add(s.ToString());
            }

            LoadingView lv = new LoadingView();

            var sr = await this.LoadingDialog.ShowDialog(lv, delegate (object s, DialogOpenedEventArgs a)
            {
                GetRoomList(lv, position, dateString, rList.ToArray());
            },(object d, DialogClosingEventArgs e) =>
            {
                RenderRooms(selectedRooms);
            });


        }

        private async void GetRoomList(LoadingView lv,int position, string dateString, string[] buildings)
        {
            try
            {
                StagRoom[] rArr = await Rest.CreateRoomGetFreeRoomsInBuildingRequest(position, dateString, buildings);
                selectedRooms = rArr;
                lv.OnRequestSuccess();

            }
            catch (RequestFailedException e)
            {
                lv.OnRequestFailed(new Tuple<int, string>(e.Code, e.Message));
            }

        }

        private void RenderRooms(StagRoom[] rArr)
        {
            ContentGrid.Children.Clear();

            foreach (StagRoom st in rArr)
            {
                
                if (!st.RoomFree && !Settings.SearchRoomsShowOccupied) { continue; }

                Border bgBorder = new Border();
                bgBorder.Width = 90;
                bgBorder.Height = 90;
                bgBorder.Background = new SolidColorBrush(Colors.WhiteSmoke);
                bgBorder.BorderBrush = new SolidColorBrush(Colors.LightGray);
                bgBorder.BorderThickness = new Thickness(1);

                Grid rGrid = new Grid();
                bgBorder.Child = rGrid;
                rGrid.Margin = new Thickness(5);

                rGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(20) });
                rGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1,GridUnitType.Star) });

                rGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
                rGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
                rGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
                rGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });

                Grid occupacyGrid = new Grid();
                rGrid.Children.Add(occupacyGrid);
                Grid.SetRow(occupacyGrid, 0);
                Grid.SetColumn(occupacyGrid, 0);
                Grid.SetRowSpan(occupacyGrid, 4);

                Rectangle occupacyRectangle = new Rectangle();
                occupacyGrid.Children.Add(occupacyRectangle);
                occupacyRectangle.HorizontalAlignment = HorizontalAlignment.Center;
                occupacyRectangle.VerticalAlignment = VerticalAlignment.Stretch;
                occupacyRectangle.Width = 5;
                occupacyRectangle.Margin = new Thickness(3);
                if (st.RoomFree)
                {
                    occupacyRectangle.Fill = new SolidColorBrush(Colors.Green);
                }
                else
                {
                    occupacyRectangle.Fill = new SolidColorBrush(Colors.Red);
                }

                // Faculty

                StackPanel facultyPanel = new StackPanel();
                rGrid.Children.Add(facultyPanel);
                facultyPanel.Orientation = Orientation.Horizontal;
                Grid.SetRow(facultyPanel, 0);
                Grid.SetColumn(facultyPanel, 1);

                PackIcon facultyIcon = new PackIcon();
                facultyPanel.Children.Add(facultyIcon);
                facultyIcon.Height = 20;
                facultyIcon.Margin = new Thickness(0, 0, 10, 0);
                facultyIcon.HorizontalAlignment = HorizontalAlignment.Left;
                facultyIcon.VerticalAlignment = VerticalAlignment.Center;
                facultyIcon.Kind = PackIconKind.Briefcase;

                TextBlock facultyText = new TextBlock();
                facultyPanel.Children.Add(facultyText);
                facultyText.HorizontalAlignment = HorizontalAlignment.Center;
                facultyText.VerticalAlignment = VerticalAlignment.Center;
                facultyText.Text = st.RoomFaculty.ToString();

                // Building

                StackPanel buildingPanel = new StackPanel();
                rGrid.Children.Add(buildingPanel);
                buildingPanel.ToolTip = st.RoomBuildingAdress;
                buildingPanel.Orientation = Orientation.Horizontal;
                Grid.SetRow(buildingPanel, 1);
                Grid.SetColumn(buildingPanel, 1);

                PackIcon buildingIcon = new PackIcon();
                buildingPanel.Children.Add(buildingIcon);
                buildingIcon.Height = 20;
                buildingIcon.Margin = new Thickness(0, 0, 10, 0);
                buildingIcon.HorizontalAlignment = HorizontalAlignment.Left;
                buildingIcon.VerticalAlignment = VerticalAlignment.Center;
                buildingIcon.Kind = PackIconKind.HospitalBuilding;

                TextBlock buildingText = new TextBlock();
                buildingPanel.Children.Add(buildingText);
                buildingText.HorizontalAlignment = HorizontalAlignment.Center;
                buildingText.VerticalAlignment = VerticalAlignment.Center;
                buildingText.Text = st.RoomBuilding.ToString();

                // Room number

                StackPanel roomPanel = new StackPanel();
                rGrid.Children.Add(roomPanel);
                roomPanel.Orientation = Orientation.Horizontal;
                Grid.SetRow(roomPanel, 2);
                Grid.SetColumn(roomPanel, 1);

                PackIcon roomIcon = new PackIcon();
                roomPanel.Children.Add(roomIcon);
                roomIcon.Height = 20;
                roomIcon.Margin = new Thickness(0, 0, 10, 0);
                roomIcon.HorizontalAlignment = HorizontalAlignment.Left;
                roomIcon.VerticalAlignment = VerticalAlignment.Center;
                roomIcon.Kind = PackIconKind.Door;

                TextBlock roomText = new TextBlock();
                roomPanel.Children.Add(roomText);
                roomText.HorizontalAlignment = HorizontalAlignment.Center;
                roomText.VerticalAlignment = VerticalAlignment.Center;
                roomText.Text = st.RoomNumber.ToString();

                // Capacity

                StackPanel capacityPanel = new StackPanel();
                rGrid.Children.Add(capacityPanel);
                capacityPanel.Orientation = Orientation.Horizontal;
                Grid.SetRow(capacityPanel, 3);
                Grid.SetColumn(capacityPanel, 1);

                PackIcon capacityIcon = new PackIcon();
                capacityPanel.Children.Add(capacityIcon);
                capacityIcon.Height = 20;
                capacityIcon.Margin = new Thickness(0, 0, 10, 0);
                capacityIcon.HorizontalAlignment = HorizontalAlignment.Left;
                capacityIcon.VerticalAlignment = VerticalAlignment.Center;
                capacityIcon.Kind = PackIconKind.AccountMultiple;

                TextBlock capacityText = new TextBlock();
                capacityPanel.Children.Add(capacityText);
                capacityText.HorizontalAlignment = HorizontalAlignment.Center;
                capacityText.VerticalAlignment = VerticalAlignment.Center;
                capacityText.Text = st.RoomCapacity.ToString();


                ContentGrid.Children.Add(bgBorder);
            }
        }

        private void ButtonAccept_Click(object sender, RoutedEventArgs e) //? Do something
        {
            if (this.CloseDialog.Command.CanExecute(selectedRooms))
            {
                this.CloseDialog.Command.Execute(selectedRooms);
            }
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            if (this.CloseDialog.Command.CanExecute(new StagRoom[] { }))
            {
                this.CloseDialog.Command.Execute(new StagRoom[] { });
            }
        }

        private void RoomSearchButton_Click(object sender, RoutedEventArgs e)
        {
            if(this.selectedBuildings.Length != 0)
            {
                this.OnSearch(this.selectedPosition,this.selectedDate,this.selectedBuildings);
            }
        }
    }
}
