﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Ultimate_Schedule_Builder.Models;

namespace Ultimate_Schedule_Builder.Views.Components
{
    public partial class SelectUsersComponent : UserControl
    {
        private QueryType selectedUserQueryType = QueryType.Both;

        private List<dynamic> selectedUsers = new List<dynamic>();
        
        public SelectUsersComponent()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void UserSearchButton_Click(object sender, RoutedEventArgs e)
        {
            if (CanSearch())
            {
                clearUserList();

                LoadingView lv = new LoadingView();
                string unt = UserNameInput.Text;
                string[] splitName = unt.Split(' ');
                string fn, ln;
                if (splitName.Length < 2)
                {
                    fn = splitName[0];
                    ln = "";
                }
                else
                {
                    fn = splitName[0];
                    ln = splitName[1];
                }

                if (!String.IsNullOrEmpty(fn))
                {
                    if (!String.IsNullOrEmpty(ln))
                    {
                        this.GetUsersRequest(lv, this.selectedUserQueryType, fn, ln);
                    }
                }
            }
        }

        private async void GetUsersRequest(LoadingView lv, QueryType qt, string fn, string ln)
        {
            switch (qt)
            {
                case QueryType.Student:
                    var sr = await LoadingDialog.ShowDialog(lv, delegate (object s, DialogOpenedEventArgs a)
                    {
                        GetStudentsRequest(lv, fn, ln);
                    });
                    break;
                case QueryType.Teacher:
                    var tr = await LoadingDialog.ShowDialog(lv, delegate (object s, DialogOpenedEventArgs a)
                    {
                        GetTeacherRequest(lv, fn, ln);
                    });
                    break;
                case QueryType.Both:
                    var br = await LoadingDialog.ShowDialog(lv, delegate (object s, DialogOpenedEventArgs a)
                    {
                        GetBothRequest(lv, fn, ln);
                    });
                    break;
            }
        }

        private async void GetStudentsRequest(LoadingView lv, string n, string s)
        {
            try
            {
                StagStudent[] res = await Rest.CreateStudentGetByNameRequest(n, s);

                List<dynamic> tList = new List<dynamic>();

                foreach(StagStudent st in res.ToList())
                {
                    bool clone = false;
                    foreach(dynamic stD in this.selectedUsers.ToList())
                    {
                        if (((StagUser)stD).stagId == Settings.LocalUser.stagId)
                        {
                            clone = true;
                            break;
                        }
                        else if (((StagUser)stD).stagId == st.stagId)
                        {
                            clone = true;
                            break;
                        }
                    }

                    if (!clone)
                    {
                        tList.Add(st);
                    }
                }

                tList.Sort((x, y) => String.Compare(((StagUser)x).stagUserLastName, ((StagUser)y).stagUserLastName));

                this.renderUsers(tList);
                lv.OnRequestSuccess();
            }
            catch (RequestFailedException e)
            {
                lv.OnRequestFailed(new Tuple<int, string>(e.Code, e.Message));
            }
        }

        private async void GetTeacherRequest(LoadingView lv, string n, string s)
        {
            try
            {
                StagTeacher[] res = await Rest.CreateTeacherGetByNameRequest(n, s);

                List<dynamic> tList = new List<dynamic>();

                foreach (StagTeacher st in res.ToList())
                {
                    bool clone = false;
                    foreach (dynamic stD in this.selectedUsers.ToList())
                    {
                        if(((StagUser)stD).stagId == Settings.LocalUser.stagId)
                        {
                            clone = true;
                            break;
                        }
                        else if (((StagUser)stD).stagId == st.stagId )
                        {
                            clone = true;
                            break;
                        }
                    }

                    if (!clone)
                    {
                        tList.Add(st);
                    }
                }

                tList.Sort((x, y) => String.Compare(((StagUser)x).stagUserLastName, ((StagUser)y).stagUserLastName));

                this.renderUsers(tList);
                lv.OnRequestSuccess();
            }
            catch (RequestFailedException e)
            {
                lv.OnRequestFailed(new Tuple<int, string>(e.Code, e.Message));
            }
        }

        private async void GetBothRequest(LoadingView lv, string n, string s)
        {
            int failCount = 0;
            int LastCode = 0;
            List<dynamic> tList = new List<dynamic>();

            try
            {
                StagStudent[] res = await Rest.CreateStudentGetByNameRequest(n, s);
                foreach (StagStudent st in res.ToList())
                {
                    bool clone = false;
                    foreach (dynamic stD in this.selectedUsers.ToList())
                    {
                        if (((StagUser)stD).stagId == Settings.LocalUser.stagId)
                        {
                            clone = true;
                            break;
                        }
                        else if (((StagUser)stD).stagId == st.stagId)
                        {
                            clone = true;
                            break;
                        }
                    }

                    if (!clone)
                    {
                        tList.Add(st);
                    }
                }
            }
            catch (RequestFailedException e)
            {
                failCount++;
                LastCode = e.Code;
            }

            try
            {
                StagTeacher[] res = await Rest.CreateTeacherGetByNameRequest(n, s);

                foreach (StagTeacher st in res.ToList())
                {
                    bool clone = false;
                    foreach (dynamic stD in this.selectedUsers.ToList())
                    {
                        if (((StagUser)stD).stagId == Settings.LocalUser.stagId)
                        {
                            clone = true;
                            break;
                        }
                        else if (((StagUser)stD).stagId == st.stagId)
                        {
                            clone = true;
                            break;
                        }
                    }

                    if (!clone)
                    {
                        tList.Add(st);
                    }
                }

            }
            catch (RequestFailedException e)
            {
                failCount++;
                LastCode = e.Code;
            }

            if(failCount < 2)
            {
                tList.Sort((x, y) => String.Compare(((StagUser)x).stagUserLastName, ((StagUser)y).stagUserLastName));
                this.renderUsers(tList);
                lv.OnRequestSuccess();
            }
            else
            {
                lv.OnRequestFailed(new Tuple<int, string>(LastCode, "Failed to get data"));
            }

        }

        private void clearUserList()
        {
            if(this.selectedUsers.Count > 0)
            {
                this.UserResultPanel.Children.Clear();
                this.renderUsers(this.selectedUsers, true);
            }
            else
            {
                this.UserResultPanel.Children.Clear();
            }
        }
    
        private void renderUsers(List<dynamic> uList, bool isSelected = false)
        {
            foreach(dynamic u in uList.ToList())
            {
                StagUser ul = (StagUser)u;

                Expander uExpander = new Expander();
                uExpander.Background = Brushes.White;

                Grid uehtg = new Grid();
                uExpander.Header = uehtg;

                StackPanel uGridContent = new StackPanel();
                uGridContent.HorizontalAlignment = HorizontalAlignment.Left;
                uGridContent.Orientation = Orientation.Horizontal;
                uehtg.Children.Add(uGridContent);

                PackIcon userIcon = new PackIcon();
                userIcon.Kind = PackIconKind.AccountCircle;
                userIcon.VerticalAlignment = VerticalAlignment.Center;
                userIcon.Width = 30;
                userIcon.Height = 30;
                userIcon.Margin = new Thickness(0, 0, 10, 0);
                userIcon.Foreground = Brushes.Black;
                if (u.stagUserRole == StagUserRole.Student)
                {
                    userIcon.Foreground = new SolidColorBrush(Color.FromRgb(95, 151, 251));
                }
                else if (u.stagUserRole == StagUserRole.Teacher)
                {
                    userIcon.Foreground = new SolidColorBrush(Color.FromRgb(250, 190, 88));
                }
                uGridContent.Children.Add(userIcon);

                TextBlock userText = new TextBlock();
                userText.VerticalAlignment = VerticalAlignment.Center;
                userText.FontSize = 18;
                userText.Foreground = Brushes.Black;
                userText.Margin = new Thickness(5, 0, 0, 0);
                userText.Text = $"{u.stagUserFirstName} {u.stagUserLastName}";
                uGridContent.Children.Add(userText);

                ToggleButton tb = new ToggleButton();
                tb.HorizontalAlignment = HorizontalAlignment.Right;
                tb.IsChecked = isSelected;
                tb.Width = 50;

                tb.Checked += (o, a) =>
                {
                    selectUser(u);
                };

                tb.Unchecked += (o, a) =>
                {
                    selectUser(u, true);
                };

                uehtg.Children.Add(tb);

                Grid eContent = new Grid();
                eContent.Margin = new Thickness(25, 0, 25, 0);
                uExpander.Content = eContent;

                StagUser stU = (StagUser)u;

                if(stU.stagUserRole == StagUserRole.Student)
                {
                    StagStudent stSt = (StagStudent)u;

                    for (int i = 0; i < 6; i++)
                    {
                        RowDefinition nrd = new RowDefinition();
                        nrd.Height = new GridLength(25);
                        eContent.RowDefinitions.Add(nrd);

                        StackPanel cData = new StackPanel();
                        cData.Orientation = Orientation.Horizontal;
                        Grid.SetRow(cData, i);
                        eContent.Children.Add(cData);

                        PackIcon cdI = new PackIcon();
                        cdI.Height = 25;
                        cdI.Width = 25;
                        cdI.Foreground = Brushes.Black;
                        cData.Children.Add(cdI);

                        TextBlock cdT = new TextBlock();
                        cdT.Margin = new Thickness(25, 0, 0, 0);
                        cdT.VerticalAlignment = VerticalAlignment.Center;
                        cdT.Foreground = Brushes.Black;
                        cData.Children.Add(cdT);

                        switch (i)
                        {
                            case 0:
                                cData.ToolTip = "StagUserName";
                                cdI.Kind = PackIconKind.AccountKey;
                                cdT.Text = stSt.stagUserName;
                                break;
                            case 1:
                                cData.ToolTip = "StagUserOsCislo";
                                cdI.Kind = PackIconKind.AccountSearch;
                                cdT.Text = stSt.stagId;
                                break;
                            case 2:
                                cData.ToolTip = "StagName";
                                cdI.Kind = PackIconKind.AccountCardDetails;
                                cdT.Text = $"{stSt.stagUserFirstName} {stSt.stagUserLastName}";
                                break;
                            case 3:
                                cData.ToolTip = "StagUserFaculty";
                                cdI.Kind = PackIconKind.Bank;
                                cdT.Text = stSt.stagUserFaculty.ToString();
                                break;
                            case 4:
                                cData.ToolTip = "StagUserField";
                                cdI.Kind = PackIconKind.Briefcase;
                                cdT.Text = stSt.stagUserField;
                                break;
                            case 5:
                                cData.ToolTip = "StagUserCurrentGrade";
                                cdI.Kind = PackIconKind.FormatListNumbers;
                                cdT.Text = stSt.stagUserCurrentGrade.ToString();
                                break;
                        }
                    }
                }
                else if(stU.stagUserRole == StagUserRole.Teacher)
                {
                    StagTeacher stSt = (StagTeacher)u;

                    for (int i = 0; i < 2; i++)
                    {
                        RowDefinition nrd = new RowDefinition();
                        nrd.Height = new GridLength(25);
                        eContent.RowDefinitions.Add(nrd);

                        StackPanel cData = new StackPanel();
                        cData.Orientation = Orientation.Horizontal;
                        Grid.SetRow(cData, i);
                        eContent.Children.Add(cData);

                        PackIcon cdI = new PackIcon();
                        cdI.Height = 25;
                        cdI.Width = 25;
                        cdI.Foreground = Brushes.Black;
                        cData.Children.Add(cdI);

                        TextBlock cdT = new TextBlock();
                        cdT.Margin = new Thickness(25, 0, 0, 0);
                        cdT.VerticalAlignment = VerticalAlignment.Center;
                        cdT.Foreground = Brushes.Black;
                        cData.Children.Add(cdT);

                        switch (i)
                        {
                            case 0:
                                cData.ToolTip = "stagUserTeacherId";
                                cdI.Kind = PackIconKind.AccountSearch;
                                cdT.Text = stSt.stagId;
                                break;
                            case 1:
                                cData.ToolTip = "StagName";
                                cdI.Kind = PackIconKind.AccountCardDetails;
                                cdT.Text = $"{stSt.stagUserFirstName} {stSt.stagUserLastName}";
                                break;
                        }
                    }
                }



                this.UserResultPanel.Children.Add(uExpander);
            }
        }

        private void selectUser(dynamic s, bool remove = false)
        {
            if (!remove)
            {
                bool Added = false;
                foreach(dynamic d in this.selectedUsers.ToList())
                {
                    if(((StagUser)d).stagId == (((StagUser)s).stagId))
                    {
                        Added = true;
                        break;
                    }
                }
                if (!Added)
                {
                    this.selectedUsers.Add(s);
                }
            }
            else
            {
                foreach (dynamic d in this.selectedUsers.ToList())
                {
                    if (((StagUser)d).stagId == (((StagUser)s).stagId))
                    {
                        this.selectedUsers.Remove(d);
                        break;
                    }
                }
            }
        }

        private bool CanSearch()
        {
            if (!String.IsNullOrWhiteSpace(UserNameInput.Text))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void UserQueryChanged(object sender, RoutedEventArgs e)
        {
            RadioButton b = (RadioButton)sender;
            if(Enum.TryParse<QueryType>((string)b.Content,out QueryType t))
            {
                selectedUserQueryType = t;
            }
        }

        private void ButtonAccept_Click(object sender, RoutedEventArgs e)
        {
            if(!(this.selectedUsers.Count > 0)) { return; }
            List<dynamic> ret = this.selectedUsers.ToList();

            if (this.CloseDialog.Command.CanExecute(ret))
            {
                this.CloseDialog.Command.Execute(ret);
            }
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            List<dynamic> ret = new List<dynamic>();

            if (this.CloseDialog.Command.CanExecute(ret))
            {
                this.CloseDialog.Command.Execute(ret);
            }
        }
    }
}
