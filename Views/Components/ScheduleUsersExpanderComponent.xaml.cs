﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Ultimate_Schedule_Builder.Models;

namespace Ultimate_Schedule_Builder.Views.Components
{
    /// <summary>
    /// Interaction logic for ScheduleUsersExpanderComponent.xaml
    /// </summary>
    public partial class ScheduleUsersExpanderComponent : UserControl
    {
        public bool Enabled { get; private set; } = false;

        private ScheduleComponent scheduleComponent = null;
        private DialogHost scheduleDialogHost = null;
        public List<Tuple<bool, StagUser>> SelectedUsers { get; private set; } = new List<Tuple<bool, StagUser>>();

        private DateTime SelectedDay = DateTime.Today;
        private bool DayMode = false;
        private bool NoCreditTerms = false;
        private bool OnlyUpcoming = false;

        public delegate void ScheduleEventArgs(Schedule s);
        public event ScheduleEventArgs OnSheduleChanged;


        public ScheduleUsersExpanderComponent()
        {
            InitializeComponent();
            DataContext = this;

            this.Visibility = Visibility.Collapsed;
        }

        public void SetUp(ScheduleComponent sc, DialogHost dh)
        {
            this.scheduleComponent = sc;
            this.scheduleDialogHost = dh;

            var par = App.Current.MainWindow as MainWindow;
            this.scheduleDialogHost = par.MV.LoadingDialog; //! Test?



            this.SelectedUsers.Add(new Tuple<bool, StagUser>(true, Settings.LocalUser));

            sc.OnScheduleSettingsChange += OnScheduleSettingsChange;
        }

        private async void Init()
        {
            LoadingView lv = new LoadingView();
            if (this.scheduleDialogHost.IsOpen)
            {
                
            }
            var res = await scheduleDialogHost.ShowDialog(lv, delegate(object o, DialogOpenedEventArgs a) 
            {
                LoadSchedulesAsync((bool b)=> 
                {
                    CreateSchedule();
                    createExpander();
                    lv.OnRequestSuccess();
                });
            });
        }

        private async void LoadSchedulesAsync(Action<bool> b)
        {
            var tasks = this.SelectedUsers.Select(x => x.Item2.LoadSchedule(this.SelectedDay, this.DayMode, this.NoCreditTerms, this.OnlyUpcoming));
            var results = await Task.WhenAll(tasks);

            b(true);
        }

        private void CreateSchedule()
        {
            List<Schedule> s = new List<Schedule>();
            foreach(Tuple<bool,StagUser> t in this.SelectedUsers)
            {
                if (t.Item1)
                {
                    s.Add((Schedule)t.Item2.stagSchedule.Clone());
                }
            }

            //var sl = this.SelectedUsers.ToList().Where(x => x.Item1.Equals(true)).Select(x => x.Item2.stagSchedule);
            //Schedule s = new Schedule(sl.ToArray());
            OnSheduleChanged(new Schedule(s.ToArray()));
        }

        private void OnScheduleSettingsChange(DateTime SelectedDay, bool DayMode, bool NoCreditTerms, bool OnlyUpcoming)
        {
            bool isChange = false;

            if(this.SelectedDay.ToString("d.M.yyyy") != SelectedDay.ToString("d.M.yyyy"))
            {
                this.SelectedDay = SelectedDay;
                isChange = true;
            }

            if(this.DayMode != DayMode)
            {
                this.DayMode = DayMode;
                isChange = true;
            }

            if (this.NoCreditTerms != NoCreditTerms)
            {
                this.NoCreditTerms = NoCreditTerms;
                isChange = true;
            }

            if (this.OnlyUpcoming != OnlyUpcoming)
            {
                this.OnlyUpcoming = OnlyUpcoming;
                isChange = true;
            }

            if (isChange)
            {
                Init();
            }

        }

        private void createExpander()
        {
            ExpanderStack.Children.Clear();

            foreach(Tuple<bool,StagUser> t in this.SelectedUsers.ToList())
            {
                Grid uGrid = new Grid();
                uGrid.Height = 50;
                uGrid.Background = Brushes.White;
                uGrid.VerticalAlignment = VerticalAlignment.Top;
                uGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
                uGrid.Margin = new Thickness(23, 0, 28, 0);

                Rectangle topDivider = new Rectangle();
                topDivider.VerticalAlignment = VerticalAlignment.Top;
                topDivider.HorizontalAlignment = HorizontalAlignment.Stretch;
                topDivider.Height = 1;
                topDivider.Fill = new SolidColorBrush(Color.FromRgb(245, 245, 245));
                uGrid.Children.Add(topDivider);

                StackPanel uGridContent = new StackPanel();
                uGridContent.HorizontalAlignment = HorizontalAlignment.Left;
                uGridContent.Orientation = Orientation.Horizontal;
                uGrid.Children.Add(uGridContent);

                PackIcon userIcon = new PackIcon();
                userIcon.Kind = PackIconKind.AccountCircle;
                userIcon.VerticalAlignment = VerticalAlignment.Center;
                userIcon.Width = 30;
                userIcon.Height = 30;
                userIcon.Margin = new Thickness(0, 0, 10, 0);
                userIcon.Foreground = new SolidColorBrush(t.Item2.ToColor());
                
                uGridContent.Children.Add(userIcon);

                TextBlock userText = new TextBlock();
                userText.VerticalAlignment = VerticalAlignment.Center;
                userText.FontSize = 18;
                userText.Width = 200;
                userText.Foreground = Brushes.Black;
                userText.Margin = new Thickness(5, 0, 0, 0);
                userText.Text = $"{t.Item2.stagId} - {t.Item2.stagUserFirstName} {t.Item2.stagUserLastName}";
                uGridContent.Children.Add(userText);

                ToggleButton tb = new ToggleButton();
                tb.IsChecked = t.Item1;
                tb.Margin = new Thickness(10, 0, 0, 0);
                tb.Width = 50;

                tb.Checked += (o, a) =>
                {
                    HideUser(t.Item2, true);
                };

                tb.Unchecked += (o, a) =>
                {
                    HideUser(t.Item2, false);
                };

                uGridContent.Children.Add(tb);

                Button rmButton = new Button();
                rmButton.HorizontalAlignment = HorizontalAlignment.Right;
                rmButton.Width = 100;
                rmButton.Background = Brushes.Red;
                rmButton.BorderBrush = Brushes.Red;
                rmButton.Content = "Remove";
                rmButton.Click += (o, r) =>
                {
                    RemoveUser(t.Item2);
                };
                if(t.Item2.stagId != Settings.LocalUser.stagId)
                {
                    uGrid.Children.Add(rmButton);
                }
                
                ExpanderStack.Children.Add(uGrid);
            }
        }

        private void HideUser(StagUser u, bool v)
        {
            foreach(Tuple<bool,StagUser> t in this.SelectedUsers.ToArray())
            {
                if(u.stagId == t.Item2.stagId)
                {
                    int id = Array.IndexOf(this.SelectedUsers.ToArray(), t);
                    this.SelectedUsers[id] = new Tuple<bool, StagUser>(v, u);
                    Init();
                    break;
                }
            }
        }

        private void RemoveUser(StagUser u)
        {
            foreach(Tuple<bool,StagUser> t in this.SelectedUsers.ToArray())
            {
                if(t.Item2.stagId == u.stagId)
                {
                    int id = Array.IndexOf(this.SelectedUsers.ToArray(), t);
                    this.SelectedUsers.Remove(t);
                    Init();
                    break;
                }
            }
        }

        private async void AddButton_Click(object sender, RoutedEventArgs e)
        {
            var par = App.Current.MainWindow as MainWindow;
            SelectUsersComponent su = new SelectUsersComponent();
            var result = await par.MV.LoadingDialog.ShowDialog(su);
            List<dynamic> r = (List<dynamic>)result;
            if (r.Count > 0)
            {
                AddUsers(r);
            }
            
        }

        private void AddUsers(List<dynamic> uList)
        {
            foreach(dynamic u in uList)
            {
                StagUser uSU = (StagUser)u;
                if(uSU.stagId == Settings.LocalUser.stagId) { continue; }
                bool clone = false;
                foreach(Tuple<bool, StagUser> t in this.SelectedUsers.ToList())
                {
                    if(t.Item2.stagId == uSU.stagId)
                    {
                        clone = true;
                        break;
                    }
                }
                if (!clone)
                {
                    this.SelectedUsers.Add(new Tuple<bool, StagUser>(false, uSU));
                }
            }

            Init();
        }
    }

    public class ScheduleEventArgs : EventArgs
    {
        public Schedule NewSchedule { get; private set; }

        public ScheduleEventArgs(Schedule n)
        {
            this.NewSchedule = n;
        }
    }
}
