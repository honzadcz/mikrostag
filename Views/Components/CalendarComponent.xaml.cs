﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Ultimate_Schedule_Builder.Models;

namespace Ultimate_Schedule_Builder.Views.Components
{
    public partial class CalendarComponent : UserControl
    {
        public DateTime SelectedDate { get; private set; } = DateTime.Today;
        public CalendarQueryType SelectedType { get; private set; } = CalendarQueryType.Static;

        public delegate void DateEventArgs (DateTime d);
        public event DateEventArgs OnDateChange;

        public delegate void QueryTypeEventArgs(CalendarQueryType t);
        public event QueryTypeEventArgs OnQueryTypeChange;

        public CalendarComponent()
        {
            InitializeComponent();
            DataContext = this;


            this.setUpEvents();

            this.MainCalendarTarget.SelectionMode = CalendarSelectionMode.None;
        }

        public CalendarComponent(CalendarSelectionMode c)
        {
            InitializeComponent();
            DataContext = this;


            this.setUpEvents();

            this.MainCalendarTarget.SelectionMode = c;

        }

        private void setUpEvents()
        {
            ScheduleTypeSelect.SelectionChanged += (s, e) =>
            {
                int si = ((ComboBox)s).SelectedIndex;
                updateCalendarQuery(si);
            };

            MainCalendarTarget.SelectedDatesChanged += MainCalendarTarget_SelectedDatesChanged;
        }

        public void SetCalendarQuery(CalendarQueryType csm)
        {
            updateCalendarQuery((int)csm);
        }

        private void updateCalendarQuery(int i)
        {
            if(this.SelectedType != (CalendarQueryType)i)
            {
                this.SelectedType = (CalendarQueryType)i;
            }
            else
            {
                return;
            }
            

            switch (this.SelectedType)
            {
                case CalendarQueryType.Static:
                    this.MainCalendarTarget.IsEnabled = true;
                    this.MainCalendarTarget.SelectionMode = CalendarSelectionMode.None;

                    break;
                case CalendarQueryType.Week:
                    this.MainCalendarTarget.IsEnabled = true;
                    this.MainCalendarTarget.SelectionMode = CalendarSelectionMode.SingleRange;

                    break;
                case CalendarQueryType.Day:
                    this.MainCalendarTarget.IsEnabled = true;
                    this.MainCalendarTarget.SelectionMode = CalendarSelectionMode.SingleDate;

                    break;
            }

            OnQueryTypeChange(this.SelectedType);
        }

        private void MainCalendarTarget_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            if(this.SelectedType == CalendarQueryType.Static) { return; }

            switch (this.SelectedType)
            {
                case CalendarQueryType.Week:
                    if(this.MainCalendarTarget.SelectionMode == CalendarSelectionMode.SingleRange)
                    {
                        if(this.MainCalendarTarget.SelectedDates.Count > 0)
                        {
                            DateTime wd = Utility.FirstDayOfWeek(this.MainCalendarTarget.SelectedDates[0]);

                            this.SelectedDate = wd;
                        }
                    }
                    break;
                case CalendarQueryType.Day:
                    if(this.MainCalendarTarget.SelectionMode == CalendarSelectionMode.SingleDate)
                    {
                        this.SelectedDate = this.MainCalendarTarget.SelectedDate ?? this.SelectedDate;
                    }
                    break;
            }

            OnDateChange(this.SelectedDate);
            UpdateCalendarDate();
        }

        private void UpdateCalendarDate()
        {
            // Select dates on calendar
            switch (this.SelectedType)
            {
                case CalendarQueryType.Static:

                    break;
                case CalendarQueryType.Week:
                    DateTime ew = Utility.LastDayOfWeek(this.SelectedDate);

                    this.MainCalendarTarget.DisplayDateStart = this.SelectedDate;
                    this.MainCalendarTarget.DisplayDateEnd = ew;

                    break;
                case CalendarQueryType.Day:

                    this.MainCalendarTarget.DisplayDate = this.SelectedDate;
                    this.MainCalendarTarget.SelectedDate = this.SelectedDate;

                    break;
            }


        }

        public void MoveCalendarNext()
        {
            if(this.SelectedType == CalendarQueryType.Static) { return; }

            switch (this.SelectedType)
            {
                case CalendarQueryType.Week:
                    DateTime ws = Utility.FirstDayOfWeek(this.SelectedDate);
                    this.SelectedDate = ws.AddDays(7);
                    break;
                case CalendarQueryType.Day:
                    this.SelectedDate = this.SelectedDate.AddDays(1);
                    break;
            }
            OnDateChange(this.SelectedDate);
            UpdateCalendarDate();
        }

        public void MoveCalendarPrevious()
        {
            if (this.SelectedType == CalendarQueryType.Static) { return; }

            switch (this.SelectedType)
            {
                case CalendarQueryType.Week:
                    DateTime ws = Utility.FirstDayOfWeek(this.SelectedDate);
                    this.SelectedDate = ws.AddDays(-7);
                    break;
                case CalendarQueryType.Day:
                    this.SelectedDate = this.SelectedDate.AddDays(-1);
                    break;
            }
            OnDateChange(this.SelectedDate);
            UpdateCalendarDate();
        }
    }

    public class DateEventArgs : EventArgs
    {
        public DateTime SelectedDate { get; private set; }

        public DateEventArgs(DateTime n)
        {
            this.SelectedDate = n;
        }
    }

    public class QueryTypeEventArgs : EventArgs
    {
        public CalendarQueryType SelectedType { get; private set; }

        public QueryTypeEventArgs(CalendarQueryType n)
        {
            this.SelectedType = n;
        }
    }

    public enum CalendarQueryType
    {
        Static = 0,
        Week = 1,
        Day = 2
    }
}
