﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ultimate_Schedule_Builder.Views.Components
{
    /// <summary>
    /// Interaction logic for ScheduleSettingsComponent.xaml
    /// </summary>
    public partial class ScheduleSettingsComponent : UserControl
    {
        private ScheduleComponent scheduleRef = null;

        public ScheduleSettings SelectedSettings { get; private set; } = new ScheduleSettings();

        private List<string> singleSelectSettings = new List<string>()
        {
            "ExportToImage"
        };

        public delegate void ScheduleSettingsUpdateArgs(string s,FieldInfo f,bool val);
        public event ScheduleSettingsUpdateArgs OnScheduleSettingsChanged;



        public ScheduleSettingsComponent()
        {
            InitializeComponent();
            DataContext = this;
        }

        public void setUp(ScheduleComponent sc)
        {
            this.scheduleRef = sc;


            this.renderSettings();
        }

        private void renderSettings()
        {
            ExpanderStack.Children.Clear();

            var fields = typeof(ScheduleSettings).GetFields(BindingFlags.NonPublic | BindingFlags.Instance);

            foreach(var st in fields)
            {
                string stName = st.Name.Split('<', '>')[1];


                Grid sGrid = new Grid();
                sGrid.Margin = new Thickness(5, 0, 5, 0);
                sGrid.Height = 30;

                TextBlock sText = new TextBlock();
                sGrid.Children.Add(sText);
                sText.HorizontalAlignment = HorizontalAlignment.Left;
                sText.VerticalAlignment = VerticalAlignment.Center;
                sText.Text = stName;

                ToggleButton sButton = new ToggleButton();
                sGrid.Children.Add(sButton);
                sButton.HorizontalAlignment = HorizontalAlignment.Right;
                sButton.VerticalAlignment = VerticalAlignment.Center;
                sButton.DataContext = st;

                sButton.IsChecked = (bool)st.GetValue(SelectedSettings);

                sButton.Checked += (object o, RoutedEventArgs a) =>
                {
                    OnScheduleSettingsChanged(stName, st, true);
                };

                sButton.Unchecked += (object o, RoutedEventArgs a) =>
                {
                    OnScheduleSettingsChanged(stName, st, false);
                };

                ExpanderStack.Children.Add(sGrid);
            }

            foreach(string sval in this.singleSelectSettings)
            {
                Grid sGrid = new Grid();
                sGrid.Margin = new Thickness(5, 0, 5, 0);
                sGrid.Height = 30;

                TextBlock sText = new TextBlock();
                sGrid.Children.Add(sText);
                sText.HorizontalAlignment = HorizontalAlignment.Left;
                sText.VerticalAlignment = VerticalAlignment.Center;
                sText.Text = sval;

                Button sButton = new Button();
                sGrid.Children.Add(sButton);
                sButton.HorizontalAlignment = HorizontalAlignment.Right;
                sButton.VerticalAlignment = VerticalAlignment.Center;
                sButton.DataContext = sval;
                sButton.Width = 50;
                sButton.Height = 25;

                sButton.Click += (object o, RoutedEventArgs a) =>
                {
                    selectButtonClicked(sval);
                };

                ExpanderStack.Children.Add(sGrid);
            }
        }

        private void selectButtonClicked(string sn)
        {

        }
    }

    public class ScheduleSettings
    {
        public bool DisplayFreeHours            { get; private set; } = false;
        public bool DisplayMultipleSchedules    { get; private set; } = false;
        public bool DisplayEmptyDays            { get; private set; } = false;
        public bool LastCourseIsLastInSchedule  { get; private set; } = true;

        public ScheduleSettings(){}

        /// <summary>
        /// Represents setting of ScheduleComponent
        /// </summary>
        /// <param name="dfh">DisplayFreeHours</param>
        /// <param name="dms">DisplayMultipleSchedules</param>
        /// <param name="ded">DisplayEmptyDays</param>
        /// <param name="lcilis">LastCourseIsLastInSchedule</param>
        public ScheduleSettings(bool dfh, bool dms, bool ded, bool lcilis)
        {
            this.DisplayFreeHours = dfh;
            this.DisplayMultipleSchedules = dms;
            this.DisplayEmptyDays = ded;
            this.LastCourseIsLastInSchedule = lcilis;
        }


    }

    public class ScheduleSettingsUpdateArgs : EventArgs
    {
        public ScheduleSettings NewSettings { get; private set; }

        public ScheduleSettingsUpdateArgs(ScheduleSettings n)
        {
            this.NewSettings = n;
        }
    }
}
