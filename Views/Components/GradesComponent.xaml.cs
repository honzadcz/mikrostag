﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Ultimate_Schedule_Builder.Models;

namespace Ultimate_Schedule_Builder.Views.Components
{
    /// <summary>
    /// Interaction logic for GradesComponent.xaml
    /// </summary>
    public partial class GradesComponent : UserControl
    {
        public GradesComponent()
        {
            InitializeComponent();
            DataContext = this;

            getGrades();

        }

        private async void getGrades()
        {
            this.GradeDropper.IsEnabled = false;
            StagGrade[] sgArr = await Rest.CreateStudentGetGradesRequest();
            renderGrades(sgArr);
            this.GradeDropper.IsEnabled = true;
        }

        private void renderGrades(StagGrade[] sgArr)
        {
            this.GradeDropperContent.Children.Clear();

            foreach(StagGrade sg in sgArr)
            {
                Grid sGrid = new Grid();
                sGrid.Height = 50;
                sGrid.VerticalAlignment = VerticalAlignment.Top;
                sGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                sGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(2, GridUnitType.Star) });
                sGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(3, GridUnitType.Star) });
                sGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });

                Rectangle divider = new Rectangle();
                sGrid.Children.Add(divider);
                divider.Height = 1;
                divider.HorizontalAlignment = HorizontalAlignment.Stretch;
                divider.VerticalAlignment = VerticalAlignment.Bottom;
                divider.Fill = Brushes.Gray;
                Grid.SetColumn(divider, 0);
                Grid.SetColumnSpan(divider, 4);

                TextBlock courseId = new TextBlock();
                sGrid.Children.Add(courseId);
                courseId.HorizontalAlignment = HorizontalAlignment.Center;
                courseId.VerticalAlignment = VerticalAlignment.Center;
                courseId.Text = sg.GradeCourse.CourseFaculty + " / " + sg.GradeCourse.CourseId;
                Grid.SetColumn(courseId, 0);

                TextBlock courseName = new TextBlock();
                sGrid.Children.Add(courseName);
                courseName.HorizontalAlignment = HorizontalAlignment.Center;
                courseName.VerticalAlignment = VerticalAlignment.Center;
                courseName.Text = sg.GradeCourse.CourseName;
                Grid.SetColumn(courseName, 1);

                if (sg.GradeCourse.CourseHasExam)
                {
                    StackPanel stPanel = new StackPanel();
                    sGrid.Children.Add(stPanel);
                    stPanel.HorizontalAlignment = HorizontalAlignment.Center;
                    stPanel.Orientation = Orientation.Horizontal;
                    Grid.SetColumn(stPanel, 3);

                    Grid firstElipse = new Grid();
                    if(sg.GradeAcredation != null)
                    {
                        firstElipse.ToolTip = $" Attempt: {sg.GradeAcredation.GradeAttempt}. \n Last attempt date: {sg.GradeAcredation.GradeAttemptDate} \n  Result: {(string)sg.GradeAcredation.GradeStatus}";
                    }
                    
                    stPanel.Children.Add(firstElipse);

                    Ellipse bgResult = new Ellipse();
                    firstElipse.Children.Add(bgResult);
                    bgResult.Width = 24;
                    bgResult.Height = 24;
                    bgResult.HorizontalAlignment = HorizontalAlignment.Center;
                    bgResult.VerticalAlignment = VerticalAlignment.Center;
                    bgResult.Fill = Brushes.Gray;

                    Ellipse fiResult = new Ellipse();
                    firstElipse.Children.Add(fiResult);
                    fiResult.Width = 20;
                    fiResult.Height = 20;
                    fiResult.HorizontalAlignment = HorizontalAlignment.Center;
                    fiResult.VerticalAlignment = VerticalAlignment.Center;

                    if(sg.GradeAcredation != null)
                    {
                        if (sg.GradeAcredation.GradeByNumber)
                        {
                            int gradeRes = (int)sg.GradeAcredation.GradeStatus;
                            if (gradeRes != 0)
                            {
                                if (gradeRes < 4)
                                {
                                    fiResult.Fill = Brushes.LightGreen;
                                }
                                else
                                {
                                    if (gradeRes == 4 && sg.GradeAcredation.GradeAttempt < 3)
                                    {
                                        fiResult.Fill = Brushes.Orange;
                                    }
                                    else
                                    {
                                        fiResult.Fill = Brushes.Red;
                                    }
                                }
                            }
                            else
                            {
                                fiResult.Fill = Brushes.Gray;
                            }
                        }
                        else
                        {
                            bool gradeRes = (bool)sg.GradeAcredation.GradeStatus;
                            if (gradeRes)
                            {
                                fiResult.Fill = Brushes.LightGreen;
                            }
                            else
                            {
                                if (gradeRes == false && sg.GradeAcredation.GradeAttempt < 3)
                                {
                                    fiResult.Fill = Brushes.Orange;
                                }
                                else
                                {
                                    fiResult.Fill = Brushes.Red;
                                }

                            }
                        }
                    }
                    else
                    {
                        fiResult.Fill = Brushes.Gray;
                    }

                    PackIcon secIcon = new PackIcon();
                    stPanel.Children.Add(secIcon);
                    secIcon.Width = 30;
                    secIcon.Height = 30;
                    secIcon.Margin = new Thickness(10, 0, 10, 0);
                    secIcon.VerticalAlignment = VerticalAlignment.Center;
                    secIcon.Foreground = Brushes.Gray;
                    secIcon.Kind = PackIconKind.ChevronDoubleRight;

                    Grid secondElipse = new Grid();

                    if (sg.GradeExam != null)
                    {
                        secondElipse.ToolTip = $" Attempt: {sg.GradeExam.GradeAttempt}. \n Last attempt date: {sg.GradeExam.GradeAttemptDate} \n  Result: {(string)sg.GradeExam.GradeStatus}";
                    }

                    stPanel.Children.Add(secondElipse);

                    Ellipse bgResultTwo = new Ellipse();
                    secondElipse.Children.Add(bgResultTwo);
                    bgResultTwo.Width = 24;
                    bgResultTwo.Height = 24;
                    bgResultTwo.HorizontalAlignment = HorizontalAlignment.Center;
                    bgResultTwo.VerticalAlignment = VerticalAlignment.Center;
                    bgResultTwo.Fill = Brushes.Gray;

                    Ellipse fiResultTwo = new Ellipse();
                    secondElipse.Children.Add(fiResultTwo);
                    fiResultTwo.Width = 20;
                    fiResultTwo.Height = 20;
                    fiResultTwo.HorizontalAlignment = HorizontalAlignment.Center;
                    fiResultTwo.VerticalAlignment = VerticalAlignment.Center;

                    if (sg.GradeExam != null)
                    {
                        if (sg.GradeExam.GradeByNumber)
                        {
                            int gradeRes = (int)sg.GradeExam.GradeStatus;
                            if (gradeRes != 0)
                            {
                                if (gradeRes < 4)
                                {
                                    fiResultTwo.Fill = Brushes.LightGreen;
                                }
                                else
                                {
                                    if (gradeRes == 4 && sg.GradeExam.GradeAttempt < 3)
                                    {
                                        fiResultTwo.Fill = Brushes.Orange;
                                    }
                                    else
                                    {
                                        fiResultTwo.Fill = Brushes.Red;
                                    }
                                }
                            }
                            else
                            {
                                fiResultTwo.Fill = Brushes.Gray;
                            }
                        }
                        else
                        {
                            bool gradeRes = (bool)sg.GradeExam.GradeStatus;
                            if (gradeRes)
                            {
                                fiResultTwo.Fill = Brushes.LightGreen;
                            }
                            else
                            {
                                if (gradeRes == false && sg.GradeExam.GradeAttempt < 3)
                                {
                                    fiResultTwo.Fill = Brushes.Orange;
                                }
                                else
                                {
                                    fiResultTwo.Fill = Brushes.Red;
                                }

                            }
                        }
                    }
                    else
                    {
                        fiResultTwo.Fill = Brushes.Gray;
                    }


                }
                else
                {
                    Ellipse bgResult = new Ellipse();
                    sGrid.Children.Add(bgResult);
                    bgResult.Width = 24;
                    bgResult.Height = 24;
                    bgResult.HorizontalAlignment = HorizontalAlignment.Center;
                    bgResult.VerticalAlignment = VerticalAlignment.Center;
                    bgResult.Fill = Brushes.Gray;
                    Grid.SetColumn(bgResult, 3);

                    Ellipse fiResult = new Ellipse();
                    sGrid.Children.Add(fiResult);
                    fiResult.Width = 20;
                    fiResult.Height = 20;
                    fiResult.HorizontalAlignment = HorizontalAlignment.Center;
                    fiResult.VerticalAlignment = VerticalAlignment.Center;
                    Grid.SetColumn(fiResult, 3);

                    if (sg.GradeExam != null)
                    {
                        fiResult.ToolTip = $" Attempt: {sg.GradeExam.GradeAttempt}. \n Last attempt date: {sg.GradeExam.GradeAttemptDate} \n  Result: {(string)sg.GradeExam.GradeStatus}";
                    }


                    if (sg.GradeExam.GradeByNumber)
                    {
                        int gradeRes = (int)sg.GradeExam.GradeStatus;
                        if (gradeRes != 0)
                        {
                            if(gradeRes < 4)
                            {
                                fiResult.Fill = Brushes.LightGreen;
                            }
                            else
                            {
                                if(gradeRes == 4 && sg.GradeExam.GradeAttempt < 3)
                                {
                                    fiResult.Fill = Brushes.Orange;
                                }
                                else
                                {
                                    fiResult.Fill = Brushes.Red;
                                }
                            }
                        }
                        else
                        {
                            fiResult.Fill = Brushes.Gray;
                        }
                    }
                    else
                    {
                        bool gradeRes = (bool)sg.GradeExam.GradeStatus;
                        if (gradeRes)
                        {
                            fiResult.Fill = Brushes.LightGreen;
                        }
                        else
                        {
                            if (gradeRes == false && sg.GradeExam.GradeAttempt < 3)
                            {
                                if(sg.GradeExam.GradeAttempt == 0)
                                {
                                    fiResult.Fill = Brushes.Gray;
                                }
                                else
                                {
                                    fiResult.Fill = Brushes.Orange;
                                }
                            }
                            else
                            {
                                fiResult.Fill = Brushes.Red;
                            }

                        }
                    }
                }
                
                this.GradeDropperContent.Children.Add(sGrid);
            }

        }
    }
}
